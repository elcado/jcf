package net.cadier.actions;

import java.util.Objects;
import java.util.function.Consumer;

public class BasicAction<T> implements Action<T> {
    /**
     * The main action function.
     */
    private Consumer<T> mainFunction;

    /**
     * Package private constructor
     */
    BasicAction(Consumer<T> function) {
        Objects.requireNonNull(function);

        this.mainFunction = function;
    }

    @Override
    public boolean execute(T t) throws IllegalStateException {
        this.mainFunction.accept(t);

        return true;
    }
}