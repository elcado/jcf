package net.cadier.actions;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

public class FailableAction<T> implements Action<T> {
    /**
     * The main action function.
     */
    private Function<T, Boolean> mainFunction;

    /**
     * The fail action function.
     */
    private Consumer<T> failFunction;

    /**
     * Internal flag to check that the failable action is well built.
     */
    private AtomicBoolean executable = new AtomicBoolean(false);

    /**
     * Package private constructor
     */
    FailableAction(Function<T, Boolean> function) {
        Objects.requireNonNull(function);

        this.mainFunction = function;
        this.executable.set(false); // not executable until a fail function is set
    }

    public FailableAction failOn(Consumer<T> function) {
        Objects.requireNonNull(function);

        this.failFunction = function;
        this.executable.set(true);
        return this;
    }

    @Override
    public boolean execute(T t) throws IllegalStateException {
        Boolean result = Boolean.FALSE;
        if (this.executable.get()) {
            // apply failable function and eventually apply failOnFunction
            result = this.mainFunction.apply(t);
            if (!result) {
                this.failFunction.accept(t);
            }
        } else {
            throw new IllegalStateException("A " + FailableAction.class.getSimpleName() + " is not executable until a failOn consumer is defined.");
        }

        return result.booleanValue();
    }

}
