package net.cadier.actions;

import java.util.function.Consumer;
import java.util.function.Function;

public interface Action<T> {
    /**
     * Build a basic action that will consume a data with the supplied consumer.
     * @param function data consumer.
     * @param <T> type of the data.
     * @return a new basic action.
     */
    static <T> BasicAction<T> execute(Consumer<T> function) {
        return new BasicAction<>(function);
    }

    /**
     * Build a failable action that will consume a data with the supplied function, and apply the fail consumer if
     * the function returns false.
     * @param function data consumer.
     * @param <T> type of the data.
     * @return a new failable action.
     */
    static <T> FailableAction<T> execute(Function<T, Boolean> function) {
        return new FailableAction<>(function);
    }

    /**
     * Execute the Action on the supplied argument
     * @param t data to execute the action on.
     * @return true if action ends without error, false otherwise.
     */
    boolean execute(T t) throws IllegalStateException;
}
