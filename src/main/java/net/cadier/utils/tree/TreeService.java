/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.utils.tree;

import java.util.List;

/**
 * @author Frédéric Cadier
 */
public interface TreeService<K, V> {
    /**
     * Test if an element with the supplied id exists in the tree.
     *
     * @param eltId id of the tested element
     * @return true if the element is in the tree, false otherwise
     */
    boolean contains(K eltId);

    /**
     * Adds a root element. If an element with the same id already exist in
     * tree, it is overriden.
     *
     * @param eltId    id of the root element to be added
     * @param eltValue element to be added
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    void addRoot(K eltId, V eltValue) throws IllegalArgumentException;

    /**
     * Check if element is a root in the tree.
     *
     * @param eltId id of the element
     * @return true if element id a root, false otherwise.
     */
    boolean isRoot(K eltId);

    /**
     * Returns the ids of root elements
     *
     * @return ids of all root elements
     */
    List<K> getRoots();

    /**
     * Adds an element under the specified parent, identified by its id. If
     * parent id is null, element will be a root one. If an element with the
     * same id already exist in tree, it is overriden.
     *
     * @param parentId id of the future parent element
     * @param eltId    id of the root element to be added
     * @param eltValue element to be added
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    void addElement(K parentId, K eltId, V eltValue) throws IllegalArgumentException;

    /**
     * Recursively removes the element referenced by the supplied id and all its
     * children form the tree.
     *
     * @param eltId id of the element to be removed
     * @return the previously element referenced by the supplied id
     */
    V removeElement(K eltId);

    /**
     * Gets the id of the parent of the specified element, or null if the
     * element is root.
     *
     * @param eltId id of the searched element
     * @return id of the parent, or null if the element is root
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    K getParent(K eltId) throws IllegalArgumentException;

    /**
     * Gets ids of the list (might be empty) of children of the specified
     * element, identified by its id.
     *
     * @param eltId id of the searched element
     * @return list of ids of the element's children
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    List<K> getChildren(K eltId) throws IllegalArgumentException;

    /**
     * Gets siblings of an element, identified by its id. Siblings are all the
     * other children of the parent of the element. If the element has no
     * parent, then it is a root element and the returned list will contain the
     * other root elements.
     *
     * @param eltId id of the searched element
     * @return list of ids of the element's siblings
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    List<K> getNeighbors(K eltId) throws IllegalArgumentException;

    /**
     * Gets the referenced object of the specified element, identified by its
     * id.
     *
     * @param eltId id of the searched element
     * @return the referenced object
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    V getRef(K eltId) throws IllegalArgumentException;

    /**
     * Gets all referenced objects
     *
     * @return all referenced objects in the tree
     */
    List<V> getRefs();

}
