/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.utils.tree;

import java.util.*;

/**
 * This structures holds a multi-rooted tree. Each node is identified by its id typed by generic K, and references an
 * object typed by generic V.
 *
 * @author Frédéric Cadier
 */
public class Tree<K, V> implements TreeService<K, V> {
    /**
     * Lists roots' ids
     */
    private final Set<K> rootIds = new HashSet<>();

    /**
     * Maps each element's id with its parent's id
     */
    private final Map<K, K> parents = new HashMap<>();

    /**
     * Maps each element's id with its referenced object
     */
    private final Map<K, V> id2ref = new HashMap<>();

    /**
     * Maps each element's id with its children's ids
     */
    private final Map<K, List<K>> children = new HashMap<>();

    /**
     * Test if an element with the supplied id exists in the tree.
     *
     * @param eltId id of the tested element
     * @return true if the element is in the tree, false otherwise
     */
    @Override
    public boolean contains(K eltId) {
        return this.id2ref.containsKey(eltId);
    }

    /**
     * Returns the ids of root elements
     *
     * @return ids of all root elements
     */
    @Override
    public List<K> getRoots() {
        return new ArrayList<>(this.rootIds);
    }

    /**
     * Adds a root element. If an element with the same id already exist in
     * tree, it is overriden.
     *
     * @param eltId    id of the root element to be added
     * @param eltValue element to be added
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    @Override
    public void addRoot(K eltId, V eltValue) throws IllegalArgumentException {
        this.addElement(null, eltId, eltValue);
    }

    /**
     * Check if element is a root in the tree.
     *
     * @param eltId id of the element
     * @return true if element id a root, false otherwise.
     */
    @Override
    public boolean isRoot(K eltId) {
        return this.rootIds.contains(eltId);
    }

    /**
     * Adds an element under the specified parent, identified by its id. If
     * parent id is null, element will be a root one. If an element with the
     * same id already exist in tree, it is overriden.
     *
     * @param parentId id of the future parent element
     * @param eltId    id of the root element to be added
     * @param eltValue element to be added
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    @Override
    public void addElement(K parentId, K eltId, V eltValue) throws IllegalArgumentException {
        if (parentId == null) {
            // ref element as a root
            this.rootIds.add(eltId);
        } else {
            // test that parent is registered
            this.checkElement(parentId, "Parent with id '" + parentId + "' is not registered, cannot register a " +
                    "child" + " [" + eltId + ":" + eltValue + "].");

            // add child to parent's children list (eventually build list)
            List<K> parentChildren = this.children.computeIfAbsent(parentId, k -> new ArrayList<>());
            parentChildren.add(eltId);

            // maps element's id to its parent's id (for ease of search)
            this.parents.put(eltId, parentId);
        }

        // ref the object
        this.id2ref.put(eltId, eltValue);
    }

    /**
     * Recursively removes the element referenced by the supplied id and all its
     * children form the tree.
     *
     * @param eltId id of the element to be removed
     * @return the previously element referenced by the supplied id
     */
    @Override
    public V removeElement(K eltId) {
        // test that element is registered
        this.checkElement(eltId, "Element with id '" + eltId + "' is not registered, cannot remove it.");

        // if element has children...
        List<K> childrenList = new ArrayList<>(this.getChildren(eltId));
        if (!childrenList.isEmpty()) {
            // ... recursively remove each child
            for (K childId : childrenList) {
                this.removeElement(childId);
            }

            // finally remove element from children map (that holds children of each element)
            this.children.remove(eltId);
        }

        // if element has a parent, removes it from its parent's children list
        K parent = this.getParent(eltId);
        if (parent != null) {
            this.parents.remove(eltId);
            this.getChildren(parent).remove(eltId);
        }

        // if it was a root, removes it from the list
        this.rootIds.remove(eltId);

        // finally remove object referenced by this element
        return this.id2ref.remove(eltId);
    }

    /**
     * Gets the id of the parent of the specified element, or null if the
     * element is root.
     *
     * @param eltId id of the searched element
     * @return id of the parent, or null if the element is root
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    @Override
    public K getParent(K eltId) throws IllegalArgumentException {
        // test that element is registered
        this.checkElement(eltId, "Element with id '" + eltId + "' is not registered, cannot get its parent.");

        // return parent or null
        return this.parents.get(eltId);
    }

    /**
     * Gets ids of the list (might be empty) of children of the specified
     * element, identified by its id.
     *
     * @param eltId id of the searched element
     * @return list of ids of the element's children
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    @Override
    public List<K> getChildren(K eltId) throws IllegalArgumentException {
        // test that element is registered
        this.checkElement(eltId, "Element with id '" + eltId + "' is not registered, cannot get its children.");

        // return children or an empty list if none
        List<K> eltChildren = this.children.get(eltId);
        return (eltChildren == null) ? new ArrayList<>() : eltChildren;
    }

    /**
     * Gets siblings of an element, identified by its id. Siblings are all the
     * other children of the parent of the element. If the element has no
     * parent, then it is a root element and the returned list will contain the
     * other root elements.
     *
     * @param eltId id of the searched element
     * @return list of ids of the element's siblings
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    @Override
    public List<K> getNeighbors(K eltId) throws IllegalArgumentException {
        // test that element is registered
        this.checkElement(eltId, "Element with id '" + eltId + "' is not registered, cannot get its neighbors.");

        // gets all children of parent (if one), or all roots (needs a list copy not to affect the children/roots
        // map/set)
        List<K> neighbors;
        if (this.getParent(eltId) != null) {
            neighbors = new ArrayList<>(this.getChildren(this.getParent(eltId)));
        } else {
            neighbors = new ArrayList<>(this.rootIds);
        }

        // filter the provided element to only return neighbors
        neighbors.remove(eltId);
        return neighbors;
    }

    /**
     * Gets the referenced object of the specified element, identified by its
     * id.
     *
     * @param eltId id of the searched element
     * @return the referenced object
     * @throws IllegalArgumentException thrown if element is not in tree.
     */
    @Override
    public V getRef(K eltId) throws IllegalArgumentException {
        // test that element is registered
        this.checkElement(eltId, "Element with id '" + eltId + "' is not registered, cannot get referenced object.");
        // return referenced object
        return this.id2ref.get(eltId);
    }

    /**
     * Gets all referenced objects
     *
     * @return all referenced objects in the tree
     */
    @Override
    public List<V> getRefs() {
        return new ArrayList<>(this.id2ref.values());
    }

    /**
     * Check that tree contains element id
     */
    private void checkElement(K eltId, String message) throws IllegalArgumentException {
        if (!this.contains(eltId)) throw new IllegalArgumentException("[Tree] " + message);
    }

}
