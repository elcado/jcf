/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.utils.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

public final class ReflectionUtils {
    private ReflectionUtils() {
    }

    public static <T> T buildObject(final Class<T> clazz, final Object... params) throws ReflectionException {
        // get constructor
        Constructor<T> constructor = ReflectionUtils.getConstructor(clazz, params);
        T object = null;
        try {
            constructor.setAccessible(true);
            object = constructor.newInstance(params);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new ReflectionException(clazz, e);
        }
        return object;
    }

    private static <T> Constructor<T> getConstructor(final Class<T> clazz, final Object... params) throws ReflectionException {
        // get args types list
        Class<?>[] argsTypes = new Class<?>[params.length];
        for (int i = 0; i < params.length; i++) {
            argsTypes[i] = params[i].getClass();
        }

        // get constructor that corresponds to the list of args
        Constructor<T> constructor = null;
        try {
            constructor = clazz.getDeclaredConstructor(argsTypes);
        } catch (NoSuchMethodException | SecurityException e) {
            throw new ReflectionException(clazz, e);
        }
        return constructor;
    }

    public static Object getFieldValue(final Object object, final String fieldName) throws ReflectionException {
        Object fieldValue = null;
        try {
            Class<?> currentClass = object.getClass();
            NoSuchFieldException noSuchFieldException;
            // look on class, then super class if NoSuchFieldException raises
            do {
                noSuchFieldException = null;
                try {
                    Field componentsManagerServicesField = currentClass.getDeclaredField(fieldName);
                    componentsManagerServicesField.setAccessible(true);
                    fieldValue = componentsManagerServicesField.get(object);
                    // break if found
                    break;
                } catch (NoSuchFieldException nfe) {
                    // keep exception for eventual later trace
                    noSuchFieldException = nfe;
                }
            } while ((currentClass = currentClass.getSuperclass()) != null);

            // if we ended on a NoSuchFieldException, trace it
            if (Objects.nonNull(noSuchFieldException)) {
                throw new ReflectionException(object, noSuchFieldException);
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new ReflectionException(object, e);
        }
        return fieldValue;
    }

    public static void setFieldValue(final Object object, final String fieldName, final Object fieldValue) throws ReflectionException {
        try {
            Class<?> currentClass = object.getClass();
            NoSuchFieldException noSuchFieldException;
            // look on class, then super class if NoSuchFieldException raises
            do {
                noSuchFieldException = null;
                try {
                    Field componentsManagerServicesField = currentClass.getDeclaredField(fieldName);
                    componentsManagerServicesField.setAccessible(true);
                    componentsManagerServicesField.set(object, fieldValue);
                    // break if found
                    break;
                } catch (NoSuchFieldException nfe) {
                    // keep exception for eventual later trace
                    noSuchFieldException = nfe;
                }
            } while ((currentClass = currentClass.getSuperclass()) != null);

            // if we ended on a NoSuchFieldException, trace it
            if (Objects.nonNull(noSuchFieldException)) {
                throw new ReflectionException(object, noSuchFieldException);
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new ReflectionException(object, e);
        }
    }
}
