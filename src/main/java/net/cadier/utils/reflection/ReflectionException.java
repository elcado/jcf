package net.cadier.utils.reflection;

import java.io.PrintStream;
import java.io.PrintWriter;

public class ReflectionException extends Exception {
    private Class<?> clazz;
    private Object object;
    private Exception initialException;

    public ReflectionException(final Class<?> aClass, final Exception anException) {
        this(aClass, null, anException);
    }

    public ReflectionException(final Object anObject, final Exception anException) {
        this(anObject.getClass(), anObject, anException);
    }

    private ReflectionException(final Class<?> aClass, final Object anObject, final Exception anException) {
        super();

        this.clazz = aClass;
        this.object = anObject;
        this.initialException = anException;
    }

    /**
     * Return the class on which exception occurred.
     * @return a Class.
     */
    public Class<?> getClazz() {
        return clazz;
    }

    /**
     * Return the eventual object on which exception occurred.
     * @return an Object.
     */
    public Object getObject() {
        return object;
    }

    @Override
    public void printStackTrace() {
        System.err.println("ReflectionException on class '" + this.clazz + "' [" + this.object + "].");
        super.printStackTrace();
    }

    @Override
    public void printStackTrace(PrintStream s) {
        System.err.println("ReflectionException on class '" + this.clazz + "' [" + this.object + "].");
        super.printStackTrace(s);
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        System.err.println("ReflectionException on class '" + this.clazz + "' [" + this.object + "].");
        super.printStackTrace(s);
    }
}
