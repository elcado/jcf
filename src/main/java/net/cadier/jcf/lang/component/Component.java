/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.lang.component;

import net.cadier.jcf.lang.component.port.events.EventsSinkPort;
import net.cadier.jcf.lang.component.port.events.EventsSourcePort;
import net.cadier.jcf.lang.component.port.id.IdPort;
import net.cadier.jcf.lang.component.port.parameters.ParametersPort;
import net.cadier.jcf.lang.component.port.services.ProvidedServicePort;
import net.cadier.jcf.lang.component.port.services.UsedServicePort;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Component implements Serializable {
    private final transient List<Component> composedComponents = new ArrayList<>();
    private final transient List<ProvidedServicePort> providedServicePorts = new ArrayList<>();
    private final transient List<UsedServicePort> usedServicePorts = new ArrayList<>();
    private final transient List<ParametersPort> parametersPorts = new ArrayList<>();
    private final transient List<EventsSourcePort> eventsSourcePorts = new ArrayList<>();
    private final transient List<EventsSinkPort> eventsSinkPorts = new ArrayList<>();
    private final transient List<Instance> instances = new ArrayList<>();
    private transient IdPort idPort;
    private boolean initializable;
    private boolean startable;
    private Class<?> clazz;
    private Type type;

    public Class<?> getClazz() {
        return this.clazz;
    }

    public void setClazz(final Class<?> value) {
        this.clazz = value;
    }

    public Type getType() {
        return type;
    }

    public void setType(final Type value) {
        this.type = value;
    }

    public boolean isInitializable() {
        return this.initializable;
    }

    public void setInitializable(final boolean value) {
        this.initializable = value;
    }

    public boolean isStartable() {
        return this.startable;
    }

    public void setStartable(final boolean value) {
        this.startable = value;
    }

    public List<Component> getComposedComponents() {
        return this.composedComponents;
    }

    public void addComposedComponent(final Component aComposedComponent) {
        if (aComposedComponent != null) this.composedComponents.add(aComposedComponent);
    }

    public IdPort getIdPort() {
        return this.idPort;
    }

    public void setIdPort(final IdPort value) {
        this.idPort = value;
    }

    public List<ProvidedServicePort> getProvidedServicePorts() {
        return this.providedServicePorts;
    }

    public void addImplementedServicePort(final ProvidedServicePort aProvidedServicePort) {
        if (aProvidedServicePort != null) this.providedServicePorts.add(aProvidedServicePort);
    }

    public List<UsedServicePort> getUsedServicePorts() {
        return this.usedServicePorts;
    }

    public void addUsedServicePort(final UsedServicePort aUsedServicePort) {
        if (aUsedServicePort != null) this.usedServicePorts.add(aUsedServicePort);
    }

    public List<EventsSourcePort> getEventsSourcePorts() {
        return this.eventsSourcePorts;
    }

    public void addEventsSourcePort(final EventsSourcePort anEventsSourcePort) {
        if (anEventsSourcePort != null) this.eventsSourcePorts.add(anEventsSourcePort);
    }

    public List<EventsSinkPort> getEventsSinkPorts() {
        return this.eventsSinkPorts;
    }

    public void addEventsSinkPort(final EventsSinkPort anEventsSinkPort) {
        if (anEventsSinkPort != null) this.eventsSinkPorts.add(anEventsSinkPort);
    }

    public List<ParametersPort> getParametersPorts() {
        return this.parametersPorts;
    }

    public void addParametersPort(final ParametersPort aParameterPort) {
        if (aParameterPort != null) this.parametersPorts.add(aParameterPort);
    }

    /**
     * Extract (and copy) instances' ids
     * @return a copy of the instances' ids
     */
    public List<InstanceId> getInstancesIds() {
        return this.instances.stream().map(Instance::getInstanceId).collect(Collectors.toList());
    }

    public void addInstance(final Instance instance) {
        this.instances.add(instance);
    }

    public void removeInstance(Instance instance) {
        this.instances.remove(instance);
    }

    @Override
    public String toString() {
        return "Component { type=" + type.getClazz() + ", clazz=" + clazz + ", " +
                "initializable=" + initializable + ", startable=" + startable + " }";
    }

    public String toLongString() {
        String prefix = "|    ";
        String sepLineMaker = "<sep_line>";
        String subSepLineMaker = "<sub_sep_line>";

        StringBuilder bob = new StringBuilder();

        bob.append(sepLineMaker).append("\n");

        // component type
        bob.append(prefix).append("Type:     ").append(this.getType().getClazz().getTypeName()).append("\n");

        // component class
        bob.append(prefix).append("Class:    ").append(this.getClazz().toString()).append("\n");

        // is component stated ?
        bob.append(prefix).append("Stated:   ").append(this.isStartable()).append("\n");

        if (
            //                this.getParametersService() != null
            //            ||
                !this.getProvidedServicePorts().isEmpty() || !this.getUsedServicePorts().isEmpty() || !this.getEventsSourcePorts().isEmpty() || !this.getEventsSinkPorts().isEmpty())
            bob.append(subSepLineMaker).append("\n");

        //        // parameters interface
        //        if (this.getParametersService() != null) {
        //            bob.append(prefix + "Parameters interface:\n");
        //            bob.append(prefix + "    " + this.getParametersService().getServiceType().getName());
        //            bob.append("\n");
        //        }

        // provided services
        if (!this.getProvidedServicePorts().isEmpty()) {
            bob.append(prefix).append("Provided services:\n");
            for (ProvidedServicePort implService : this.getProvidedServicePorts()) {
                StringBuilder modifiersBob = new StringBuilder();
                implService.getModifiers().forEach(mod -> modifiersBob.append(mod.toString()).append(" "));
                modifiersBob.delete(modifiersBob.length() - 1, modifiersBob.length());

                bob.append(prefix).append("    ").append("(").append(modifiersBob).append(") ").append(implService.getClazz().getTypeName());
                bob.append("\n");
            }
        }

        // used services
        if (!this.getUsedServicePorts().isEmpty()) {
            bob.append(prefix).append("Used services:\n");
            for (UsedServicePort usedService : this.getUsedServicePorts()) {
                StringBuilder modifiersBob = new StringBuilder();
                usedService.getModifiers().forEach(mod -> modifiersBob.append(mod.toString()).append(" "));
                modifiersBob.delete(modifiersBob.length() - 1, modifiersBob.length());

                bob.append(prefix).append("    ").append("(").append(modifiersBob).append(") ").append(usedService.getClazz().getTypeName()).append(usedService.isCardinalityMany() ? " []" : "");
                bob.append("\n");
            }
        }

        // event sources
        if (!this.getEventsSourcePorts().isEmpty()) {
            bob.append(prefix).append("Events sources:\n");
            for (EventsSourcePort eventsSourcePort : this.getEventsSourcePorts()) {
                StringBuilder modifiersBob = new StringBuilder();
                eventsSourcePort.getModifiers().forEach(mod -> modifiersBob.append(mod.toString()).append(" "));
                modifiersBob.delete(modifiersBob.length() - 1, modifiersBob.length());

                bob.append(prefix).append("    ").append("(").append(modifiersBob).append(") ").append(eventsSourcePort.getClazz().getTypeName());
                bob.append("\n");
            }
        }

        // event sinks
        if (!this.getEventsSinkPorts().isEmpty()) {
            bob.append(prefix).append("Events sinks:\n");
            for (EventsSinkPort eventsSinkPort : this.getEventsSinkPorts()) {
                StringBuilder modifiersBob = new StringBuilder();
                eventsSinkPort.getModifiers().forEach(mod -> modifiersBob.append(mod.toString()).append(" "));
                modifiersBob.delete(modifiersBob.length() - 1, modifiersBob.length());

                bob.append(prefix).append("    ").append("(").append(modifiersBob).append(") ").append(eventsSinkPort.getClazz().getTypeName());
                bob.append("\n");
            }
        }

        // composed components
        if (!this.getComposedComponents().isEmpty()) {
            bob.append(subSepLineMaker).append("\n");
            bob.append(prefix).append("Composed components:").append("\n");

            for (Component composedComponent : this.getComposedComponents()) {
                //                // get sub description
                //                String subDescString = composedComponent.toString();
                //                subDescString = subDescString.replaceAll("\n", "\n" + prefix);
                //                // remove last prefix
                //                subDescString = subDescString.substring(0, subDescString.length() - prefix.length());

                bob.append(prefix).append("    ").append(composedComponent.getClazz().toString()).append("\n");
            }
        }

        bob.append(sepLineMaker);

        /*
         * post-production
         */

        // extract lines
        String[] lines = bob.toString().split("\n");

        // get max line length
        int max = 0;
        for (String line : lines) {
            int length = line.length();
            if (length > max) max = length;
        }
        max += 5;

        // build the sep & sub sep lines
        StringBuilder sepLineString = new StringBuilder();
        for (int i = 0; i < max; i++)
            sepLineString.append("=");

        StringBuilder subSepLineString = new StringBuilder("|");
        for (int i = 1; i < max - 1; i++)
            subSepLineString.append("-");
        subSepLineString.append("|");

        // rebuilt one single string w/ normalized lines and replaced markers
        StringBuilder result = new StringBuilder();
        for (String line : lines) {
            // test sep line marker
            if (line.contains(sepLineMaker)) result.append(sepLineString).append("\n");
            else if (line.contains(subSepLineMaker)) result.append(subSepLineString).append("\n");
            else {
                // add line
                result.append(line);

                // normalize line
                int lineLength = line.length();
                if (lineLength == 0) result.append("|");
                else result.append(" ");
                if (lineLength < max) for (int i = 1; i < max - lineLength - 1; i++)
                    result.append(" ");
                result.append("|\n");
            }
        }
        return result.toString();
    }
}
