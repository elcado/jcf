/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.lang.component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Meta description of a component's type.
 */
public class Type implements Serializable {
    private transient List<Class<?>> providedServices = new ArrayList<>();
    private transient List<Class<?>> producedEvents = new ArrayList<>();

    private final List<Component> implementations = new ArrayList<>();
    private Class<?> clazz;

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public List<Component> getImplementations() {
        return implementations;
    }

    public void addImplementation(Component aComponent) {
        this.implementations.add(aComponent);
    }

    public List<Class<?>> getProvidedServices() {
        return providedServices;
    }

    public void setProvidedServices(List<Class<?>> providedServices) {
        this.providedServices = providedServices;
    }

    public List<Class<?>> getProducedEvents() {
        return producedEvents;
    }

    public void setProducedEvents(List<Class<?>> producedEvents) {
        this.producedEvents = producedEvents;
    }

    @Override
    public String toString() {
        Collector<CharSequence, ?, String> joiner = Collectors.joining(", ", "[", "]");
        String providedServicesAsString = this.providedServices.stream().map(Class::getTypeName).collect(joiner);
        String producedEventsAsString = this.producedEvents.stream().map(Class::getTypeName).collect(joiner);
        return "Type { " + "clazz=" + clazz + ", " +
                "providesTypeMirror=" + providedServicesAsString + ", producesTypeMirror=" + producedEventsAsString + " }";
    }

    public String toLongString() {
        String prefix = "|    ";
        String sepLineMaker = "<sep_line>";
        String subSepLineMaker = "<sub_sep_line>";

        StringBuilder bob = new StringBuilder();

        bob.append(sepLineMaker).append("\n");

        // component class
        bob.append(prefix).append("Class:    ").append(this.getClazz().toString()).append("\n");

        // contract
        bob.append(subSepLineMaker).append("\n");

        // provided services
        if (!this.providedServices.isEmpty()) {
            bob.append(prefix).append("Provided services:\n");
            for (Class<?> providedService : this.providedServices) {
                bob.append(prefix).append("    ").append(providedService.getTypeName());
                bob.append("\n");
            }
        }

        // produced events
        if (!this.producedEvents.isEmpty()) {
            bob.append(prefix).append("Produced events:\n");
            for (Class<?> producedEvent : this.producedEvents) {
                bob.append(prefix).append("    ").append(producedEvent.getTypeName());
                bob.append("\n");
            }
        }

        bob.append(sepLineMaker);

        /*
         * post-production
         */

        // extract lines
        String[] lines = bob.toString().split("\n");

        // get max line length
        int max = 0;
        for (String line : lines) {
            int length = line.length();
            if (length > max) max = length;
        }
        max += 5;

        // build the sep & sub sep lines
        StringBuilder sepLineString = new StringBuilder();
        for (int i = 0; i < max; i++)
            sepLineString.append("=");

        StringBuilder subSepLineString = new StringBuilder("|");
        for (int i = 1; i < max - 1; i++)
            subSepLineString.append("-");
        subSepLineString.append("|");

        // rebuilt one single string w/ normalized lines and replaced markers
        StringBuilder result = new StringBuilder();
        for (String line : lines) {
            // test sep line marker
            if (line.contains(sepLineMaker)) result.append(sepLineString).append("\n");
            else if (line.contains(subSepLineMaker)) result.append(subSepLineString).append("\n");
            else {
                // add line
                result.append(line);

                // normalize line
                int lineLength = line.length();
                if (lineLength == 0) result.append("|");
                else result.append(" ");
                if (lineLength < max) for (int i = 1; i < max - lineLength - 1; i++)
                    result.append(" ");
                result.append("|\n");
            }
        }
        return result.toString();
    }
}
