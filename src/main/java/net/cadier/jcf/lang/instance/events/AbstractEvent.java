/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.lang.instance.events;

import net.cadier.jcf.lang.component.port.events.EventsPort;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.utils.reflection.ReflectionException;
import net.cadier.utils.reflection.ReflectionUtils;

import javax.lang.model.element.Modifier;
import java.util.Set;

public abstract class AbstractEvent {
    protected final Instance instance;

    protected final EventsPort port;

    public AbstractEvent(final Instance instance, final EventsPort port) {
        this.instance = instance;
        this.port = port;
    }

    public Class<?> getEventClazz() {
        return this.port.getClazz();
    }

    public Set<Modifier> getModifiers() {
        return this.port.getModifiers();
    }

    public Object getObject() {
        Object object = null;
        try {
            object = ReflectionUtils.getFieldValue(this.instance.getObject(), this.port.getName());
        } catch (ReflectionException e) {
            e.printStackTrace();
        }
        return object;
    }

    public void setObject(final Object value) {
        try {
            ReflectionUtils.setFieldValue(this.instance.getObject(), this.port.getName(), value);
        } catch (ReflectionException e) {
            e.printStackTrace();
        }
    }

}
