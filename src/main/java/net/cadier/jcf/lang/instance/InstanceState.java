/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.lang.instance;

import net.cadier.actions.Action;
import net.cadier.jcf.exception.IllegalComponentStateTransitionException;
import net.cadier.jcf.lang.component.Failable;
import net.cadier.jcf.lang.component.Initializable;
import net.cadier.jcf.lang.component.Startable;

public abstract class InstanceState {
    /*
     * The instance states { IDLE, INITIALIZED, STARTED, PAUSED, RESUMED, STOPPED, DISPOSED, FAILED }
     */
    public static final InstanceState IDLE = new InstanceState() {
        @Override
        public void init(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterInit(instance, enterStateAction);
        }
    };

    public static final InstanceState INITIALIZED = new InstanceState() {
        @Override
        public void start(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterStart(instance, enterStateAction);
        }

        @Override
        public void dispose(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterDispose(instance, enterStateAction);
        }
    };

    public static final InstanceState STARTED = new InstanceState() {
        @Override
        public void pause(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterPause(instance, enterStateAction);
        }

        @Override
        public void stop(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterStop(instance, enterStateAction);
        }
    };

    public static final InstanceState PAUSED = new InstanceState() {
        @Override
        public void resume(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterResume(instance, enterStateAction);
        }

        @Override
        public void stop(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterStop(instance, enterStateAction);
        }
    };

    public static final InstanceState RESUMED = new InstanceState() {
        @Override
        public void pause(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterPause(instance, enterStateAction);
        }

        @Override
        public void stop(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterStop(instance, enterStateAction);
        }
    };

    public static final InstanceState STOPPED = new InstanceState() {

        @Override
        public void start(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterStart(instance, enterStateAction);
        }

        @Override
        public void dispose(final Instance instance, final Action<Instance> enterStateAction) {
            this.enterDispose(instance, enterStateAction);
        }
    };

    public static final InstanceState DISPOSED = new InstanceState() { };

    public static final InstanceState FAILED = new InstanceState() { };

    /*
     *  The transition triggering events { init, start, pause, resume, stop, dispose }
     */

    public void init(final Instance instance, final Action<Instance> enterStateAction) throws IllegalComponentStateTransitionException {
        throw new IllegalComponentStateTransitionException(this, InstanceState.INITIALIZED, instance.getInstanceId());
    }

    public void start(final Instance instance, final Action<Instance> enterStateAction) throws IllegalComponentStateTransitionException {
        throw new IllegalComponentStateTransitionException(this, InstanceState.STARTED, instance.getInstanceId());
    }

    public void pause(final Instance instance, final Action<Instance> enterStateAction) throws IllegalComponentStateTransitionException {
        throw new IllegalComponentStateTransitionException(this, InstanceState.PAUSED, instance.getInstanceId());
    }

    public void resume(final Instance instance, final Action<Instance> enterStateAction) throws IllegalComponentStateTransitionException {
        throw new IllegalComponentStateTransitionException(this, InstanceState.RESUMED, instance.getInstanceId());
    }

    public void stop(final Instance instance, final Action<Instance> enterStateAction) throws IllegalComponentStateTransitionException {
        throw new IllegalComponentStateTransitionException(this, InstanceState.STOPPED, instance.getInstanceId());
    }

    public void dispose(final Instance instance, final Action<Instance> enterStateAction) throws IllegalComponentStateTransitionException {
        throw new IllegalComponentStateTransitionException(this, InstanceState.DISPOSED, instance.getInstanceId());
    }

    @Override
    public String toString() {
        String result = "UNKNOWN";

        if (InstanceState.IDLE.equals(this)) {
            result = "IDLE";
        } else if (InstanceState.INITIALIZED.equals(this)) {
            result = "INITIALIZED";
        } else if (InstanceState.STARTED.equals(this)) {
            result = "STARTED";
        } else if (InstanceState.PAUSED.equals(this)) {
            result = "PAUSED";
        } else if (InstanceState.RESUMED.equals(this)) {
            result = "RESUMED";
        } else if (InstanceState.STOPPED.equals(this)) {
            result = "STOPPED";
        } else if (InstanceState.DISPOSED.equals(this)) {
            result = "DISPOSED";
        } else if (InstanceState.FAILED.equals(this)) {
            result = "FAILED";
        }
        return result;
    }

    protected void enterInit(Instance instance, final Action<Instance> enterStateAction) {
        // invoke enter state action and fail instance if false
        if (enterStateAction != null && !enterStateAction.execute(instance)) {
            this.enterFail(instance);
        }

        // invoke user listener
        Initializable initializableFacet = this.getInitializableFacet(instance);
        if (initializableFacet != null && !initializableFacet.onInitialized()) {
            this.enterFail(instance);
        }

        // set state if not failed
        if (!InstanceState.FAILED.equals(instance.getCurrentInstanceState())) {
            instance.setCurrentInstanceState(InstanceState.INITIALIZED);
        }
    }

    protected void enterStart(Instance instance, final Action<Instance> enterStateAction) {
        // invoke enter state action and fail instance if false
        if (enterStateAction != null && !enterStateAction.execute(instance)) {
            this.enterFail(instance);
        }

        // invoke user listener
        Startable startableFacet = this.getStartableFacet(instance);
        if (startableFacet != null && !startableFacet.onStarted()) {
            this.enterFail(instance);
        }

        // set state if not failed
        if (!InstanceState.FAILED.equals(instance.getCurrentInstanceState())) {
            instance.setCurrentInstanceState(InstanceState.STARTED);
        }
    }

    protected void enterPause(Instance instance, final Action<Instance> enterStateAction) {
        // invoke enter state action and fail instance if false
        if (enterStateAction != null && !enterStateAction.execute(instance)) {
            this.enterFail(instance);
        }

        // invoke user listener
        Startable startableFacet = this.getStartableFacet(instance);
        if (startableFacet != null && !startableFacet.onPaused()) {
            this.enterFail(instance);
        }

        // set state if not failed
        if (!InstanceState.FAILED.equals(instance.getCurrentInstanceState())) {
            instance.setCurrentInstanceState(InstanceState.PAUSED);
        }
    }

    protected void enterResume(Instance instance, final Action<Instance> enterStateAction) {
        // invoke enter state action and fail instance if false
        if (enterStateAction != null && !enterStateAction.execute(instance)) {
            this.enterFail(instance);
        }

        // invoke user listener
        Startable startableFacet = this.getStartableFacet(instance);
        if (startableFacet != null && !startableFacet.onResumed()) {
            this.enterFail(instance);
        }

        // set state if not failed
        if (!InstanceState.FAILED.equals(instance.getCurrentInstanceState())) {
            instance.setCurrentInstanceState(InstanceState.RESUMED);
        }
    }

    protected void enterStop(Instance instance, final Action<Instance> enterStateAction) {
        // invoke enter state action and fail instance if false
        if (enterStateAction != null && !enterStateAction.execute(instance)) {
            this.enterFail(instance);
        }

        // invoke user listener
        Startable startableFacet = this.getStartableFacet(instance);
        if (startableFacet != null && !startableFacet.onStopped()) {
            this.enterFail(instance);
        }

        // set state if not failed
        if (!InstanceState.FAILED.equals(instance.getCurrentInstanceState())) {
            instance.setCurrentInstanceState(InstanceState.STOPPED);
        }
    }

    protected void enterDispose(Instance instance, final Action<Instance> enterStateAction) {
        // invoke enter state action and fail instance if false
        if (enterStateAction != null && !enterStateAction.execute(instance)) {
            this.enterFail(instance);
        }

        // invoke user listener
        Initializable initializableFacet = this.getInitializableFacet(instance);
        if (initializableFacet != null && !initializableFacet.onDisposed()) {
            this.enterFail(instance);
        }

        // set state if not failed
        if (!InstanceState.FAILED.equals(instance.getCurrentInstanceState())) {
            instance.setCurrentInstanceState(InstanceState.DISPOSED);
        }
    }

    private void enterFail(final Instance instance) {
        // invoke user listener
        Failable failableFacet = this.getFailableFacet(instance);
        if (failableFacet != null) {
            failableFacet.onFailed();
        }

        // set state
        instance.setCurrentInstanceState(InstanceState.FAILED);
    }

    private Failable getFailableFacet(final Instance anInstance) {
        Failable failableFacet = null;

        if ((anInstance.getComponent().isInitializable() || anInstance.getComponent().isStartable()) && anInstance.getObject() != null) {
            failableFacet = (Failable) anInstance.getObject();
        }
        return failableFacet;
    }

    private Initializable getInitializableFacet(final Instance anInstance) {
        Initializable initializableFacet = null;

        if (anInstance.getComponent().isInitializable() && anInstance.getObject() != null) {
            initializableFacet = (Initializable) anInstance.getObject();
        }
        return initializableFacet;
    }

    private Startable getStartableFacet(final Instance anInstance) {
        Startable startableFacet = null;

        if (anInstance.getComponent().isStartable() && anInstance.getObject() != null) {
            startableFacet = (Startable) anInstance.getObject();
        }
        return startableFacet;
    }

}
