/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.instancesManager.impl.parametersManager.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.ProvidedService;
import net.cadier.jcf.core.instancesManager.impl.parametersManager.ParametersManager;
import net.cadier.jcf.core.instancesManager.impl.parametersManager.ParametersManagerService;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.events.EventsDispatcher;
import net.cadier.jcf.lang.instance.events.SinkQueue;
import net.cadier.jcf.lang.instance.parameters.Parameters;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@ComponentImplementation
public class ParametersManagerImpl implements ParametersManager, ParametersManagerService {
    @ProvidedService
    public final transient ParametersManagerService parametersManagerService = this;

    private final Properties globalProperties = new Properties();

    private final Map<InstanceId, Properties> instancesProperties = new HashMap<>();

    private final Map<Class, EventsDispatcher> eventsDispatchers = new HashMap<>();

    private ParametersManagerImpl() {
    }

    @Override
    public <T extends Enum<T>> Parameters<T> newParameters(final Instance anInstance) {
        // prepare instance's properties
        this.instancesProperties.putIfAbsent(anInstance.getInstanceId(), new Properties());

        return new Parameters<T>() {
            @Override
            @SuppressWarnings("unchecked")
            public <V> V get(T param) {
                return (V) ParametersManagerImpl.this.globalProperties.get(param);
            }

            @Override
            @SuppressWarnings("unchecked")
            public <V> V set(T param, V value) {
                V previousValue;
                if (value == null) {
                    previousValue = (V) ParametersManagerImpl.this.globalProperties.remove(param);
                } else {
                    previousValue = (V) ParametersManagerImpl.this.globalProperties.put(param, value);
                }

                tryPostEvent(param);

                return previousValue;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <V> V get(InstanceId instanceId, T param) {
                return (V) ParametersManagerImpl.this.instancesProperties.get(instanceId).get(param);
            }

            @Override
            @SuppressWarnings("unchecked")
            public <V> V set(InstanceId instanceId, T param, V value) {
                V previousValue;
                if (value == null) {
                    previousValue = (V) ParametersManagerImpl.this.instancesProperties.get(instanceId).remove(param);
                } else {
                    previousValue = (V) ParametersManagerImpl.this.instancesProperties.get(instanceId).put(param, value);
                }

                tryPostEvent(param);

                return previousValue;
            }
        };
    }

    private <T extends Enum<T>> void tryPostEvent(T param) {
        EventsDispatcher eventsDispatcher = eventsDispatchers.get(param.getClass());
        if (eventsDispatcher != null) {
            eventsDispatcher.postEvent(param);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addSinkQueue(final Class paramType, final SinkQueue sinkQueue) {
        this.eventsDispatchers.computeIfAbsent(paramType, aClass -> new EventsDispatcher()).addSinkQueue(sinkQueue);
    }
}
