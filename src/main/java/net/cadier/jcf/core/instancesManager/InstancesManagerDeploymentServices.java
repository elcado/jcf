/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.instancesManager;

import net.cadier.jcf.annotations.ComponentService;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.exception.DeploymentRuleException;
import net.cadier.jcf.lang.component.Component;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.utils.reflection.ReflectionException;

import java.util.List;

@ComponentService
public interface InstancesManagerDeploymentServices {
    /**
     * Deploy an existing, or a new (if object arg is null), instance of a registered component.
     *
     * @param parentId  The instance id of the parent of the deployed instance.
     * @param component The component to deploy.
     * @param object    An existing instance of the component or null if it's to be built.
     * @return the instance id of the deployed instance. Null if the component is deployment rules are not met or if
     * an instance cannot be built.
     * @throws DependencyResolutionException
     * @throws DeploymentRuleException
     * @throws ReflectionException
     */
    InstanceId deployInstance(final InstanceId parentId, Component component, Object object) throws DependencyResolutionException, DeploymentRuleException, ReflectionException;

    /**
     * Undeploy an instance.
     *
     * @param instanceId The instance id of the instance to remove.
     * @return the list of undeployed instances' ids
     */
    List<InstanceId> undeployInstance(final InstanceId instanceId);

}
