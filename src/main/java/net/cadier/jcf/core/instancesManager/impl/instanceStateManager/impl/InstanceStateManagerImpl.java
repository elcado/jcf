/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.instancesManager.impl.instanceStateManager.impl;

import net.cadier.actions.Action;
import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.EventSource;
import net.cadier.jcf.annotations.ProvidedService;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.core.instancesManager.InstancesManagerEvent;
import net.cadier.jcf.core.instancesManager.InstancesManagerEvent.InstancesManagerEventType;
import net.cadier.jcf.core.instancesManager.impl.instanceStateManager.InstanceStateManager;
import net.cadier.jcf.core.instancesManager.impl.instanceStateManager.InstanceStateManagerServices;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.exception.IllegalComponentStateTransitionException;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceState;
import net.cadier.jcf.lang.instance.events.Source;

@ComponentImplementation
public class InstanceStateManagerImpl implements InstanceStateManager, InstanceStateManagerServices {
    @ProvidedService
    public final transient InstanceStateManagerServices instanceStateManagerServices = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @EventSource
    public final transient Source<InstancesManagerEvent> instancesManagerEventsSource = null;

    private InstanceStateManagerImpl() {
    }

    @Override
    public boolean initInstance(final Instance anInstance, final Action<Instance> enterStateAction) {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Initializing " + anInstance.getInstanceId());
        }

        // try to initialize from current state
        return this.doTransition(anInstance, anInstance.getCurrentInstanceState()::init,
                InstanceState.INITIALIZED, enterStateAction);
    }

    @Override
    public boolean startInstance(final Instance anInstance, final Action<Instance> enterStateAction) {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Starting " + anInstance.getInstanceId());
        }

        // try to start instance from current state
        return this.doTransition(anInstance, anInstance.getCurrentInstanceState()::start,
                InstanceState.STARTED, enterStateAction);
    }

    @Override
    public boolean pauseInstance(final Instance anInstance, final Action<Instance> enterStateAction) {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Pausing " + anInstance.getInstanceId());
        }

        // try to pause instance from current state
        return this.doTransition(anInstance, anInstance.getCurrentInstanceState()::pause,
                InstanceState.PAUSED, enterStateAction);
    }

    @Override
    public boolean resumeInstance(final Instance anInstance, final Action<Instance> enterStateAction) {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Resuming " + anInstance.getInstanceId());
        }

        // try to resume instance from current state
        return this.doTransition(anInstance, anInstance.getCurrentInstanceState()::resume,
                InstanceState.RESUMED, enterStateAction);
    }

    @Override
    public boolean stopInstance(final Instance anInstance, final Action<Instance> enterStateAction) {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Stopping " + anInstance.getInstanceId());
        }

        // try to stop instance from current state
        return this.doTransition(anInstance, anInstance.getCurrentInstanceState()::stop,
                InstanceState.STOPPED, enterStateAction);
    }

    @Override
    public boolean disposeInstance(final Instance anInstance, final Action<Instance> enterStateAction) {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Disposing " + anInstance.getInstanceId());
        }

        // try to dispose instance from current state
        return this.doTransition(anInstance, anInstance.getCurrentInstanceState()::dispose,
                InstanceState.DISPOSED, enterStateAction);
    }

    @Override
    public boolean isInitialized(final Instance anInstance) {
        return InstanceState.INITIALIZED.equals(anInstance.getCurrentInstanceState());
    }

    @Override
    public boolean isStarted(final Instance anInstance) {
        return InstanceState.STARTED.equals(anInstance.getCurrentInstanceState());
    }

    @Override
    public boolean isPaused(final Instance anInstance) {
        return InstanceState.PAUSED.equals(anInstance.getCurrentInstanceState());
    }

    @Override
    public boolean isResumed(final Instance anInstance) {
        return InstanceState.RESUMED.equals(anInstance.getCurrentInstanceState());
    }

    @Override
    public boolean isStopped(final Instance anInstance) {
        return InstanceState.STOPPED.equals(anInstance.getCurrentInstanceState());
    }

    @Override
    public boolean isDisposed(final Instance anInstance) {
        return InstanceState.DISPOSED.equals(anInstance.getCurrentInstanceState());
    }

    @Override
    public boolean isFailed(Instance anInstance) {
        return InstanceState.FAILED.equals(anInstance.getCurrentInstanceState());
    }

    private boolean doTransition(final Instance anInstance, final NextStateSignal nextStateSignal,
                                 final InstanceState expectedState, final Action<Instance> enterStateAction) {
        // try transition
        try  {
            nextStateSignal.accept(anInstance, enterStateAction);
        } catch (IllegalComponentStateTransitionException icse) {
            // transition is not allowed from current state to expected state
            if (this.loggingServices != null) {
                this.loggingServices.error(icse.getMessage());
                // no exception: error is handled by testing the resulting state
            }
        }

        // notify for new current instance state
        if (this.instancesManagerEventsSource != null) {
            InstancesManagerEventType eventType = this.getEventTypeFromCurrentInstanceState(anInstance);
            this.instancesManagerEventsSource.postEvent(new InstancesManagerEvent(eventType, anInstance.getInstanceId()));
        }

        // transition is only ok if new current state is now the supplied expected state
        InstanceState currentStateAfterTransition = anInstance.getCurrentInstanceState();
        return expectedState.equals(currentStateAfterTransition);
    }

    /**
     * Consumer that can throw IllegalComponentStateTransitionException.
     */
    interface NextStateSignal {
        void accept(final Instance anInstance, final Action<Instance> enterStateAction) throws IllegalComponentStateTransitionException;
    }

    private InstancesManagerEventType getEventTypeFromCurrentInstanceState(Instance anInstance) {
        InstancesManagerEventType eventType = null;
        if (this.isInitialized(anInstance)) {
            eventType = InstancesManagerEventType.INSTANCE_INITIALIZED;
        } else if (this.isStarted(anInstance)) {
            eventType = InstancesManagerEventType.INSTANCE_STARTED;
        } else if (this.isPaused(anInstance)) {
            eventType = InstancesManagerEventType.INSTANCE_PAUSED;
        } else if (this.isResumed(anInstance)) {
            eventType = InstancesManagerEventType.INSTANCE_RESUMED;
        } else if (this.isStopped(anInstance)) {
            eventType = InstancesManagerEventType.INSTANCE_STOPPED;
        } else if (this.isDisposed(anInstance)) {
            eventType = InstancesManagerEventType.INSTANCE_DISPOSED;
        } else if (this.isFailed(anInstance)) {
            eventType = InstancesManagerEventType.INSTANCE_FAILED;
        }

        return eventType;
    }

}
