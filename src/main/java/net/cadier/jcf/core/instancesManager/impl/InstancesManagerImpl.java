/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.instancesManager.impl;

import net.cadier.actions.Action;
import net.cadier.jcf.annotations.*;
import net.cadier.jcf.core.instancesManager.*;
import net.cadier.jcf.core.instancesManager.impl.dependenciesResolver.DependenciesResolverServices;
import net.cadier.jcf.core.instancesManager.impl.instanceStateManager.InstanceStateManagerServices;
import net.cadier.jcf.core.instancesManager.impl.parametersManager.ParametersManagerService;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.exception.DeploymentRuleException;
import net.cadier.jcf.lang.component.Component;
import net.cadier.jcf.lang.component.port.events.EventsSinkPort;
import net.cadier.jcf.lang.component.port.events.EventsSourcePort;
import net.cadier.jcf.lang.component.port.parameters.ParametersPort;
import net.cadier.jcf.lang.component.port.services.ProvidedServicePort;
import net.cadier.jcf.lang.component.port.services.UsedServicePort;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.events.*;
import net.cadier.jcf.lang.instance.parameters.Parameters;
import net.cadier.jcf.lang.instance.services.ImplementedService;
import net.cadier.utils.reflection.ReflectionException;
import net.cadier.utils.reflection.ReflectionUtils;
import net.cadier.utils.tree.Tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@ComponentImplementation
public class InstancesManagerImpl implements InstancesManager, InstancesManagerDeploymentServices,
        InstancesManagerLifeCycleServices, InstancesManagerDebugServices {
    private final Tree<InstanceId, Instance> instancesTree = new Tree<>();

    @ProvidedService
    public final transient InstancesManagerDeploymentServices instancesManagerDeployementServices = this;

    @ProvidedService
    public final transient InstancesManagerLifeCycleServices instancesManagerLifeCycleServices = this;

    @ProvidedService
    public final transient InstancesManagerDebugServices instancesManagerDebugServices = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @UsedService
    private final transient DependenciesResolverServices dependenciesResolverServices = null;

    @UsedService
    private final transient ParametersManagerService parametersManagerService = null;

    @UsedService
    private final transient InstanceStateManagerServices instanceStateManagerServices = null;

    @EventSink
    private final transient Sink<InstancesManagerEvent> instancesManagerEventsSink = event -> {
        Instance instance = instancesTree.getRef(event.getInstanceId());

        // debug log
        if (this.loggingServices != null) {
            this.loggingServices.debug(event.getEventType() + " received for [" + instance.getInstanceId() + "]");
        }

        // specific event action
        switch (event.getEventType()) {
            case INSTANCE_INITIALIZED:
                break;
            case INSTANCE_STARTED:
                break;
            case INSTANCE_PAUSED:
                break;
            case INSTANCE_RESUMED:
                break;
            case INSTANCE_STOPPED:
                break;
            case INSTANCE_DISPOSED:
                break;
            case INSTANCE_FAILED:
                break;
        }
    };

    private InstancesManagerImpl() {
    }

    public Tree<InstanceId, Instance> getInstancesTree() {
        return this.instancesTree;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public InstanceId deployInstance(final InstanceId parentId, Component component, Object object) throws DeploymentRuleException, DependencyResolutionException, ReflectionException {
        // check deployment rules before doing anything
        this.checkDeploymentRules(parentId, component);

        InstanceId instanceId = null;

        // build component instance's object if null is provided
        if (object == null) {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Building object of " + component.getClazz());
            }

            object = ReflectionUtils.buildObject(component.getClazz());
        }

        // log
        if (this.loggingServices != null) {
            this.loggingServices.debug("Deploying object " + object + " of " + component.getClazz() + (parentId == null ? " as a root instance" : " under " + parentId));
        }

        // build instance upon object (which cannot be null because it would have raised a ReflectionException)
        Instance instance = this.buildInstance(component, object);
        instanceId = instance.getInstanceId();

        // deploy instance
        this.deployInstanceInTree(parentId, component, instance);

        return instanceId;
    }

    @Override
    public void initInstance(final InstanceId instanceId) {
        this.initInstanceAndChildren(instanceId);
    }

    @Override
    public void startInstance(final InstanceId instanceId) {
        this.startInstanceAndChildren(instanceId);
    }

    @Override
    public void pauseInstance(final InstanceId instanceId) {
        this.pauseInstanceAndChildren(instanceId);
    }

    @Override
    public void resumeInstance(final InstanceId instanceId) {
        this.resumeInstanceAndChildren(instanceId);
    }

    @Override
    public void stopInstance(final InstanceId instanceId) {
        this.stopInstanceAndChildren(instanceId);
    }

    public List<InstanceId> undeployInstance(final InstanceId instanceId) {
        // recursively undeploy instances
        List<InstanceId> removedIds = new ArrayList<>();
        this.undeployInstance(instanceId, removedIds);
        return removedIds;
    }

    @Override
    public boolean isInitialized(InstanceId instanceId) {
        Instance instance = this.instancesTree.getRef(instanceId);
        return this.instanceStateManagerServices.isInitialized(instance);
    }

    @Override
    public boolean isStarted(InstanceId instanceId) {
        Instance instance = this.instancesTree.getRef(instanceId);
        return this.instanceStateManagerServices.isStarted(instance);
    }

    @Override
    public boolean isPaused(InstanceId instanceId) {
        Instance instance = this.instancesTree.getRef(instanceId);
        return this.instanceStateManagerServices.isPaused(instance);
    }

    @Override
    public boolean isResumed(InstanceId instanceId) {
        Instance instance = this.instancesTree.getRef(instanceId);
        return this.instanceStateManagerServices.isResumed(instance);
    }

    @Override
    public boolean isStopped(InstanceId instanceId) {
        Instance instance = this.instancesTree.getRef(instanceId);
        return this.instanceStateManagerServices.isStopped(instance);
    }

    @Override
    public boolean isFailed(InstanceId instanceId) {
        Instance instance = this.instancesTree.getRef(instanceId);
        return this.instanceStateManagerServices.isFailed(instance);
    }

    /**
     * Recursively initializes supplied instance.
     *
     * @param instanceId The instance id of the instance to initialize.
     * @return true if instance has been initialized, false otherwise
     */
    private boolean initInstanceAndChildren(InstanceId instanceId) {
        // first init children
        List<InstanceId> childrenIds = this.instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.initInstanceAndChildren(childId);
        }

        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = this.instancesTree.getRef(instanceId);

            // initialize instance
            isTransitionOK = this.instanceStateManagerServices.initInstance(instance, null);
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.error("Cannot initialize " + instanceId + " because one of its children cannot initialize.");
                // no exception because an IllegalComponentStateTransitionException has already been logged
            }
        }

        // return boolean for recursive calls
        return isTransitionOK;
    }

    /**
     * Recursively starts supplied instance.
     *
     * @param instanceId The instance id of the instance to start.
     * @return true if instance has been started, false otherwise
     */
    private boolean startInstanceAndChildren(InstanceId instanceId) {
        // first start children
        List<InstanceId> childrenIds = this.instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.startInstanceAndChildren(childId);
        }

        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = this.instancesTree.getRef(instanceId);

            // start instance
            // do nothing if instance is already started
            if (this.instanceStateManagerServices.isStarted(instance)) {
                isTransitionOK = true;
            } else {
                Action<Instance> startAction = Action.execute(this::startSinksAndRemoteServices).failOn(this::stopSinksAndRemoteServices);
                isTransitionOK = this.instanceStateManagerServices.startInstance(instance, startAction);
            }
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.error("Cannot start " + instanceId + " because one of its children cannot start.");
                // no exception because an IllegalComponentStateTransitionException has already been logged
            }
        }

        // return boolean for recursive calls
        return isTransitionOK;
    }

    /**
     * Recursively pauses supplied instance.
     *
     * @param instanceId The instance id of the instance to pause.
     * @return true if instance has been paused, false otherwise
     */
    private boolean pauseInstanceAndChildren(InstanceId instanceId) {
        // first pause children
        List<InstanceId> childrenIds = this.instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.pauseInstanceAndChildren(childId);
        }

        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = this.instancesTree.getRef(instanceId);

            // do nothing if instance is already paused
            if (this.instanceStateManagerServices.isPaused(instance)) {
                isTransitionOK = true;
            } else {
                // TODO pause sinks and remote services

                // pause instance
                isTransitionOK = this.instanceStateManagerServices.pauseInstance(instance, null);
            }
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.error("Cannot pause " + instanceId + " because one of its children cannot pause.");
                // no exception because an IllegalComponentStateTransitionException has already been logged
            }
        }

        // return boolean for recursive calls
        return isTransitionOK;
    }

    /**
     * Recursively resumes supplied instance.
     *
     * @param instanceId The instance id of the instance to resume.
     * @return true if instance has been resumed, false otherwise
     */
    private boolean resumeInstanceAndChildren(InstanceId instanceId) {
        // first resume children
        List<InstanceId> childrenIds = this.instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.resumeInstanceAndChildren(childId);
        }

        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = this.instancesTree.getRef(instanceId);

            // do nothing if instance is already resumed
            if (this.instanceStateManagerServices.isResumed(instance)) {
                isTransitionOK = true;
            } else {
                // resume instance
                isTransitionOK = this.instanceStateManagerServices.resumeInstance(instance, null);
            }
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.error("Cannot resume " + instanceId + " because one of its children cannot resume.");
                // no exception because an IllegalComponentStateTransitionException has already been logged
            }
        }

        // return boolean for recursive calls
        return isTransitionOK;
    }

    /**
     * Recursively stops supplied instance.
     *
     * @param instanceId The instance id of the instance to stop.
     * @return true if instance has been stopped, false otherwise
     */
    private boolean stopInstanceAndChildren(InstanceId instanceId) {
        // first stop children
        List<InstanceId> childrenIds = this.instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.stopInstanceAndChildren(childId);
        }

        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = this.instancesTree.getRef(instanceId);

            // do nothing if instance is already stopped
            if (this.instanceStateManagerServices.isStopped(instance)) {
                isTransitionOK = true;
            } else {
                // stop instance
                Action<Instance> stopAction = Action.execute(this::stopSinksAndRemoteServices);
                isTransitionOK = this.instanceStateManagerServices.stopInstance(instance, stopAction);
            }
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.error("Cannot stop " + instanceId + " because one of its children cannot stop.");
                // no exception because an IllegalComponentStateTransitionException has already been logged
            }
        }

        // return boolean for recursive calls
        return isTransitionOK;
    }

    /**
     * @param parentId  The instance id of the parent of the deployed instance.
     * @param component The component to deploy.
     */
    private void checkDeploymentRules(final InstanceId parentId, Component component) throws DeploymentRuleException {
        if (parentId == null) {
            // to be deployed as a root instance, it has not to be already deployed (i.e. list of instance is empty)
            if (!component.getInstancesIds().isEmpty()) {
                throw new DeploymentRuleException(component.getClazz(), "Instance of '" + component.getClazz().getCanonicalName() + "' cannot be deployed as a root instance because it is already deployed.");
            }
        } else {
            // to be deployed as a regular instance, it has :
            //   - not to be already deployed
            //   - or to be deployed only as regular instances, i.e. every instance has a parent
            boolean canBeDeployed = component.getInstancesIds().isEmpty();
            canBeDeployed |= component.getInstancesIds().stream()
                    .map(this.instancesTree::getParent)
                    .allMatch(Objects::nonNull);

            if (!canBeDeployed) {
                throw new DeploymentRuleException(parentId, component.getClazz(), "Instance of '" + component.getClazz().getCanonicalName() + "' cannot be deployed under '" + parentId + "' because it is already deployed as a root instance.");
            }
        }
    }

    private Instance buildInstance(final Component component, final Object object) throws ReflectionException {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Building instance for object " + object);
        }

        Instance instance = new Instance(component, object);
        component.addInstance(instance);

        // inject the id field
        if (component.getIdPort() != null) {
            ReflectionUtils.setFieldValue(instance.getObject(), component.getIdPort().getName(),
                    instance.getInstanceId());
        }

        // set the implemented services
        for (ProvidedServicePort providedServicePort : component.getProvidedServicePorts()) {
            ImplementedService implementedService = new ImplementedService(instance, providedServicePort);
            instance.addImplementedService(implementedService);
        }

        // set used services
        for (UsedServicePort usedServicePort : component.getUsedServicePorts()) {
            net.cadier.jcf.lang.instance.services.UsedService usedService =
                    new net.cadier.jcf.lang.instance.services.UsedService(instance, usedServicePort);
            instance.addUsedService(usedService);
        }

        // set events sources
        for (EventsSourcePort eventsSourcePort : component.getEventsSourcePorts()) {
            EventsSource eventsSource = new EventsSource(instance, eventsSourcePort);

            // inject events dispatcher
            EventsDispatcher<Object> eventsDispatcher = new EventsDispatcher<>();
            eventsSource.setObject(eventsDispatcher);

            instance.addEventsSource(eventsSource);
        }

        // set events sinks
        for (EventsSinkPort eventsSinkPort : component.getEventsSinkPorts()) {
            EventsSink eventsSink = new EventsSink(instance, eventsSinkPort);

            // inject sink queue
            SinkQueue<?> sinkQueue = new SinkQueue<>((Sink<?>) eventsSink.getObject());
            eventsSink.setObject(sinkQueue);

            instance.addEventsSink(eventsSink);

            // if event data is also a parameter data, add sink queue to the ParametersManager
            ParameterData parameterData = eventsSinkPort.getClazz().getAnnotation(ParameterData.class);
            if (Objects.nonNull(parameterData)) {
                this.parametersManagerService.addSinkQueue(eventsSinkPort.getClazz(), sinkQueue);
            }
        }

        // inject parameters
        for (ParametersPort parametersPort : component.getParametersPorts()) {
            Parameters<?> parameters = this.parametersManagerService.newParameters(instance);
            ReflectionUtils.setFieldValue(instance.getObject(), parametersPort.getName(), parameters);
        }
        return instance;
    }

    private void deployInstanceInTree(InstanceId parentId, Component component, Instance instance) throws DependencyResolutionException, DeploymentRuleException, ReflectionException {
        // deploy instance
        this.putInstanceInTree(parentId, instance);

        // recursively deploy composed components' instances
        if (component.getComposedComponents() != null) {
            for (Component composedComponent : component.getComposedComponents()) {
                this.deployInstance(instance.getInstanceId(), composedComponent, null);
            }
        }

        // resolve dependencies
        this.resolveDependencies(instance);

        // logging
        if (this.loggingServices != null) {
            this.loggingServices.debug("Deployed instance " + instance.getInstanceId() + " [" + component.getClazz() + "]" + (parentId == null ? " as root in tree" : " under " + parentId + " in tree"));
        }
    }

    private void putInstanceInTree(final InstanceId parentId, final Instance instance) {
        // put instance in tree
        if (this.loggingServices != null) {
            this.loggingServices.debug("Put instance " + instance.getInstanceId() + " [" + instance.getComponent().getClazz() + "]" + (parentId == null ? " as root in tree" : " under " + parentId + " in tree"));
        }

        if (parentId == null) {
            this.instancesTree.addRoot(instance.getInstanceId(), instance);
        } else {
            this.instancesTree.addElement(parentId, instance.getInstanceId(), instance);
        }
    }

    private void resolveDependencies(final Instance instance) throws DependencyResolutionException {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Resolving dependencies for instance " + instance.getInstanceId());
        }

        this.dependenciesResolverServices.resolveDependencies(instance.getInstanceId(), this.instancesTree);
    }

    @SuppressWarnings("rawtypes")
    private void undeployInstance(final InstanceId instanceId, final List<InstanceId> removedIds) {
        // first check that instance is still in the tree (it might have been removed as a child of another instance)
        if (this.instancesTree.contains(instanceId)) {
            // undeploy children first
            for (InstanceId childId : this.instancesTree.getChildren(instanceId).toArray(new InstanceId[]{})) {
                this.undeployInstance(childId, removedIds);
            }

            // only undeploy stopped or failed instance
            Instance instance = this.instancesTree.getRef(instanceId);
            if (this.instanceStateManagerServices.isStopped(instance) || this.instanceStateManagerServices.isFailed(instance)) {
                if (this.loggingServices != null) {
                    this.loggingServices.debug("Undeploying instance [" + instanceId + "]");
                }

                // remove dependencies for instance
                this.dependenciesResolverServices.removeDependencies(instanceId, this.instancesTree);

                // remove instance from the tree
                Instance removedInstance = this.instancesTree.removeElement(instanceId);

                // remove instance from component
                removedInstance.getComponent().removeInstance(removedInstance);

                // add instance id to removed ids list
                removedIds.add(instanceId);

                // dispose instance
                this.instanceStateManagerServices.disposeInstance(removedInstance, null);
            } else {
                // TODO exception ?
                if (this.loggingServices != null) {
                    this.loggingServices.error("Cannot undeploy [" + instanceId + "] because it is neither stopped nor failed.");
                    // TODO throw exception
                }
            }
        }
    }

    private boolean startSinksAndRemoteServices(final Instance instance) {
        // start all instance's sinks
        for (EventsSink eventsSink : instance.getEventsSinks()) {
            ((SinkQueue) eventsSink.getObject()).startProcessingEvents();
        }

        return true;
    }

    private void stopSinksAndRemoteServices(final Instance instance) {
        // stop all instance's sinks
        for (EventsSink eventsSink : instance.getEventsSinks()) {
            ((SinkQueue) eventsSink.getObject()).stopProcessingEvents();
        }

        // unregister remote implemented services
        //        this.remoteManagerServices.stopRemoteImplementedServices(instance);
    }

}
