/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.instancesManager;

import net.cadier.jcf.annotations.ComponentService;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceId;

/**
 * Provides control over components' life cycles.
 *
 * @author Frédéric Cadier
 */
@ComponentService
public interface InstancesManagerLifeCycleServices {
    /**
     * Recursively initializes supplied instance.
     *
     * @param instanceId The instance id of the instance to initialize.
     */
    void initInstance(final InstanceId instanceId);

    /**
     * Check that instance is in {@link net.cadier.jcf.lang.instance.InstanceState#INITIALIZED INITIALIZED} state.
     *
     * @param instanceId instance id
     * @return true if instance is initialized
     */
    boolean isInitialized(final InstanceId instanceId);

    /**
     * Starts supplied instance if it is deployed and it implements {@link net.cadier.jcf.lang.component.Startable
     * Startable}. Do nothing otherwise.
     *
     * @param instanceId The instance id of the instance to start.
     */
    void startInstance(final InstanceId instanceId);

    /**
     * Check that instance is in {@link net.cadier.jcf.lang.instance.InstanceState#STARTED STARTED} state.
     *
     * @param instanceId instance id
     * @return true if instance is started
     */
    boolean isStarted(final InstanceId instanceId);

    /**
     * Pauses supplied instance if it is deployed and it implements {@link net.cadier.jcf.lang.component.Startable
     * Startable}. Do nothing otherwise.
     *
     * @param instanceId The instance id of the instance to pause.
     */
    void pauseInstance(final InstanceId instanceId);

    /**
     * Check that instance is in {@link net.cadier.jcf.lang.instance.InstanceState#PAUSED PAUSED} state.
     *
     * @param instanceId instance id
     * @return true if instance is paused
     */
    boolean isPaused(final InstanceId instanceId);

    /**
     * Resumes supplied instance if it is deployed and it implements {@link net.cadier.jcf.lang.component.Startable
     * Startable}. Do nothing otherwise.
     *
     * @param instanceId The instance id of the instance to resume.
     */
    void resumeInstance(final InstanceId instanceId);

    /**
     * Check that instance is in {@link net.cadier.jcf.lang.instance.InstanceState#RESUMED RESUMED} state.
     *
     * @param instanceId instance id
     * @return true if instance is resumed
     */
    boolean isResumed(final InstanceId instanceId);

    /**
     * Stops supplied instance if it is deployed and it implements {@link net.cadier.jcf.lang.component.Startable
     * Startable}. Do nothing otherwise.
     *
     * @param instanceId The instance id of the instance to stop.
     */
    void stopInstance(final InstanceId instanceId);

    /**
     * Check that instance is in {@link net.cadier.jcf.lang.instance.InstanceState#STOPPED STOPPED} state.
     *
     * @param instanceId instance id
     * @return true if instance is stopped
     */
    boolean isStopped(final InstanceId instanceId);

    /**
     * Check that instance is in {@link net.cadier.jcf.lang.instance.InstanceState#FAILED FAILED} state.
     *
     * @param instanceId instance id
     * @return true if instance is failed
     */
    boolean isFailed(InstanceId instanceId);

}
