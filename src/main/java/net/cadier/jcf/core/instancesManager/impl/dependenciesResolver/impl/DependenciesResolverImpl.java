/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.instancesManager.impl.dependenciesResolver.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.ProvidedService;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.core.instancesManager.impl.dependenciesResolver.DependenciesResolver;
import net.cadier.jcf.core.instancesManager.impl.dependenciesResolver.DependenciesResolverServices;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.events.EventsDispatcher;
import net.cadier.jcf.lang.instance.events.EventsSink;
import net.cadier.jcf.lang.instance.events.EventsSource;
import net.cadier.jcf.lang.instance.events.SinkQueue;
import net.cadier.jcf.lang.instance.services.ImplementedService;
import net.cadier.utils.tree.Tree;

import javax.lang.model.element.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ComponentImplementation
@SuppressWarnings("ConstantConditions")
public class DependenciesResolverImpl implements DependenciesResolver, DependenciesResolverServices {
    private Tree<InstanceId, Instance> instancesTree;

    @ProvidedService
    public final transient DependenciesResolverServices dependenciesResolverServices = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    private DependenciesResolverImpl() {
    }

    @Override
    public void resolveDependencies(final InstanceId anInstanceId, final Tree<InstanceId, Instance> anInstancesTree) throws DependencyResolutionException {
        this.instancesTree = anInstancesTree;

        this.resolveDependencies(anInstanceId);
    }

    @Override
    public void removeDependencies(final InstanceId anInstanceId, Tree<InstanceId, Instance> anInstancesTree) {
        this.instancesTree = anInstancesTree;

        this.removeDependencies(anInstanceId);
    }

    private void resolveDependencies(final InstanceId anInstanceId) throws DependencyResolutionException {
        Instance instance = this.instancesTree.getRef(anInstanceId);

        if (this.loggingServices != null) {
            this.loggingServices.debug("Resolving dependencies for " + anInstanceId + " [" + instance.getComponent().getClazz().getCanonicalName() + "]");
        }

        // resolve used services dependencies
        for (net.cadier.jcf.lang.instance.services.UsedService usedService : instance.getUsedServices()) {
            this.resolveUsedServiceDependency(anInstanceId, usedService);
        }

        // check for usage of implemented services
        for (ImplementedService implementedService : instance.getImplementedServices()) {
            this.resolveImplementedServiceUsage(anInstanceId, implementedService);
        }

        // resolve events sinks
        for (EventsSink eventsSink : instance.getEventsSinks()) {
            this.resolveEventsSinkDependency(anInstanceId, eventsSink);
        }

        // check for sources of sinks
        for (EventsSource eventsSource : instance.getEventsSources()) {
            this.resolveSourcesUsage(anInstanceId, eventsSource);
        }
    }

    @SuppressWarnings("unchecked")
    private void resolveUsedServiceDependency(final InstanceId anInstanceId,
                                              final net.cadier.jcf.lang.instance.services.UsedService aUsedService) throws DependencyResolutionException {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Looking for implementation for @UsedService " + aUsedService.getClazz() + " " +
                    "in " + anInstanceId + " [" + this.instancesTree.getRef(anInstanceId).getComponent().getClazz().getCanonicalName() + "]");
        }

        // find all implementation for this used service
        Map<InstanceId, ImplementedService> foundImplementationServices = this.findImplementedServices(anInstanceId, aUsedService);
        if (foundImplementationServices.size() > 0) {
            if (aUsedService.isCardinalityMany()) {
                // eventually inject Map
                if (aUsedService.getObject() == null) {
                    aUsedService.setObject(new HashMap<>());
                }

                if (this.loggingServices != null) {
                    this.loggingServices.debug("Found implementations in " + foundImplementationServices.keySet());
                }

                // for each found implementations...
                foundImplementationServices.forEach((id, implementedService) -> {
                    // inject found implementation
                    ((Map<InstanceId, Object>) aUsedService.getObject()).put(id, implementedService.getObject());
                    implementedService.addUsedService(aUsedService);
                });
            } else {
                if (foundImplementationServices.size() == 1) {
                    InstanceId implementationInstanceId = foundImplementationServices.keySet().iterator().next();
                    ImplementedService implementedService = foundImplementationServices.values().iterator().next();
                    if (this.loggingServices != null) {
                        this.loggingServices.debug("Found implementation in " + implementationInstanceId);
                    }

                    // inject found implementation
                    aUsedService.setObject(implementedService.getObject());
                    implementedService.addUsedService(aUsedService);
                }
                // too much found implementations
                else if (foundImplementationServices.size() > 1) {
                    if (this.loggingServices != null) {
                        String message = "Found too much implementations for @UsedService '" + aUsedService.getClazz().getCanonicalName() + "' of '" + anInstanceId + "' [" + this.instancesTree.getRef(anInstanceId).getComponent().getClazz().getCanonicalName() + "]";
                        this.loggingServices.error(message);
                        throw new DependencyResolutionException(anInstanceId, message);
                    }
                }
            }
        } else {
            // no found implementations
            if (this.loggingServices != null) {
                this.loggingServices.debug("Found no implementation for @UsedService " + aUsedService.getClazz().getCanonicalName() + " in " + anInstanceId + " [" + this.instancesTree.getRef(anInstanceId).getComponent().getClazz().getCanonicalName() + "]");
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void resolveImplementedServiceUsage(final InstanceId anInstanceId, final ImplementedService anImplementedService) throws DependencyResolutionException {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Looking for usage of @ImplementedService " + anImplementedService.getClazz() +
                    " provided by" + " " + anInstanceId + " [" + this.instancesTree.getRef(anInstanceId).getComponent().getClazz().getCanonicalName() + "]");
        }

        // find all implementation for this used service
        Map<InstanceId, net.cadier.jcf.lang.instance.services.UsedService> foundUsedServices = this.findUsedServices(anInstanceId, anImplementedService);
        for (Map.Entry<InstanceId, net.cadier.jcf.lang.instance.services.UsedService> foundUsage : foundUsedServices.entrySet()) {
            InstanceId userId = foundUsage.getKey();
            net.cadier.jcf.lang.instance.services.UsedService usedService = foundUsage.getValue();

            if (this.loggingServices != null) {
                this.loggingServices.debug("Found usage in " + userId);
            }

            if (usedService.isCardinalityMany()) {
                // eventually inject Map
                if (usedService.getObject() == null) {
                    usedService.setObject(new HashMap<>());
                }

                // inject implementation
                ((Map<InstanceId, Object>) usedService.getObject()).put(anInstanceId, anImplementedService.getObject());
                anImplementedService.addUsedService(usedService);
            } else {
                // inject implementation if there is nothing yet
                if (usedService.getObject() == null) {
                    usedService.setObject(anImplementedService.getObject());
                    anImplementedService.addUsedService(usedService);
                } else if (this.loggingServices != null) {
                    String message = "Cannot inject @ImplementedService, because a service has already been injected, in found @UsedService " + usedService.getClazz().getCanonicalName() + " in " + anInstanceId + " [" + this.instancesTree.getRef(anInstanceId).getComponent().getClazz().getCanonicalName() + "]";
                    this.loggingServices.error(message);
                    throw new DependencyResolutionException(anInstanceId, message);
                }
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void resolveEventsSinkDependency(final InstanceId anInstanceId, final EventsSink anEventsSink) throws DependencyResolutionException {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Looking for sources for @EventsSink " + anEventsSink.getEventClazz().getCanonicalName() + " in " + this.instancesTree.getRef(anInstanceId).getComponent().getClazz().getCanonicalName());
        }

        // find all events sources for this sink
        Map<InstanceId, EventsSource> foundEventsSources = this.findEventsSources(anInstanceId, anEventsSink);
        if (foundEventsSources.size() > 0) {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Found sources " + foundEventsSources);
            }

            // add sink to found sources
            foundEventsSources.values().forEach(eventsSource -> {
                // add sources and sink to each others
                anEventsSink.addEventsSource(eventsSource);
                eventsSource.addEventsSink(anEventsSink);

                // add sink to source
                // NOTE: cast are ok since, if we are here, it means that SinkQueue and EventsDispatcher have
                // already been injected
                EventsDispatcher eventsDispatcher = (EventsDispatcher) eventsSource.getObject();
                SinkQueue sinkQueue = (SinkQueue) anEventsSink.getObject();
                eventsDispatcher.addSinkQueue(sinkQueue);
            });
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Found no source for @EventsSink " + anEventsSink.getEventClazz().getCanonicalName() + " in " + this.instancesTree.getRef(anInstanceId).getComponent().getClazz().getCanonicalName());
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void resolveSourcesUsage(final InstanceId anInstanceId, final EventsSource anEventsSource) throws DependencyResolutionException {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Looking for sinks for @EventsSource " + anEventsSource.getEventClazz().getCanonicalName() + " produced by " + this.instancesTree.getRef(anInstanceId).getComponent().getClazz().getCanonicalName());
        }

        // find all events sinks for this source
        Map<InstanceId, EventsSink> foundEventsSinks = this.findEventsSinks(anInstanceId, anEventsSource);
        for (EventsSink anEventsSink : foundEventsSinks.values()) {
            // add sources and sink to each others
            anEventsSink.addEventsSource(anEventsSource);
            anEventsSource.addEventsSink(anEventsSink);

            // add sink to source
            // NOTE: cast are ok since, if we are here, it means that SinkQueue and EventsDispatcher have
            // already been injected
            EventsDispatcher eventsDispatcher = (EventsDispatcher) anEventsSource.getObject();
            SinkQueue sinkQueue = (SinkQueue) anEventsSink.getObject();
            eventsDispatcher.addSinkQueue(sinkQueue);
        }
    }


    private Map<InstanceId, ImplementedService> findImplementedServices(final InstanceId anInstanceId,
                                                                        net.cadier.jcf.lang.instance.services.UsedService usedService) throws DependencyResolutionException {
        Map<InstanceId, ImplementedService> foundImplementedServices = new HashMap<>();

        Class<?> usedServiceClazz = usedService.getClazz();
        List<InstanceId> lookedInstanceId = new ArrayList<>();

        // look for PUBLIC, PRIVATE or PROTECTED implementations in parent
        InstanceId parentId = this.instancesTree.getParent(anInstanceId);
        if (parentId != null) {
            lookedInstanceId.add(parentId);

            this.lookForImplementedService(parentId, usedServiceClazz, Modifier.PUBLIC, foundImplementedServices);
            this.lookForImplementedService(parentId, usedServiceClazz, Modifier.PROTECTED, foundImplementedServices);
            this.lookForImplementedService(parentId, usedServiceClazz, Modifier.PRIVATE, foundImplementedServices);
        }

        // look for PUBLIC or PROTECTED implementations in neighbors
        for (InstanceId neighborId : this.instancesTree.getNeighbors(anInstanceId)) {
            lookedInstanceId.add(neighborId);

            this.lookForImplementedService(neighborId, usedServiceClazz, Modifier.PUBLIC, foundImplementedServices);
            this.lookForImplementedService(neighborId, usedServiceClazz, Modifier.PROTECTED, foundImplementedServices);
        }

        // look for PUBLIC implementations in children
        for (InstanceId childId : this.instancesTree.getChildren(anInstanceId)) {
            lookedInstanceId.add(childId);

            this.lookForImplementedService(childId, usedServiceClazz, Modifier.PUBLIC, foundImplementedServices);
        }

        // look for PUBLIC implementations in roots
        for (InstanceId rootId : this.instancesTree.getRoots()) {
            // only look in roots that are not the parent, neighbors or children (impossible, though) of the
            // instance
            if (!lookedInstanceId.contains(rootId)) {
                this.lookForImplementedService(rootId, usedServiceClazz, Modifier.PUBLIC, foundImplementedServices);
            }
        }

        return foundImplementedServices;
    }

    private Map<InstanceId, net.cadier.jcf.lang.instance.services.UsedService> findUsedServices(final InstanceId anInstanceId,
                                                                                                final ImplementedService implementedService) throws DependencyResolutionException {
        Map<InstanceId, net.cadier.jcf.lang.instance.services.UsedService> foundUsedServices = new HashMap<>();

        Class<?> implementedServiceClazz = implementedService.getClazz();

        boolean isPublic = implementedService.getModifiers().contains(Modifier.PUBLIC);
        boolean isProtected = implementedService.getModifiers().contains(Modifier.PROTECTED);
        boolean isPrivate = implementedService.getModifiers().contains(Modifier.PRIVATE);

        // service implementation is public...
        if (isPublic) {
            //... and instance is deployed as root: look for service usage in all instances
            if (this.instancesTree.isRoot(anInstanceId)) {
                for (Instance instance : this.instancesTree.getRefs()) {
                    // get instance's id
                    InstanceId otherInstanceId = instance.getInstanceId();
                    // filter the current instance
                    if (!otherInstanceId.equals(anInstanceId)) {
                        // look for service in instance
                        this.lookForUsedService(otherInstanceId, implementedServiceClazz, foundUsedServices);
                    }
                }
            }
            //... and instance is not deployed as root: look for service in parent, neighbors and children
            else {
                InstanceId parentId = this.instancesTree.getParent(anInstanceId);
                this.lookForUsedService(parentId, implementedServiceClazz, foundUsedServices);

                for (InstanceId neighborId : this.instancesTree.getNeighbors(anInstanceId)) {
                    this.lookForUsedService(neighborId, implementedServiceClazz, foundUsedServices);
                }

                for (InstanceId childId : this.instancesTree.getChildren(anInstanceId)) {
                    this.lookForUsedService(childId, implementedServiceClazz, foundUsedServices);
                }
            }
        }
        // service implementation is protected : look for service in neighbors and children
        else if (isProtected) {
            for (InstanceId neighborId : this.instancesTree.getNeighbors(anInstanceId)) {
                this.lookForUsedService(neighborId, implementedServiceClazz, foundUsedServices);
            }

            for (InstanceId childId : this.instancesTree.getChildren(anInstanceId)) {
                this.lookForUsedService(childId, implementedServiceClazz, foundUsedServices);
            }
        }
        // service implementation is private : look for service in children
        else if (isPrivate) {
            for (InstanceId childId : this.instancesTree.getChildren(anInstanceId)) {
                this.lookForUsedService(childId, implementedServiceClazz, foundUsedServices);
            }
        }

        return foundUsedServices;
    }

    private Map<InstanceId, EventsSource> findEventsSources(InstanceId instanceId, EventsSink eventsSink) throws DependencyResolutionException{
        Map<InstanceId, EventsSource> foundEventsSources = new HashMap<>();

        Class<?> sinkEventClazz = eventsSink.getEventClazz();
        List<InstanceId> lookedInstanceId = new ArrayList<>();

        // look for PUBLIC, PRIVATE or PROTECTED sources in parent
        InstanceId parentId = this.instancesTree.getParent(instanceId);
        if (parentId != null) {
            lookedInstanceId.add(parentId);

            this.lookForEventsSources(parentId, sinkEventClazz, Modifier.PUBLIC, foundEventsSources);
            this.lookForEventsSources(parentId, sinkEventClazz, Modifier.PROTECTED, foundEventsSources);
            this.lookForEventsSources(parentId, sinkEventClazz, Modifier.PRIVATE, foundEventsSources);
        }

        // look for PUBLIC or PROTECTED sources in neighbors
        for (InstanceId neighborId : this.instancesTree.getNeighbors(instanceId)) {
            lookedInstanceId.add(neighborId);

            this.lookForEventsSources(neighborId, sinkEventClazz, Modifier.PUBLIC, foundEventsSources);
            this.lookForEventsSources(neighborId, sinkEventClazz, Modifier.PROTECTED, foundEventsSources);
        }

        // look for PUBLIC sources in children
        for (InstanceId childId : this.instancesTree.getChildren(instanceId)) {
            lookedInstanceId.add(childId);

            this.lookForEventsSources(childId, sinkEventClazz, Modifier.PUBLIC, foundEventsSources);
        }

        // look for PUBLIC sources in roots
        for (InstanceId rootId : this.instancesTree.getRoots()) {
            // only look in roots that are not the parent, neighbors or children (impossible, though) of the
            // instance
            if (!lookedInstanceId.contains(rootId)) {
                this.lookForEventsSources(rootId, sinkEventClazz, Modifier.PUBLIC, foundEventsSources);
            }
        }

        return foundEventsSources;
    }

    private Map<InstanceId, EventsSink> findEventsSinks(InstanceId anInstanceId, EventsSource anEventsSource) throws DependencyResolutionException{
        Map<InstanceId, EventsSink> foundEventsSinks = new HashMap<>();

        Class<?> sourceEventClazz = anEventsSource.getEventClazz();

        boolean isPublic = anEventsSource.getModifiers().contains(Modifier.PUBLIC);
        boolean isProtected = anEventsSource.getModifiers().contains(Modifier.PROTECTED);
        boolean isPrivate = anEventsSource.getModifiers().contains(Modifier.PRIVATE);

        // source is public...
        if (isPublic) {
            //... and instance is deployed as root: look for sinks in all instances
            if (this.instancesTree.isRoot(anInstanceId)) {
                for (Instance instance : this.instancesTree.getRefs()) {
                    // get instance's id
                    InstanceId otherInstanceId = instance.getInstanceId();
                    // filter the current instance
                    if (!otherInstanceId.equals(anInstanceId)) {
                        // look for service in instance
                        this.lookForEventsSinks(otherInstanceId, sourceEventClazz, foundEventsSinks);
                    }
                }
            }
            //... and instance is not deployed as root: look for sinks in parent, neighbors and children
            else {
                InstanceId parentId = this.instancesTree.getParent(anInstanceId);
                this.lookForEventsSinks(parentId, sourceEventClazz, foundEventsSinks);

                for (InstanceId neighborId : this.instancesTree.getNeighbors(anInstanceId)) {
                    this.lookForEventsSinks(neighborId, sourceEventClazz, foundEventsSinks);
                }

                for (InstanceId childId : this.instancesTree.getChildren(anInstanceId)) {
                    this.lookForEventsSinks(childId, sourceEventClazz, foundEventsSinks);
                }
            }
        }
        // source is protected : look for sinks in neighbors and children
        else if (isProtected) {
            for (InstanceId neighborId : this.instancesTree.getNeighbors(anInstanceId)) {
                this.lookForEventsSinks(neighborId, sourceEventClazz, foundEventsSinks);
            }

            for (InstanceId childId : this.instancesTree.getChildren(anInstanceId)) {
                this.lookForEventsSinks(childId, sourceEventClazz, foundEventsSinks);
            }
        }
        // source is private : look for sinks in children
        else if (isPrivate) {
            for (InstanceId childId : this.instancesTree.getChildren(anInstanceId)) {
                this.lookForEventsSinks(childId, sourceEventClazz, foundEventsSinks);
            }
        }

        return foundEventsSinks;
    }

    /**
     * Look in supplied instance for a service's implementation with the supplied class and modifier.
     *
     * ERROR if many implementations are found.
     *
     * EXCEPTION if this instance has already been looked for this service.
     */
    private void lookForImplementedService(final InstanceId instanceId, final Class<?> serviceClazz, final Modifier modifier,
                                           final Map<InstanceId, ImplementedService> foundImplementedServices) throws DependencyResolutionException {
        // get instance
        Instance instance = this.instancesTree.getRef(instanceId);

        // find all implementations for this service type and these modifiers
        for (ImplementedService implementedService : instance.getImplementedServices()) {
            if (implementedService.getClazz() == serviceClazz && implementedService.getModifiers().contains(modifier)) {
                // add found implemented service and check that nothing has been previously found
                ImplementedService previousValue = foundImplementedServices.put(instanceId, implementedService);
                if (previousValue != null) {
                    // service already found -> error
                    String message = "Duplicate @ImplementedService " + serviceClazz + " with modifiers " + modifier + " found in " + instanceId;
                    this.loggingServices.error(message);
                    throw new DependencyResolutionException(instanceId, message);
                }
            }
        }
    }

    /**
     * Look in supplied instance for a service's usage with the supplied class.
     *
     * ERROR if many usages are found.
     *
     * EXCEPTION if this instance has already been looked for this service.
     */
    private void lookForUsedService(final InstanceId instanceId, final Class<?> serviceClazz,
                                    final Map<InstanceId, net.cadier.jcf.lang.instance.services.UsedService> foundUsedServices) throws DependencyResolutionException {
        // get instance
        Instance instance = this.instancesTree.getRef(instanceId);

        // find all users for this service type
        for (net.cadier.jcf.lang.instance.services.UsedService usedService : instance.getUsedServices()) {
            if (usedService.getClazz() == serviceClazz) {
                // add found used service and check that nothing has been previously found
                net.cadier.jcf.lang.instance.services.UsedService previousValue = foundUsedServices.put(instanceId, usedService);
                if (previousValue != null) {
                    // service already found -> error
                    String message = "Duplicate @UsedService " + serviceClazz + " found in " + instanceId;
                    this.loggingServices.error(message);
                    throw new DependencyResolutionException(instanceId, message);
                }
            }
        }
    }

    private void lookForEventsSources(final InstanceId instanceId, Class<?> eventClazz, final Modifier modifier,
                                      final Map<InstanceId, EventsSource> foundEventsSources) throws DependencyResolutionException {
        // get instance
        Instance instance = this.instancesTree.getRef(instanceId);

        // find all sources for this sink event type and these modifiers
        for (EventsSource eventsSource : instance.getEventsSources()) {
            // test source event type, modifiers and object
            if (eventsSource.getEventClazz() == eventClazz && eventsSource.getModifiers().contains(modifier) && instance.getObject() != null) {
                // add found source and check that nothing has been previously found
                EventsSource previousValue = foundEventsSources.put(instanceId, eventsSource);
                if (previousValue != null) {
                    // source already found -> error
                    String message = "Duplicate @EventsSource " + eventClazz + " with modifiers " + modifier + " found in " + instanceId;
                    this.loggingServices.error(message);
                    throw new DependencyResolutionException(instanceId, message);
                }
            }
        }
    }

    private void lookForEventsSinks(InstanceId instanceId, Class<?> eventClazz, Map<InstanceId, EventsSink> foundEventsSinks) throws DependencyResolutionException {
        // get instance
        Instance instance = this.instancesTree.getRef(instanceId);

        // find all sources for this sink event type and these modifiers
        for (EventsSink eventsSink : instance.getEventsSinks()) {
            if (eventsSink.getEventClazz() == eventClazz && instance.getObject() != null) {
                // add found sink and check that nothing has been previously found
                EventsSink previousValue = foundEventsSinks.put(instanceId, eventsSink);
                if (previousValue != null) {
                    // source already found -> error
                    String message = "Duplicate @EventsSink " + eventClazz + " found in " + instanceId;
                    this.loggingServices.error(message);
                    throw new DependencyResolutionException(instanceId, message);
                }
            }
        }
    }

    private void removeDependencies(final InstanceId instanceId) {
        if (this.loggingServices != null) {
            this.loggingServices.debug("Removing dependencies of instance [" + instanceId + "]");
        }

        // get instance
        Instance instance = this.instancesTree.getRef(instanceId);

        // remove implemented from used services
        for (ImplementedService implementedService : instance.getImplementedServices()) {
            for (net.cadier.jcf.lang.instance.services.UsedService usedService : implementedService.getUsedServices()) {
                if (usedService.isCardinalityMany()) {
                    @SuppressWarnings("unchecked")
                    Map<InstanceId, Object> map = (Map<InstanceId, Object>) usedService.getObject();
                    map.remove(instanceId);
                } else {
                    usedService.setObject(null);
                }
            }
        }

        // remove events sinks from sources
        for (EventsSink eventsSink : instance.getEventsSinks()) {
            eventsSink.getEventsSources().forEach(eventsSource -> eventsSource.removeEventsSink(eventsSink));
        }
    }

}
