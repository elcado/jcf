/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.core.componentsManager.impl.processors.visitors;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.SimpleAnnotationValueVisitor8;
import javax.lang.model.util.Types;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public interface ComponentVisitor {

    /**
     * Get all the interfaces of the supplied TypeElement that implements the supplied annotation. If the
     * component represented by this TypeElement is well formed, there should be only one item in the resulting List.
     *
     * @param aComponentImplElement TypeElement of a component implementation.
     * @param anAnnotationClass     Searched annotation.
     * @param aTypeUtils            Types utils.
     * @param <A>                   type of the annotation.
     * @return list of the TypeElement of the found the component types.
     */
    static <A extends Annotation> List<TypeElement> getAnnotatedInterfaces(final TypeElement aComponentImplElement,
                                                                           final Class<A> anAnnotationClass,
                                                                           final Types aTypeUtils) {
        return aComponentImplElement.getInterfaces().stream()
                // map to TypeElement
                .map(aTypeUtils::asElement).filter(TypeElement.class::isInstance).map(TypeElement.class::cast)
                // filter those annotated with ComponentType
                .filter(componentElementInterface -> componentElementInterface.getAnnotation(anAnnotationClass) != null)
                // collect
                .collect(Collectors.toList());
    }

    /**
     * Extract the component type's contract in the form of 3 lists of:
     * <ul>
     *     <li>composed components</li>
     *     <li>provided services</li>
     *     <li>produced events</li>
     * </ul>
     *
     * @param aTypeElement                   TypeElement of a component type.
     * @param anAnnotationClass              The annotation class to parse.
     * @param typeMirrorListPerAnnotationKey To be filled produced events list per annotation key.
     * @param anElementUtils                 Elements utils.
     */
    static void extractComponentAnnotationInfo(final TypeElement aTypeElement, final Class<?> anAnnotationClass,
                                               final Map<String, List<TypeMirror>> typeMirrorListPerAnnotationKey,
                                               final Elements anElementUtils) {
        // build the generic visitor for our AnnotationValues
        BiConsumer<AnnotationValue, List<TypeMirror>> annotationValueVisitor =
                (annotationValue, typeMirrorsList) -> annotationValue.accept(new SimpleAnnotationValueVisitor8<Void,
                        Void>() {
            @Override
            public Void visitType(TypeMirror typeMirror, Void p) {
                typeMirrorsList.add(typeMirror);
                return p;
            }

            @Override
            public Void visitArray(List<? extends AnnotationValue> vals, Void p) {
                vals.forEach(val -> val.accept(this, p));
                return p;
            }
        }, null);

        // parse annotation
        for (AnnotationMirror annotationMirror : anElementUtils.getAllAnnotationMirrors(aTypeElement)) {
            if (anAnnotationClass.getName().equals(annotationMirror.getAnnotationType().toString())) {
                Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues =
                        anElementUtils.getElementValuesWithDefaults(annotationMirror);
                for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry :
                        elementValues.entrySet()) {
                    String key = entry.getKey().getSimpleName().toString();
                    List<TypeMirror> typeMirrorList = typeMirrorListPerAnnotationKey.get(key);

                    annotationValueVisitor.accept(entry.getValue(), typeMirrorList);
                }
            }
        }
    }
}
