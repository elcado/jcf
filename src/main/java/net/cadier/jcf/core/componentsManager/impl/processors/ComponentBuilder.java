/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.componentsManager.impl.processors;


import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.core.componentsManager.impl.processors.visitors.BuilderVisitor;
import net.cadier.jcf.lang.component.Component;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.util.HashSet;
import java.util.Set;

public class ComponentBuilder extends AbstractProcessor {
    private Types typeUtils;
    private Elements elementUtils;
    private Messager messager;

    /**
     * The resulting Component.
     */
    private Component component;

    /**
     * Getter on the resulting Component.
     *
     * @return the resulting Component.
     */
    public Component getComponent() {
        return component;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> annotations = new HashSet<>();
        //        annotations.add(ComponentType.class.getCanonicalName());
        annotations.add(ComponentImplementation.class.getCanonicalName());
        return annotations;
    }

    @Override
    public void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);

        this.typeUtils = processingEnvironment.getTypeUtils();
        this.elementUtils = processingEnvironment.getElementUtils();
        this.messager = processingEnvironment.getMessager();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        // 1 annotation (see ComponentProcessor.getSupportedAnnotationTypes())
        // 1 element because we discover components one by one
        if (annotations.size() == 1 && roundEnv.getRootElements().size() == 1) {
            // now we can build the meta component
            Element rootElement = roundEnv.getRootElements().iterator().next();
            this.component = rootElement.accept(new BuilderVisitor(this.typeUtils, this.elementUtils, this.messager),
                    new Component());
        }

        // We have to return false for subsequent processors to be called.
        // Therefore, we return this.component != null.
        return this.component != null;
    }

}
