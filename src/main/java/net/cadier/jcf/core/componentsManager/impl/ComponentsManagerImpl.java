/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.componentsManager.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.ProvidedService;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.core.componentsManager.ComponentsManager;
import net.cadier.jcf.core.componentsManager.ComponentsManagerServices;
import net.cadier.jcf.core.componentsManager.impl.processors.ComponentBuilder;
import net.cadier.jcf.core.componentsManager.impl.processors.ComponentChecker;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.exception.MalformedComponentException;
import net.cadier.jcf.lang.component.Component;

import javax.annotation.processing.AbstractProcessor;
import javax.tools.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@ComponentImplementation
public class ComponentsManagerImpl implements ComponentsManager, ComponentsManagerServices {
    private static final String ERROR_PREFIX = "error: ";
    private static final String MISSING_IMPL_ANNOTATION_ERROR = "Implementation class '%s' is missing @ComponentImplementation annotation.";

    /**
     * The map of built components : the key is the component's implementation class.
     */
    private final Map<Class<?>, Component> registeredComponents = new HashMap<>();

    @ProvidedService
    public final transient ComponentsManagerServices componentsManagerServices = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    private ComponentsManagerImpl() {
    }

    @Override
    public void registerComponent(Class<?> componentImplementation) throws MalformedComponentException {
        // if component has not yet been registered, discover and register it
        if (!this.registeredComponents.containsKey(componentImplementation)) {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Discovering " + componentImplementation + ".");
            }

            // discover component (and crash if it fails)
            Component component = this.discoverComponent(componentImplementation);
            if (component == null) {
                throw new MalformedComponentException(componentImplementation, null, Arrays.asList("Unknown malformed exception: cannot build a description of " + componentImplementation + "."));
            }

            // log discovered component
            if (this.loggingServices != null) {
                this.loggingServices.debug(component.getType().toLongString());
                this.loggingServices.debug(component.toLongString());
            }

            // register component and its composed components
            this.registeredComponents.put(componentImplementation, component);
            component.getComposedComponents().forEach(comp -> this.registeredComponents.put(comp.getClazz(), comp));
        }
    }

    /**
     * Discover the supplied component.
     *
     * @param componentClass the class to discover as a component
     * @return the resulting Component of the discovery process
     */
    private Component discoverComponent(Class<?> componentClass) throws MalformedComponentException {
        // first, check that ComponentImplementation annotation is here, so that annotation processor can work
        if (Objects.isNull(componentClass.getAnnotation(ComponentImplementation.class))) {
            throw new MalformedComponentException(componentClass, null, Arrays.asList(String.format(MISSING_IMPL_ANNOTATION_ERROR, componentClass.getCanonicalName())));
        }

        // build annotations processors
        ComponentBuilder componentBuilder = new ComponentBuilder();

        List<AbstractProcessor> processorList = new ArrayList<>();
        assert processorList.add(new ComponentChecker()); // this only adds this processor if assertions are enabled
        processorList.add(componentBuilder);

        // build DiagnosticCollector
        DiagnosticCollector<JavaFileObject> diagnosticsCollector = new DiagnosticCollector<>();

        // build compiler task
        JavaCompiler.CompilationTask task = ToolProvider.getSystemJavaCompiler().getTask(null, null,
                diagnosticsCollector, null, Arrays.asList(componentClass.getCanonicalName()), null);
        task.setProcessors(processorList);

        // launch compiler task and get result
        AtomicBoolean isMalformed = new AtomicBoolean(!task.call());

        // logging diagnostics of compiler task
        if (this.loggingServices != null) {
            diagnosticsCollector.getDiagnostics().forEach(diagnostic -> {
                switch (diagnostic.getKind()) {
                    case OTHER:
                    case WARNING:
                        this.loggingServices.warn(diagnostic.toString());
                        break;
                    case MANDATORY_WARNING:
                    case NOTE:
                        this.loggingServices.debug(diagnostic.toString());
                        break;
                    case ERROR:
                        // malformed component error
                        isMalformed.set(true);
                        break;
                }
            });
        }

        // throws exception on malformed component
        if (isMalformed.get()) {
            List<String> malformedCauses = diagnosticsCollector.getDiagnostics().stream()
                    .filter(diagnostic -> Diagnostic.Kind.ERROR.equals(diagnostic.getKind()))
                    .map(Diagnostic::toString)
                    .map(diagnostic -> {
                        if (diagnostic.startsWith(ERROR_PREFIX)) {
                            diagnostic = diagnostic.split(ERROR_PREFIX)[1];
                        }
                        return diagnostic;
                    })
                    .collect(Collectors.toList());
            throw new MalformedComponentException(componentClass, componentBuilder.getComponent(), malformedCauses);
        }

        // return built Component
        return componentBuilder.getComponent();
    }

    @Override
    public Component getComponent(Class<?> ComponentImplementation) {
        return this.registeredComponents.get(ComponentImplementation);
    }

    @Override
    public List<Component> getRegisteredComponents() {
        return new ArrayList<>(this.registeredComponents.values());
    }

    @Override
    public void unregisterComponent(Component component) {
        // unregister component and its composed components
        component.getComposedComponents().forEach(comp -> this.registeredComponents.remove(comp.getClazz()));
        this.registeredComponents.remove(component.getClazz());
    }

}
