/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.componentsManager.impl.processors.visitors;

import net.cadier.jcf.annotations.*;
import net.cadier.jcf.lang.component.Component;
import net.cadier.jcf.lang.component.Initializable;
import net.cadier.jcf.lang.component.Startable;
import net.cadier.jcf.lang.component.Type;
import net.cadier.jcf.lang.component.port.events.EventsSinkPort;
import net.cadier.jcf.lang.component.port.events.EventsSourcePort;
import net.cadier.jcf.lang.component.port.id.IdPort;
import net.cadier.jcf.lang.component.port.parameters.ParametersPort;
import net.cadier.jcf.lang.component.port.services.ProvidedServicePort;
import net.cadier.jcf.lang.component.port.services.UsedServicePort;
import net.cadier.utils.javaxLangModel.JavaxLangModelUtils;

import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementKindVisitor8;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Build the meta description of a component implementation supplied to the components manager for registration.
 * <p>
 * All the building done here take advantage of the checking that (should) have been done by the
 * {@link net.cadier.jcf.core.componentsManager.impl.processors.ComponentChecker ComponentChecker}.
 * A lot of checking before casting or getting data can be forgotten.
 * </p>
 */
public class BuilderVisitor extends ElementKindVisitor8<Component, Component> implements ComponentVisitor {
    private final Types typeUtils;
    private final Elements elementUtils;
    private final Messager messager;

    public BuilderVisitor(final Types aTypeUtils, Elements anElementUtils, final Messager aMessager) {
        this.typeUtils = aTypeUtils;
        this.elementUtils = anElementUtils;
        this.messager = aMessager;
    }

    @Override
    public Component visitTypeAsClass(final TypeElement componentImplElement, final Component aComponent) {
        // avoid working on inner classes by checking @ComponentImplementation
        if (componentImplElement.getAnnotation(ComponentImplementation.class) != null) {
            /*
             * Component's type
             */
            Type type = new Type();

            // link type and implementation
            aComponent.setType(type);
            type.addImplementation(aComponent);

            TypeElement componentTypeTypeElement = ComponentVisitor.getAnnotatedInterfaces(componentImplElement,
                    ComponentType.class, this.typeUtils).get(0);

            // get component type
            type.setClazz(JavaxLangModelUtils.typeElementToClass(componentTypeTypeElement));

            // extracting info from @ComponentType annotation
            List<TypeMirror> providesTypeMirror = new ArrayList<>();
            List<TypeMirror> producesTypeMirror = new ArrayList<>();

            Map<String, List<TypeMirror>> componentTypeInfo = new HashMap<>();
            componentTypeInfo.put("provides", providesTypeMirror);
            componentTypeInfo.put("produces", producesTypeMirror);

            ComponentVisitor.extractComponentAnnotationInfo(componentTypeTypeElement, ComponentType.class,
                    componentTypeInfo, this.elementUtils);

            List<Class<?>> providedServices =
                    providesTypeMirror.stream().map(JavaxLangModelUtils::typeMirrorToClass).collect(Collectors.toList());
            List<Class<?>> producedEvents =
                    producesTypeMirror.stream().map(JavaxLangModelUtils::typeMirrorToClass).collect(Collectors.toList());

            type.setProvidedServices(providedServices);
            type.setProducedEvents(producedEvents);

            /*
             * Component's implementation
             */

            // get component's class
            aComponent.setClazz(JavaxLangModelUtils.typeElementToClass(componentImplElement));

            // is component initializable
            boolean isComponentInitializable = this.checkThatTypeImplements(componentImplElement, Initializable.class);
            aComponent.setInitializable(isComponentInitializable);

            // is component startable
            boolean isComponentStartable = this.checkThatTypeImplements(componentImplElement, Startable.class);
            aComponent.setStartable(isComponentStartable);

            // element is a type, visit its enclosed elements
            List<? extends Element> allMembers = this.elementUtils.getAllMembers(componentImplElement);
            allMembers.forEach(enclosedElement -> enclosedElement.accept(this, aComponent));

            // extracting info from @ComponentImplementation annotation
            List<TypeMirror> compositionTypeMirror = new ArrayList<>();

            Map<String, List<TypeMirror>> componentImplementationInfo = new HashMap<>();
            componentImplementationInfo.put("composition", compositionTypeMirror);
            ComponentVisitor.extractComponentAnnotationInfo(componentImplElement, ComponentImplementation.class,
                    componentImplementationInfo, this.elementUtils);
            for (TypeMirror composedType : compositionTypeMirror) {
                TypeElement composedElement = (TypeElement) this.typeUtils.asElement(composedType);
                Component composedComponent = composedElement.accept(this, new Component());
                aComponent.addComposedComponent(composedComponent);
            }
        }

        return aComponent;
    }

    private boolean checkThatTypeImplements(final TypeElement aTypeElement, final Class<?> anInterfaceClass) {
        TypeElement typeElement = this.elementUtils.getTypeElement(anInterfaceClass.getCanonicalName());
        return this.typeUtils.isAssignable(aTypeElement.asType(), typeElement.asType());
    }

    @Override
    public Component visitVariableAsField(final VariableElement aVariableElement, final Component aComponent) {
        // get id
        if (aVariableElement.getAnnotation(Id.class) != null) {
            IdPort idp = this.buildIdPort(aVariableElement);
            aComponent.setIdPort(idp);
        }

        // get provided services
        if (aVariableElement.getAnnotation(ProvidedService.class) != null) {
            ProvidedServicePort isp = this.buildImplementedServicePort(aVariableElement);
            aComponent.addImplementedServicePort(isp);
        }

        // get used services
        if (aVariableElement.getAnnotation(UsedService.class) != null) {
            UsedServicePort usp = this.buildUsedServicePort(aVariableElement);
            aComponent.addUsedServicePort(usp);
        }

        // get events sources
        if (aVariableElement.getAnnotation(EventSource.class) != null) {
            EventsSourcePort esp = this.buildEventsSourcePort(aVariableElement);
            aComponent.addEventsSourcePort(esp);
        }

        // get events sinks
        if (aVariableElement.getAnnotation(EventSink.class) != null) {
            EventsSinkPort esp = this.buildEventsSinkPort(aVariableElement);
            aComponent.addEventsSinkPort(esp);
        }

        // get parameters
        if (aVariableElement.getAnnotation(Parameter.class) != null) {
            ParametersPort pp = this.buildParametersPort(aVariableElement);
            aComponent.addParametersPort(pp);
        }
        return aComponent;
    }

    private IdPort buildIdPort(final VariableElement aVariableElement) {
        IdPort idp = new IdPort();
        idp.setModifiers(aVariableElement.getModifiers());
        idp.setClazz(JavaxLangModelUtils.typeMirrorToClass(aVariableElement.asType()));
        idp.setName(aVariableElement.getSimpleName().toString());
        return idp;
    }

    private ProvidedServicePort buildImplementedServicePort(final VariableElement aVariableElement) {
        ProvidedServicePort isp = new ProvidedServicePort();
        isp.setModifiers(aVariableElement.getModifiers());
        isp.setClazz(JavaxLangModelUtils.typeMirrorToClass(aVariableElement.asType()));
        isp.setName(aVariableElement.getSimpleName().toString());
        return isp;
    }

    private UsedServicePort buildUsedServicePort(final VariableElement aVariableElement) {
        UsedServicePort usp = new UsedServicePort();
        usp.setModifiers(aVariableElement.getModifiers());
        usp.setCardinalityMany("java.util.Map".equals(this.typeUtils.erasure(aVariableElement.asType()).toString()));
        if (usp.isCardinalityMany()) {
            List<? extends TypeMirror> mapTypeArguments = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
            usp.setClazz(JavaxLangModelUtils.typeMirrorToClass(mapTypeArguments.get(1)));
        } else {
            usp.setClazz(JavaxLangModelUtils.typeMirrorToClass(aVariableElement.asType()));
        }
        usp.setName(aVariableElement.getSimpleName().toString());
        return usp;
    }

    private EventsSourcePort buildEventsSourcePort(final VariableElement aVariableElement) {
        EventsSourcePort esp = new EventsSourcePort();
        esp.setModifiers(aVariableElement.getModifiers());
        List<? extends TypeMirror> sourceTypes = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
        esp.setClazz(JavaxLangModelUtils.typeMirrorToClass(sourceTypes.get(0)));
        esp.setName(aVariableElement.getSimpleName().toString());
        return esp;
    }

    private EventsSinkPort buildEventsSinkPort(final VariableElement aVariableElement) {
        EventsSinkPort esp = new EventsSinkPort();
        esp.setModifiers(aVariableElement.getModifiers());
        List<? extends TypeMirror> sourceTypes = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
        esp.setClazz(JavaxLangModelUtils.typeMirrorToClass(sourceTypes.get(0)));
        esp.setName(aVariableElement.getSimpleName().toString());
        return esp;
    }

    private ParametersPort buildParametersPort(final VariableElement aVariableElement) {
        ParametersPort pp = new ParametersPort();
        pp.setModifiers(aVariableElement.getModifiers());
        List<? extends TypeMirror> parametersType = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
        pp.setClazz(JavaxLangModelUtils.typeMirrorToClass(parametersType.get(0)));
        pp.setName(aVariableElement.getSimpleName().toString());
        return pp;
    }

}
