
/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.componentsManager.impl.processors.visitors;

import net.cadier.jcf.annotations.*;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.events.Sink;
import net.cadier.jcf.lang.instance.events.Source;
import net.cadier.jcf.lang.instance.parameters.Parameters;
import net.cadier.utils.javaxLangModel.JavaxLangModelUtils;

import javax.annotation.processing.Messager;
import javax.lang.model.element.*;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementKindVisitor8;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import java.util.*;
import java.util.stream.Collectors;

public class CheckerVisitor extends ElementKindVisitor8<Boolean, Void> implements ComponentVisitor {
    /** All errors messages */
    private static final String MISSING_SERVICE_IMPLEMENTATION_ERROR = "Following provided services declared in component's type have not been found in implementation: '%s'.";
    private static final String MISSING_PRODUCED_EVENTS_ERROR = "Following produced events declared in component's type have not been found in implementation: '%s'.";
    private static final String MISSING_TYPE_ANNOTATION_ERROR = "Cannot find an implemented interface annotated with @ComponentType for '%s'.";
    private static final String TOO_MANY_TYPE_ANNOTATIONS_ERROR = "Found more thant one implemented interfaces annotated with @ComponentType for '%s'.";
    private static final String TYPE_PACKAGE_NAMING_ERROR = "The package's name of type '%s' must end with '%s'.";
    private static final String IMPL_NAMING_ERROR = "The name of the component implementation '%s' must end with '%sImpl'.";
    private static final String IMPL_PACKAGE_NAMING_ERROR = "The package's name of implementation '%s' must end with '%s'.";
    private static final String IMPL_NOT_A_CLASS_ERROR = "@ComponentImplementation element '%s' must be a class.";

    private static final String ID_INSTANCE_ID_TYPED_ERROR = "@Id field '%s' must be typed as '%s'.";
    private static final String ID_SIGNATURE_ERROR = "@Id field '%s' field must be %s.";

    private static final String PROVIDED_SERVICE_TYPE_INTERFACE_ERROR = "@ProvidedService '%s' type must be an interface.";
    private static final String PROVIDED_SERVICE_ANNOTATION_ERROR = "@ProvidedService '%s' type must be annotated with @%s.";
    private static final String UNDECLARED_PROVIDED_SERVICE_ERROR = "@ProvidedService '%s' isn't declared in component's type @%s annotation.";
    private static final String PROVIDED_SERVICE_SIGNATURE_ERROR = "@ProvidedService '%s' field  must be %s.";

    private static final String USED_SERVICE_MAP_KEY_INSTANCE_ID_ERROR = "@UsedService '%s' *-cardinality map's key must be typed as '%s'.";
    private static final String USED_SERVICE_TYPE_INTERFACE_ERROR = "@UsedService '%s' field's type must be an interface.";
    private static final String USED_SERVICE_ANNOTATION_ERROR = "@UsedService '%s' type must be annotated with @%s.";
    private static final String USED_SERVICE_SIGNATURE_ERROR = "@UsedService '%s' field  must be %s.";

    private static final String SOURCE_TYPE_ERROR = "@EventsSource '%s' must be typed as '%s'.";
    private static final String SOURCE_DATA_TYPE_CLASS_OR_ENUM_ERROR = "@EventsSource '%s' data type '%s' must be a class or an enum.";
    private static final String SOURCE_DATA_TYPE_ANNOTATION_ERROR = "@EventsSource '%s' data type '%s' must be annotated with @%s.";
    private static final String UNDECLARED_PRODUCED_EVENT_ERROR = "@EventsSource '%s' data type '%s' isn't declared in component's type @%s annotation.";
    private static final String SOURCE_SIGNATURE_ERROR = "@EventsSource '%s' field must be %s.";

    private static final String SINK_TYPE_ERROR = "@EventsSink '%s' must be typed as '%s'.";
    private static final String SINK_DATA_TYPE_CLASS_OR_ENUM_ERROR = "@EventsSink '%s' data type '%s' must be a class or an enum.";
    private static final String SINK_DATA_TYPE_ANNOTATION_ERROR = "@EventsSink '%s' data type '%s' must be annotated with @%s.";
    private static final String SINK_SIGNATURE_ERROR = "@EventsSink '%s' field must be %s.";

    private static final String PARAMETERS_TYPE_ERROR = "@Parameter '%s' must be typed as '%s'.";
    private static final String PARAMETERS_DATA_TYPE_ANNOTATION_ERROR = "@Parameter '%s' data type '%s' must be annotated with @%s.";
    private static final String PARAMETERS_SIGNATURE_ERROR = "@Parameter '%s' field must be %s.";

    private final Types typeUtils;
    private final Elements elementUtils;
    private final Messager messager;

    private CheckingContext checkingContext = null;

    public CheckerVisitor(final Types anTypeUtils, final Elements anElementUtils, final Messager aMessager) {
        // init default value to true to bypass what we are not explicitly checking
        super(Boolean.TRUE);

        this.typeUtils = anTypeUtils;
        this.elementUtils = anElementUtils;
        this.messager = aMessager;
    }

    public CheckingContext setCurrentElement(final Element anElement) {
        CheckingContext previousCheckingContext = this.checkingContext;
        this.checkingContext = new CheckingContext(anElement);
        return previousCheckingContext;
    }

    @Override
    public Boolean visitTypeAsClass(final TypeElement componentImplElement, final Void unused) {
        boolean result = true;

        if (this.checkingContext.getCurrentElement().equals(componentImplElement)) {
            // check component's implementation definition (filter on this.currentElement not to check enclosed elements)
            this.messager.printMessage(Diagnostic.Kind.NOTE, "Start checking for '" + componentImplElement + "'.");

            // check component's type definition
            if (!this.checkComponentType(componentImplElement)) {
                // cannot go further, return
                return false;
            }

            // check component's implementation definition
            if (!this.checkComponentImplementation(componentImplElement)) {
                // cannot go further, return
                return false;
            }

            // check naming convention
            result &= this.checkNamingConvention(componentImplElement);

                    // visit enclosed elements (fields, methods, constructors and member types)
            List<? extends Element> allMembers = this.elementUtils.getAllMembers(componentImplElement);
            result &= allMembers.stream().map(enclosedElement -> enclosedElement.accept(this, unused)).reduce(true,
                    Boolean::logicalAnd);

            // extracting info from @ComponentImplementation annotation
            List<TypeMirror> compositionTypeMirror = new ArrayList<>();

            Map<String, List<TypeMirror>> componentImplementationInfo = new HashMap<>();
            componentImplementationInfo.put("composition", compositionTypeMirror);
            ComponentVisitor.extractComponentAnnotationInfo(componentImplElement, ComponentImplementation.class,
                    componentImplementationInfo, this.elementUtils);

            // check all composed elements
            result &= compositionTypeMirror.stream()
                    .map(composedType -> (TypeElement) this.typeUtils.asElement(composedType))
                    .map(composedElement -> {
                        // check composed element
                        CheckingContext previousCheckingContext = this.setCurrentElement(composedElement);
                        Boolean composedResult = composedElement.accept(this, unused);
                        this.checkingContext = previousCheckingContext;

                        return composedResult;
                    })
                    .reduce(true, Boolean::logicalAnd);

            // at this point, we should have found all provided services and all produced events
            if (!this.checkingContext.getProvidesTypeMirror().isEmpty()) {
                String listOfProvidesTypeMirror =
                        this.checkingContext.getProvidesTypeMirror().stream().map(TypeMirror::toString).collect(Collectors.joining(","));
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(MISSING_SERVICE_IMPLEMENTATION_ERROR, listOfProvidesTypeMirror));
            }

            if (!this.checkingContext.getProducesTypeMirror().isEmpty()) {
                String listOfProducesTypeMirror =
                        this.checkingContext.getProducesTypeMirror().stream().map(TypeMirror::toString).collect(Collectors.joining(","));
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(MISSING_PRODUCED_EVENTS_ERROR, listOfProducesTypeMirror));
            }
        }

        return result;
    }

    @Override
    public Boolean visitTypeAsEnum(TypeElement componentImplElement, Void unused) {
        boolean result = true;

        // check that implementation is not an enum
        if (this.checkingContext.getCurrentElement().equals(componentImplElement)) {
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(IMPL_NOT_A_CLASS_ERROR, componentImplElement));
            result = false;
        }

        return result;
    }

    private boolean checkComponentType(TypeElement componentImplElement) {
        // check that an interface with a ComponentType annotation is implemented only once
        List<TypeElement> componentTypesInterfaces = ComponentVisitor.getAnnotatedInterfaces(componentImplElement,
                ComponentType.class, this.typeUtils);
        if (componentTypesInterfaces.size() == 0) {
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(MISSING_TYPE_ANNOTATION_ERROR, componentImplElement));
            // cannot go further, returns
            return false;
        } else if (componentTypesInterfaces.size() > 1) {
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(TOO_MANY_TYPE_ANNOTATIONS_ERROR, componentImplElement));
            // cannot go further, returns
            return false;
        }

        // working on component type
        TypeElement componentTypeElement = componentTypesInterfaces.get(0);

        // extracting info from @ComponentType annotation
        Map<String, List<TypeMirror>> typeMirrorListPerAnnotationKey = new HashMap<>();
        typeMirrorListPerAnnotationKey.put("provides", this.checkingContext.getProvidesTypeMirror());
        typeMirrorListPerAnnotationKey.put("produces", this.checkingContext.getProducesTypeMirror());
        ComponentVisitor.extractComponentAnnotationInfo(componentTypeElement, ComponentType.class,
                typeMirrorListPerAnnotationKey, this.elementUtils);

        return true;
    }

    private boolean checkComponentImplementation(TypeElement componentImplElement) {
        // nothing to do at the moment
        return true;
    }

    private boolean checkNamingConvention(TypeElement componentImplElement) {
        boolean result = true;

        // get the impl's type (check on type has been made)
        List<TypeElement> componentTypesInterfaces = ComponentVisitor.getAnnotatedInterfaces(componentImplElement,
                ComponentType.class, this.typeUtils);
        TypeElement componentTypeElement = componentTypesInterfaces.get(0);

        String componentTypeName = componentTypeElement.getSimpleName().toString();
        String componentImplName = componentImplElement.getSimpleName().toString();

        String packageTypeName = this.elementUtils.getPackageOf(componentTypeElement).getQualifiedName().toString();
        String packageImplName = this.elementUtils.getPackageOf(componentImplElement).getQualifiedName().toString();

        // check component's type naming rules
        if (!packageTypeName.toLowerCase().endsWith(componentTypeName.toLowerCase())) {
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(TYPE_PACKAGE_NAMING_ERROR, componentTypeElement, componentTypeName.toLowerCase()));
            result = false;
        }

        // check component's impl naming rules
        if (!componentImplName.endsWith(componentTypeName.concat("Impl"))) {
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(IMPL_NAMING_ERROR, componentImplElement.toString(), componentTypeName));
            result = false;
        }

        if (!packageImplName.toLowerCase().endsWith(componentTypeName.toLowerCase().concat(".impl"))) {
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(IMPL_PACKAGE_NAMING_ERROR, componentImplElement, componentTypeName.toLowerCase().concat(".impl")));
            result = false;
        }

        return result;
    }

    @Override
    public Boolean visitVariableAsField(final VariableElement aVariableElement, final Void unused) {
        boolean result = true;

        // check id field
        if (aVariableElement.getAnnotation(Id.class) != null) {
            result = this.checkIdField(aVariableElement);
        }

        // check provided service
        if (aVariableElement.getAnnotation(ProvidedService.class) != null) {
            result &= this.checkProvidedService(aVariableElement);
        }

        // check used service
        if (aVariableElement.getAnnotation(UsedService.class) != null) {
            result &= this.checkUsedService(aVariableElement);
        }

        // check events source
        if (aVariableElement.getAnnotation(EventSource.class) != null) {
            result &= this.checkEventsSource(aVariableElement);
        }

        // check events sink
        if (aVariableElement.getAnnotation(EventSink.class) != null) {
            result &= this.checkEventsSink(aVariableElement);
        }

        // check parameters
        if (aVariableElement.getAnnotation(Parameter.class) != null) {
            result &= this.checkParametersField(aVariableElement);
        }

        return result;
    }

    private boolean checkIdField(final VariableElement aVariableElement) {
        // check field type
        boolean fieldClassOK = true;

        // TODO check that Id field is unique

        Element fieldElement = typeUtils.asElement(aVariableElement.asType());
        if (fieldElement instanceof TypeElement) {
            Class<?> fieldClass = JavaxLangModelUtils.typeElementToClass((TypeElement) fieldElement);
            if (!InstanceId.class.equals(fieldClass)) {
                fieldClassOK = false;
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(ID_INSTANCE_ID_TYPED_ERROR, aVariableElement, InstanceId.class.getCanonicalName()));
            }
        }

        // check field's modifiers
        if (!(aVariableElement.getModifiers().contains(Modifier.PROTECTED)
                || aVariableElement.getModifiers().contains(Modifier.PRIVATE))) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(ID_SIGNATURE_ERROR, aVariableElement, "private or protected"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.FINAL)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(ID_SIGNATURE_ERROR, aVariableElement, "final"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.TRANSIENT)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(ID_SIGNATURE_ERROR, aVariableElement, "transient"));
        }

        return fieldClassOK;
    }

    private boolean checkProvidedService(final VariableElement aVariableElement) {
        // check field type
        boolean fieldClassOK = true;

        TypeMirror fieldTypeMirror = aVariableElement.asType();
        Element fieldTypeElement = this.typeUtils.asElement(fieldTypeMirror);
        if (!ElementKind.INTERFACE.equals(fieldTypeElement.getKind())) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PROVIDED_SERVICE_TYPE_INTERFACE_ERROR, aVariableElement));
        }

        if (fieldTypeElement.getAnnotation(ComponentService.class) == null) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PROVIDED_SERVICE_ANNOTATION_ERROR, aVariableElement, ComponentService.class.getSimpleName()));
        }

        if (!this.checkingContext.getProvidesTypeMirror().remove(fieldTypeMirror)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(UNDECLARED_PROVIDED_SERVICE_ERROR, aVariableElement, ComponentType.class.getSimpleName()));
        }

        // check field's modifiers
        if (!(aVariableElement.getModifiers().contains(Modifier.PUBLIC)
                || aVariableElement.getModifiers().contains(Modifier.PROTECTED)
                || aVariableElement.getModifiers().contains(Modifier.PRIVATE))) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PROVIDED_SERVICE_SIGNATURE_ERROR, aVariableElement, "public or protected or private"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.FINAL)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PROVIDED_SERVICE_SIGNATURE_ERROR, aVariableElement, "final"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.TRANSIENT)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PROVIDED_SERVICE_SIGNATURE_ERROR, aVariableElement, "transient"));
        }

        return fieldClassOK;
    }

    private boolean checkUsedService(final VariableElement aVariableElement) {
        // check field type
        boolean fieldClassOK = true;

        TypeMirror fieldTypeMirror = aVariableElement.asType();
        TypeMirror fieldTypeErasure = this.typeUtils.erasure(fieldTypeMirror);
        Element fieldTypeElement;
        if (java.util.Map.class.getName().equals(fieldTypeErasure.toString())) {
            // Case where used service is a map of used services
            // -> check first type argument is typed InstanceId
            // -> get second type argument as a TypeElement
            // NOTE: we can safely cast to DeclaredType because annotation can only be set on fields
            List<? extends TypeMirror> mapTypeArguments = ((DeclaredType) fieldTypeMirror).getTypeArguments();

            Class<?> firstTypeArgument = JavaxLangModelUtils.typeMirrorToClass(mapTypeArguments.get(0));
            if (!InstanceId.class.equals(firstTypeArgument)) {
                fieldClassOK = false;
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(USED_SERVICE_MAP_KEY_INSTANCE_ID_ERROR, aVariableElement, InstanceId.class.getCanonicalName()));
            }

            fieldTypeElement = this.typeUtils.asElement(mapTypeArguments.get(1));
        } else {
            fieldTypeElement = this.typeUtils.asElement(fieldTypeMirror);
        }

        if (!ElementKind.INTERFACE.equals(fieldTypeElement.getKind())) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(USED_SERVICE_TYPE_INTERFACE_ERROR, aVariableElement));
        }

        if (fieldTypeElement.getAnnotation(ComponentService.class) == null) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(USED_SERVICE_ANNOTATION_ERROR, aVariableElement, ComponentService.class.getSimpleName()));
        }

        // check field's modifiers
        if (!(aVariableElement.getModifiers().contains(Modifier.PROTECTED)
                || aVariableElement.getModifiers().contains(Modifier.PRIVATE))) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(USED_SERVICE_SIGNATURE_ERROR, aVariableElement, "protected or private"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.FINAL)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(USED_SERVICE_SIGNATURE_ERROR, aVariableElement, "final"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.TRANSIENT)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(USED_SERVICE_SIGNATURE_ERROR, aVariableElement, "transient"));
        }

        return fieldClassOK;
    }

    private boolean checkEventsSource(final VariableElement aVariableElement) {
        // check field type
        boolean fieldClassOK = true;

        TypeMirror fieldTypeMirror = aVariableElement.asType();
        TypeMirror fieldTypeErasure = this.typeUtils.erasure(fieldTypeMirror);

        // check that source is a generic typed with @Source
        // NOTE: we can safely cast to DeclaredType because annotation can only be set on fields
        List<? extends TypeMirror> sourceTypes = ((DeclaredType) fieldTypeMirror).getTypeArguments();
        if (sourceTypes.size() == 0 || !Source.class.getName().equals(fieldTypeErasure.toString())) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SOURCE_TYPE_ERROR, aVariableElement, Source.class.getCanonicalName()));
        }
        else {
            // check that source data is a class or an enum annotated with @EventData
            TypeMirror sourceDataTypeMirror = sourceTypes.get(0);
            Element sourceDataTypeElement = this.typeUtils.asElement(sourceDataTypeMirror);

            if (!(ElementKind.CLASS.equals(sourceDataTypeElement.getKind())
                    || ElementKind.ENUM.equals(sourceDataTypeElement.getKind()))) {
                fieldClassOK = false;
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SOURCE_DATA_TYPE_CLASS_OR_ENUM_ERROR, aVariableElement, sourceDataTypeElement));
            }

            if (sourceDataTypeElement.getAnnotation(EventData.class) == null) {
                fieldClassOK = false;
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SOURCE_DATA_TYPE_ANNOTATION_ERROR, aVariableElement, sourceDataTypeElement, EventData.class.getSimpleName()));
            }

            if (!this.checkingContext.getProducesTypeMirror().remove(sourceDataTypeMirror)) {
                fieldClassOK = false;
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(UNDECLARED_PRODUCED_EVENT_ERROR, aVariableElement, sourceDataTypeElement, ComponentType.class.getSimpleName()));
            }
        }

        // check field's modifiers
        if (!(aVariableElement.getModifiers().contains(Modifier.PUBLIC)
                || aVariableElement.getModifiers().contains(Modifier.PROTECTED)
                || aVariableElement.getModifiers().contains(Modifier.PRIVATE))) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SOURCE_SIGNATURE_ERROR, aVariableElement, "public or protected or private"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.FINAL)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SOURCE_SIGNATURE_ERROR, aVariableElement, "final"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.TRANSIENT)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SOURCE_SIGNATURE_ERROR, aVariableElement, "transient"));
        }

        return fieldClassOK;
    }

    private boolean checkEventsSink(final VariableElement aVariableElement) {
        // check field type
        boolean fieldClassOK = true;

        TypeMirror fieldTypeMirror = aVariableElement.asType();
        TypeMirror fieldTypeErasure = this.typeUtils.erasure(fieldTypeMirror);

        // check that sink is a generic typed with @Sink
        // NOTE: we can safely cast to DeclaredType because annotation can only be set on fields
        List<? extends TypeMirror> sinkTypes = ((DeclaredType) fieldTypeMirror).getTypeArguments();
        if (sinkTypes.size() == 0 || !Sink.class.getName().equals(fieldTypeErasure.toString())) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SINK_TYPE_ERROR, aVariableElement, Sink.class.getCanonicalName()));
        }
        else {
            // check that sink data is a class or an enum annotated with @EventData
            Element sinkDataTypeElement = this.typeUtils.asElement(sinkTypes.get(0));

            if (!(ElementKind.CLASS.equals(sinkDataTypeElement.getKind())
                    || ElementKind.ENUM.equals(sinkDataTypeElement.getKind()))) {
                fieldClassOK = false;
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SINK_DATA_TYPE_CLASS_OR_ENUM_ERROR, aVariableElement, sinkDataTypeElement));
            }

            if (sinkDataTypeElement.getAnnotation(EventData.class) == null) {
                fieldClassOK = false;
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SINK_DATA_TYPE_ANNOTATION_ERROR, aVariableElement, sinkDataTypeElement, EventData.class.getSimpleName()));
            }
        }

        // check field's modifiers
        if (!aVariableElement.getModifiers().contains(Modifier.PRIVATE)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SINK_SIGNATURE_ERROR, aVariableElement, "private"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.FINAL)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SINK_SIGNATURE_ERROR, aVariableElement, "final"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.TRANSIENT)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(SINK_SIGNATURE_ERROR, aVariableElement, "transient"));
        }

        return fieldClassOK;
    }

    private boolean checkParametersField(final VariableElement aVariableElement) {
        // check field type
        boolean fieldClassOK = true;

        TypeMirror fieldTypeMirror = aVariableElement.asType();
        TypeMirror fieldTypeErasure = this.typeUtils.erasure(fieldTypeMirror);

        // check that sink is a generic typed with @Parameters
        // NOTE: we can safely cast to DeclaredType because annotation can only be set on fields
        List<? extends TypeMirror> sinkTypes = ((DeclaredType) fieldTypeMirror).getTypeArguments();
        if (sinkTypes.size() == 0 || !Parameters.class.getName().equals(fieldTypeErasure.toString())) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PARAMETERS_TYPE_ERROR, aVariableElement, Parameters.class.getCanonicalName()));
        }
        else {
            // check that parameter data is an enum annotated with @ParameterData
            Element parameterDataTypeElement = this.typeUtils.asElement(sinkTypes.get(0));

            if (parameterDataTypeElement.getAnnotation(ParameterData.class) == null) {
                fieldClassOK = false;
                this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PARAMETERS_DATA_TYPE_ANNOTATION_ERROR, aVariableElement, parameterDataTypeElement, ParameterData.class.getSimpleName()));
            }
        }

        // check field's modifiers
        if (!(aVariableElement.getModifiers().contains(Modifier.PROTECTED)
                || aVariableElement.getModifiers().contains(Modifier.PRIVATE))) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PARAMETERS_SIGNATURE_ERROR, aVariableElement, "private or protected"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.FINAL)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PARAMETERS_SIGNATURE_ERROR, aVariableElement, "final"));
        }

        if (!aVariableElement.getModifiers().contains(Modifier.TRANSIENT)) {
            fieldClassOK = false;
            this.messager.printMessage(Diagnostic.Kind.ERROR, String.format(PARAMETERS_SIGNATURE_ERROR, aVariableElement, "transient"));
        }

        return fieldClassOK;
    }

    private static class CheckingContext {
        private final List<TypeMirror> providesTypeMirror;
        private final List<TypeMirror> producesTypeMirror;
        private final Element currentElement;

        public CheckingContext(Element anElement) {
            this.currentElement = anElement;
            this.providesTypeMirror = new ArrayList<>();
            this.producesTypeMirror = new ArrayList<>();
        }

        public Element getCurrentElement() {
            return currentElement;
        }

        public List<TypeMirror> getProvidesTypeMirror() {
            return providesTypeMirror;
        }

        public List<TypeMirror> getProducesTypeMirror() {
            return producesTypeMirror;
        }
    }
}

