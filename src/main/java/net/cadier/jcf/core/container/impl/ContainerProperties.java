package net.cadier.jcf.core.container.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Function;
import java.util.logging.Level;

enum ContainerProperties {
    INSTANCE;

    private static final String PROPERTIES_FILE_NAME = "jcf.properties";

    private static final String LOG_LEVEL_PROPERTY = "jcf.log.level";
    private static final String CHECKER_EXIT_ON_ERROR_PROPERTY = "jcf.checker.exit.on.error";

    private Properties properties = new Properties();

    ContainerProperties() {
        this.reloadProperties();
    }

    public void reloadProperties() {
        try (InputStream fileInputStream = this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);) {
            // input stream can be null if file is not found
            if (Objects.nonNull(fileInputStream)) {
                this.properties.load(fileInputStream);
            }
        } catch(IOException ioe) {
            ioe.printStackTrace();
            System.exit(-1);
        }
    }

    public Level getLoggingLevel() {
        return this.extractProperty(LOG_LEVEL_PROPERTY, Level::parse);
    }

    public boolean isCheckerExitOnError() {
        return this.extractProperty(CHECKER_EXIT_ON_ERROR_PROPERTY, Boolean::getBoolean);
    }

    private <T> T extractProperty(final String key, final Function<String, T> tSupplier) {
        T result = null;

        try {
            String property = this.properties.getProperty(key);
            if (Objects.nonNull(property)) {
                result = tSupplier.apply(property);
            }
        } catch (IllegalArgumentException iae) {
            // illegal value, print and continue with default value
            iae.printStackTrace();
        }

        return result;
    }
}
