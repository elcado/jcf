/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.container;

import net.cadier.jcf.annotations.ComponentService;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.exception.DeploymentRuleException;
import net.cadier.jcf.exception.MalformedComponentException;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.utils.reflection.ReflectionException;

@ComponentService
public interface ContainerServices {
    /**
     * Discovers and registers a new component in the {@link net.cadier.jcf.core.componentsManager.ComponentsManager ComponentsManager}.
     *
     * @param componentImplementation The class of a component's implementation.
     * @throws MalformedComponentException exception thrown if component is malformed.
     */
    void registerComponent(Class<?> componentImplementation) throws MalformedComponentException;

    /**
     * Deploy, and eventually start, a new instance of a registered component in the {@link net.cadier.jcf.core.instancesManager.InstancesManager InstancesManager}.
     *
     * @param parentId      The optional instance id of the parent of the deployed instance.
     * @param componentType The class of the component to instantiate.
     * @param autoStart     When set to true, and if the component implements {@link net.cadier.jcf.lang.component.Startable Startable}, the deployed instance will be automatically started.
     *
     * @return the instance id of the deployed instance. Null if the component is not registered.
     *
     * @throws DeploymentRuleException        exception thrown if deployment rules are violated.
     * @throws DependencyResolutionException  exception thrown if component's dependencies cannot be fulfilled.
     * @throws ReflectionException            exception thrown on reflection error.
     */
    InstanceId deployInstance(final InstanceId parentId, Class<?> componentType, final boolean autoStart) throws DeploymentRuleException, DependencyResolutionException, ReflectionException;

    /**
     * Start supplied instance if it is deployed and it implements {@link net.cadier.jcf.lang.component.Startable
     * Startable}. Do nothing otherwise.
     *
     * @param instanceId The instance id of the instance to remove.
     */
    void startInstance(final InstanceId instanceId);

    /**
     * Pauses supplied instance if it is deployed and it implements {@link net.cadier.jcf.lang.component.Startable
     * Startable}. Do nothing otherwise.
     *
     * @param instanceId The instance id of the instance to remove.
     */
    void pauseInstance(final InstanceId instanceId);

    /**
     * Resumes supplied instance if it is deployed and it implements {@link net.cadier.jcf.lang.component.Startable
     * Startable}. Do nothing otherwise.
     *
     * @param instanceId The instance id of the instance to remove.
     */
    void  resumeInstance(final InstanceId instanceId);

    /**
     * Stop supplied instance if it is deployed and it implements {@link net.cadier.jcf.lang.component.Startable
     * Startable}. Do nothing otherwise.
     *
     * @param instanceId The instance id of the instance to remove.
     */
    void stopInstance(final InstanceId instanceId);

    /**
     * Undeploy an instance.
     *
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been undeployed, false otherwise
     */
    boolean undeployInstance(final InstanceId instanceId);

    /**
     * Unregister a component.
     *
     * @param componentImplementation The class of a component's implementation.
     * @return true if the component's implementation has been correctly registered, false otherwise.
     */
    boolean unregisterComponent(Class<?> componentImplementation);

    /**
     * Terminate container.
     *
     * @param forceClose if true, all manually started instances will be forced to stop immediately.
     */
    void terminate(final boolean forceClose);
}
