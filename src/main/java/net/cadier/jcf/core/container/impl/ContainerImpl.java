/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.container.impl;

import net.cadier.jcf.annotations.*;
import net.cadier.jcf.core.componentsManager.ComponentsManagerServices;
import net.cadier.jcf.core.componentsManager.impl.ComponentsManagerImpl;
import net.cadier.jcf.core.container.Container;
import net.cadier.jcf.core.container.ContainerDebugServices;
import net.cadier.jcf.core.container.ContainerServices;
import net.cadier.jcf.core.instancesManager.InstancesManagerDebugServices;
import net.cadier.jcf.core.instancesManager.InstancesManagerDeploymentServices;
import net.cadier.jcf.core.instancesManager.InstancesManagerLifeCycleServices;
import net.cadier.jcf.core.instancesManager.impl.InstancesManagerImpl;
import net.cadier.jcf.core.instancesManager.impl.dependenciesResolver.impl.DependenciesResolverImpl;
import net.cadier.jcf.core.instancesManager.impl.instanceStateManager.impl.InstanceStateManagerImpl;
import net.cadier.jcf.core.instancesManager.impl.parametersManager.impl.ParametersManagerImpl;
import net.cadier.jcf.core.logging.LoggingParameters;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.core.logging.impl.JDKLoggingImpl;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.exception.DeploymentRuleException;
import net.cadier.jcf.exception.MalformedComponentException;
import net.cadier.jcf.lang.component.Component;
import net.cadier.jcf.lang.component.Startable;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.parameters.Parameters;
import net.cadier.utils.reflection.ReflectionException;
import net.cadier.utils.reflection.ReflectionUtils;
import net.cadier.utils.tree.Tree;

import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

@ComponentImplementation
public class ContainerImpl implements Container, ContainerServices, Startable {

    /**
     * Express in ms the time the terminate() will wait for manually started instances to call stopInstance().
     */
    private static final long TIMEOUT_FOR_MANUALLY_STARTED_INSTANCES = 10000;
    @ProvidedService
    public final transient ContainerServices containerServices = this;
    /**
     * List of roots' instances.
     */
    private final List<InstanceId> rootInstanceIds = new ArrayList<>();
    /**
     * List of instances that have been manually started and that have to be manually stopped.
     */
    private final List<InstanceId> manuallyStartedInstanceIds = new ArrayList<>();
    @Id
    private final transient InstanceId thisId = null;
    @UsedService
    private final transient InstancesManagerDebugServices instancesManagerDebugServices = null;
    @ProvidedService
    public final transient ContainerDebugServices containerDebugServices = new ContainerDebugServices() {
        @Override
        public InstanceId getLocalContainerId() {
            return thisId;
        }

        @Override
        public List<InstanceId> getRemoteContainersIds() {
            return new ArrayList<>();
        }

        @Override
        public Tree<InstanceId, Instance> getLocalInstancesTree() {
            return instancesManagerDebugServices.getInstancesTree();
        }

        @Override
        public Map<InstanceId, Tree<InstanceId, Instance>> getRemoteInstancesTree() {
            return new HashMap<>();
        }
    };
    @UsedService
    private final transient LoggingServices loggingServices = null;

    @UsedService
    private final transient ComponentsManagerServices componentsManagerServices = null;

    @UsedService
    private final transient InstancesManagerDeploymentServices instancesManagerDeploymentServices = null;

    @UsedService
    private final transient InstancesManagerLifeCycleServices instancesManagerLifeCycleServices = null;

    @Parameter
    private final transient Parameters<LoggingParameters> loggingParameters = null;

    /**
     * Private constructor: a jCF component is not supposed to be built directly from user code.
     */
    private ContainerImpl() {
    }

    /**
     * Setup a jCF {@link Container}.
     *
     * @param roots list of eventual object's instance to encapsulate in jCF instances.
     * @return the container's {@link ContainerServices} facet.
     */
    public static ContainerServices initialize(final Object... roots) {
        boolean withLogging = false;

        // build a proto container that knows nothing, and extract the instances it contains
        ContainerImpl container = null;

        Object componentsManager = null;
        Object instancesManager = null;
        Object dependenciesResolver = null;
        Object parametersManager = null;
        Object instanceStateManager = null;
        Object loggingServices = null;

        try {
            container = buildProtoContainer(withLogging);

            componentsManager = ReflectionUtils.getFieldValue(container, "componentsManagerServices");
            instancesManager = ReflectionUtils.getFieldValue(container, "instancesManagerDeploymentServices");
            dependenciesResolver = ReflectionUtils.getFieldValue(instancesManager, "dependenciesResolverServices");
            parametersManager = ReflectionUtils.getFieldValue(instancesManager, "parametersManagerService");
            instanceStateManager = ReflectionUtils.getFieldValue(instancesManager, "instanceStateManagerServices");
            if (withLogging) {
                loggingServices = ReflectionUtils.getFieldValue(container, "loggingServices");
            }
        } catch (ReflectionException re) {
            re.printStackTrace();
            System.exit(-1);
        }

        // make it knows itself
        try {
            container.registerComponent(ContainerImpl.class);
            container.registerComponent(ComponentsManagerImpl.class);
            container.registerComponent(InstancesManagerImpl.class);
            container.registerComponent(DependenciesResolverImpl.class);
            container.registerComponent(ParametersManagerImpl.class);
            container.registerComponent(InstanceStateManagerImpl.class);
            container.registerComponent(JDKLoggingImpl.class);
        } catch (MalformedComponentException mce) {
            // MalformedComponentException cannot occur here 'cause this strange hacked auto deployment has been exhaustively tested
            mce.printDiagnostic(System.err);
            container.containerServices.terminate(true);
            System.exit(-1);
        }

        // register eventual supplied roots
        for (Object root : roots) {
            try {
                container.registerComponent(root.getClass());
            } catch (MalformedComponentException mce) {
                mce.printDiagnostic(System.err);
                container.containerServices.terminate(true);
                System.exit(-1);
            }
        }

        // make it deploys itself (autostart for all but the container)
        try {
            InstanceId containerId = container.deployInstance(null, ContainerImpl.class, container, true);
            container.deployInstance(containerId, ComponentsManagerImpl.class, componentsManager, true);
            InstanceId instancesManagerId = container.deployInstance(containerId, InstancesManagerImpl.class, instancesManager, true);
            container.deployInstance(instancesManagerId, DependenciesResolverImpl.class, dependenciesResolver, true);
            container.deployInstance(instancesManagerId, ParametersManagerImpl.class, parametersManager, true);
            container.deployInstance(instancesManagerId, InstanceStateManagerImpl.class, instanceStateManager, true);

            InstanceId loggingId = container.deployInstance(null, JDKLoggingImpl.class, loggingServices, true);

            Level loggingLevel = ContainerProperties.INSTANCE.getLoggingLevel();
            if (Objects.nonNull(loggingLevel)) {
                container.loggingParameters.set(loggingId, LoggingParameters.LOGGING_LEVEL, loggingLevel);
            }

            // deploy eventual supplied roots
            for (Object root : roots) {
                container.deployInstance(null, root.getClass(), root, true);
            }
        } catch (DeploymentRuleException | DependencyResolutionException | ReflectionException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        // return the ContainerServices facet
        return container.containerServices;
    }

    private static ContainerImpl buildProtoContainer(final boolean withLogging) throws ReflectionException {
        // core components
        ContainerImpl protoContainer = ReflectionUtils.buildObject(ContainerImpl.class);
        ComponentsManagerImpl componentsManager = ReflectionUtils.buildObject(ComponentsManagerImpl.class);
        InstancesManagerImpl instancesManager = ReflectionUtils.buildObject(InstancesManagerImpl.class);
        DependenciesResolverImpl dependenciesResolver = ReflectionUtils.buildObject(DependenciesResolverImpl.class);
        ParametersManagerImpl parametersManager = ReflectionUtils.buildObject(ParametersManagerImpl.class);
        InstanceStateManagerImpl instanceStateManager = ReflectionUtils.buildObject(InstanceStateManagerImpl.class);

        ReflectionUtils.setFieldValue(protoContainer, "componentsManagerServices", componentsManager);
        ReflectionUtils.setFieldValue(protoContainer, "instancesManagerDeploymentServices", instancesManager);
        ReflectionUtils.setFieldValue(protoContainer, "instancesManagerLifeCycleServices", instancesManager);
        ReflectionUtils.setFieldValue(instancesManager, "dependenciesResolverServices", dependenciesResolver);
        ReflectionUtils.setFieldValue(instancesManager, "parametersManagerService", parametersManager);
        ReflectionUtils.setFieldValue(instancesManager, "instanceStateManagerServices", instanceStateManager);

        // logging
        if (withLogging) {
            JDKLoggingImpl logging = ReflectionUtils.buildObject(JDKLoggingImpl.class);

            ReflectionUtils.setFieldValue(protoContainer, "loggingServices", logging);
            ReflectionUtils.setFieldValue(componentsManager, "loggingServices", logging);
            ReflectionUtils.setFieldValue(instancesManager, "loggingServices", logging);
            ReflectionUtils.setFieldValue(dependenciesResolver, "loggingServices", logging);
            ReflectionUtils.setFieldValue(instanceStateManager, "loggingServices", logging);
        }

        return protoContainer;
    }

    @Override
    public boolean onStarted() {
        return true;
    }

    @Override
    public boolean onPaused() {
        return true;
    }

    @Override
    public boolean onResumed() {
        return true;
    }

    @Override
    public boolean onStopped() {
        return true;
    }

    @Override
    public boolean onFailed() {
        return true;
    }

    @Override
    public void terminate(final boolean forceClose) {
        /*
         *  if not forced to close, wait until manuallyStartedInstanceIds list is empty
         */

        if (!forceClose) {
            synchronized (this.manuallyStartedInstanceIds) {
                if (!this.manuallyStartedInstanceIds.isEmpty()) {
                    try {
                        this.manuallyStartedInstanceIds.wait(TIMEOUT_FOR_MANUALLY_STARTED_INSTANCES);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // log that lasting manually started instances will be automatically stopped (appends on timeout of
                    // previous wait)
                    if (!this.manuallyStartedInstanceIds.isEmpty() && this.loggingServices != null) {
                        this.loggingServices.warn("Some manually started instances haven't been " +
                                "stopped and will be automatically stopped");
                    }
                }
            }
        }

        /*
         *  Undeploy all roots
         */

        List<InstanceId> allRootsInstancesButContainer = new ArrayList<>(this.rootInstanceIds);

        // stop all roots but container
        allRootsInstancesButContainer.stream().forEach(this::stopInstance);

        // undeploy all roots but container
        allRootsInstancesButContainer.stream().forEach(this::undeployInstance);

        // check that all instances have been undeployed
        if (!this.rootInstanceIds.isEmpty()) {
            // not all root instances have been undeployed
            String remainingRoots = this.rootInstanceIds.stream()
                    .map(rootId -> "\t'" + rootId + "'")
                    .collect(Collectors.joining("\n"));
            // TODO raise exception ?
            System.err.println("Not all root instances have been undeployed. Remaining roots are:\n" + remainingRoots);
        }

        /*
         *  Unregister all components
         */

        // unregister all components
        List<Component> allRegisteredComponents = new ArrayList<>(this.componentsManagerServices.getRegisteredComponents());
        allRegisteredComponents.forEach(this.componentsManagerServices::unregisterComponent);

        // assert that all components (including Container) have been unregistered
        if (!this.componentsManagerServices.getRegisteredComponents().isEmpty()) {
            // TODO raise exception ?
            System.err.println("Not all components have been unregistered");
        }
    }

    @Override
    public void registerComponent(Class<?> componentImplementation) throws MalformedComponentException {
        this.componentsManagerServices.registerComponent(componentImplementation);
    }

    @Override
    public InstanceId deployInstance(final InstanceId parentId, Class<?> componentType, final boolean autoStart) throws DeploymentRuleException, DependencyResolutionException, ReflectionException {
        return this.deployInstance(parentId, componentType, null, autoStart);
    }

    /**
     * Builds (if not supplied), deploys, and eventually starts, a new instance of a registered component in the
     * {@link net.cadier.jcf.core.instancesManager.InstancesManager InstancesManager}. If the component has composed
     * components, instances of the composed components are
     *
     * @param parentId      The optional instance id of the parent of the deployed instance.
     * @param componentType The class of the component to instantiate.
     * @param object        An existing instance of the component.
     * @param autoStart     When set to true, and if the component implements {@link net.cadier.jcf.lang.component.Startable Startable}, the deployed instance will be automatically started.
     * @return the instance id of the deployed instance. Null if the component is not registered.
     * @throws DeploymentRuleException        exception thrown if deployment rules are violated for the instance or one of .
     * @throws DependencyResolutionException  exception thrown if component's dependencies cannot be fulfilled.
     * @throws ReflectionException            exception thrown on reflection error.
     */
    private InstanceId deployInstance(final InstanceId parentId, Class<?> componentType, Object object,
                                      final boolean autoStart) throws DeploymentRuleException, DependencyResolutionException, ReflectionException {
        InstanceId instanceId = null;

        Component component = this.componentsManagerServices.getComponent(componentType);
        if (component != null) {
            // deploy instance
            instanceId = this.instancesManagerDeploymentServices.deployInstance(parentId, component, object);

            // eventually add instance id in root list
            if (parentId == null) {
                this.rootInstanceIds.add(instanceId);
            }

            // initialize instance (might end in FAILED state, but this will be logged
            this.instancesManagerLifeCycleServices.initInstance(instanceId);

            boolean initialized = this.instancesManagerLifeCycleServices.isInitialized(instanceId);

            // eventually start instance,
            // otherwise add instance id in list of instances that won't be automatically undeployed on terminate

            if (autoStart) {
                this.startInstance(instanceId);
            } else {
                synchronized (this.manuallyStartedInstanceIds) {
                    this.manuallyStartedInstanceIds.add(instanceId);
                }
            }
        } else {
            throw new DeploymentRuleException(componentType, "Cannot deploy instance of an unregistered component: '" + componentType.getCanonicalName() + "'.");
        }
        return instanceId;
    }

    @Override
    public void startInstance(final InstanceId instanceId) {
        this.instancesManagerLifeCycleServices.startInstance(instanceId);
    }

    @Override
    public void pauseInstance(final InstanceId instanceId) {
        this.instancesManagerLifeCycleServices.pauseInstance(instanceId);
    }

    @Override
    public void resumeInstance(final InstanceId instanceId) {
        this.instancesManagerLifeCycleServices.resumeInstance(instanceId);
    }

    @Override
    public void stopInstance(final InstanceId instanceId) {
        this.instancesManagerLifeCycleServices.stopInstance(instanceId);

        // eventually remove from manuallyStartedInstanceIds
        synchronized (this.manuallyStartedInstanceIds) {
            if (this.instancesManagerLifeCycleServices.isStopped(instanceId) && this.manuallyStartedInstanceIds.contains(instanceId)) {
                this.manuallyStartedInstanceIds.remove(instanceId);

                // notify terminate()
                this.manuallyStartedInstanceIds.notify();
            }
        }
    }

    @Override
    public boolean undeployInstance(final InstanceId instanceId) {
        boolean isInstanceUndeployed = false;

        List<InstanceId> undeployInstanceIds = this.instancesManagerDeploymentServices.undeployInstance(instanceId);

        // instance is undeployed if the list of previously undeployed instance contains it
        isInstanceUndeployed = undeployInstanceIds.contains(instanceId);

        // eventually remove from rootInstanceIds
        if (isInstanceUndeployed) {
            this.rootInstanceIds.remove(instanceId);
        }

        return isInstanceUndeployed;
    }

    @Override
    public boolean unregisterComponent(Class<?> componentImplementation) {
        boolean result = false;

        Component component = this.componentsManagerServices.getComponent(componentImplementation);
        if (component != null) {
            if (this.loggingServices != null) {
                this.loggingServices.debug(Thread.currentThread() + ": Removing component [" + componentImplementation.getSimpleName() + "]");
            }

            // extract (and copy) instances' ids (to avoid concurrency pb)
            List<InstanceId> instanceIds = component.getInstancesIds();

            // TODO remove : should not stop and undeploy, user's job
            instanceIds.forEach(this::stopInstance);
            instanceIds.forEach(this::undeployInstance);

            // remove component
            this.componentsManagerServices.unregisterComponent(component);

            if (this.loggingServices != null) {
                this.loggingServices.debug(Thread.currentThread() + ": [" + componentImplementation.getSimpleName() + "] removed");
            }

            result = true;
        }
        return result;
    }

}


