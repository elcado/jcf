/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.core.logging.impl;

import net.cadier.jcf.annotations.*;
import net.cadier.jcf.core.logging.Logging;
import net.cadier.jcf.core.logging.LoggingParameters;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.events.Sink;
import net.cadier.jcf.lang.instance.parameters.Parameters;

import java.time.Instant;
import java.util.Arrays;
import java.util.Objects;
import java.util.logging.*;

/**
 * JDK implementation of {@link LoggingServices}.
 *
 * @author Frédéric Cadier
 */
@ComponentImplementation
public class JDKLoggingImpl implements Logging, LoggingServices {
    private final static Level DEFAULT_LOGGING_LEVEL = Level.SEVERE;

    @ProvidedService
    public final transient LoggingServices loggingServices = this;

    @Id
    private final transient InstanceId thisId = null;

    @Parameter
    private final transient Parameters<LoggingParameters> loggingParameters = null;

    @EventSink
    private final transient Sink<LoggingParameters> parameterEventSink = event -> {
        switch (event) {
            case LOGGING_LEVEL:
                // try to get specific parameter
                Level globalLoggingLevel = this.loggingParameters.get(LoggingParameters.LOGGING_LEVEL);

                // if null, try to get specific parameter
                if (Objects.isNull(globalLoggingLevel)) {
                    globalLoggingLevel = this.loggingParameters.get(this.thisId, LoggingParameters.LOGGING_LEVEL);
                }

                // if not null, set logging level from param
                if (Objects.nonNull(globalLoggingLevel)) {
                    this.loggingLevel = globalLoggingLevel;
                }
                // if null, reset to default
                else {
                    this.loggingLevel = DEFAULT_LOGGING_LEVEL;
                }

                break;
        }
    };

    private final Logger logger = Logger.getAnonymousLogger();

    private Level loggingLevel = DEFAULT_LOGGING_LEVEL;

    public JDKLoggingImpl() {
        // log handler
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.ALL); // we log everything here because we use a filter on each log method
        consoleHandler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                String formatString = "[%d] %s - %s - %s\n%s\n";
                return String.format(formatString, record.getMillis(), record.getParameters()[1], record.getParameters()[0], record.getParameters()[2],
                        record.getMessage());
            }
        });

        // default log level and filter
        this.logger.setLevel(Level.ALL); // we log everything here because we use a filter on each log method

        // set our handlers
        this.logger.setUseParentHandlers(false);
        this.logger.addHandler(consoleHandler);
    }

    /**
     * ERROR logging level.
     *
     * @param message the message to logging.
     */
    public void error(String message) {
        this.log(Level.SEVERE, message);
    }

    /**
     * INFO logging level.
     *
     * @param message the message to logging.
     */
    public void warn(String message) {
        this.log(Level.WARNING, message);
    }

    /**
     * INFO logging level.
     *
     * @param message the message to logging.
     */
    public void info(String message) {
        this.log(Level.INFO, message);
    }

    /**
     * DEBUG logging level.
     *
     * @param message the message to logging.
     */
    public void debug(String message) {
        this.log(Level.FINER, message);
    }

    /**
     * INFO logging level.
     *
     * @param message the message to logging.
     */
    public void trace(String message) {
        this.log(Level.FINEST, message);
    }

    private void log(Level recordLevel, String message) {
        // filter before building record to avoid load on filtered levels.
        if (this.loggingLevel.intValue() <= recordLevel.intValue()) {
            this.logger.log(this.buildRecord(recordLevel, message));
        }
    }

    private LogRecord buildRecord(Level finer, String message) {
        LogRecord record = new LogRecord(finer, message);
        record.setInstant(Instant.now());

        record.setParameters(Arrays.asList(Thread.currentThread(),
                Thread.currentThread().getStackTrace()[3].getMethodName().toUpperCase(),
                Thread.currentThread().getStackTrace()[4]).toArray());

        return record;
    }
}
