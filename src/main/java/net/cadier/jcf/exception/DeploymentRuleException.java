package net.cadier.jcf.exception;

import net.cadier.jcf.lang.instance.InstanceId;

/**
 * Exception raised when a component deployment is violating ont of the following rule:
 * <ul>
 *     <li>The component has not been previously registered.</li>
 *     <li>The component is to be deployed as root, but an instance of the component is already deployed as root.</li>
 *     <li>The component is to be deployed under an instance of another component, but an instance of the component is already deployed as root.</li>
 * </ul>
 */
public class DeploymentRuleException extends Exception {
    private InstanceId parentId;
    private Class<?> componentType;

    public DeploymentRuleException(Class<?> aComponentType, String message) {
        super(message);

        this.parentId = null;
        this.componentType = aComponentType;
    }

    public DeploymentRuleException(InstanceId anInstanceId, Class<?> aComponentType, String message) {
        super(message);

        this.parentId = anInstanceId;
        this.componentType = aComponentType;
    }

    /**
     * Instance id of the "should-be" parent.
     * @return an InstanceId.
     */
    public InstanceId getParentId() {
        return parentId;
    }

    /**
     * Component type concerned by this exception.
     * @return a Class.
     */
    public Class<?> getComponentType() {
        return componentType;
    }
}
