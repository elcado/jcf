package net.cadier.jcf.exception;

import net.cadier.jcf.lang.instance.InstanceId;

/**
 * Exception raised when one of the following dependency problem is met:
 * <ul>
 *     <li>A @UsedService cannot be injected because to many implementations are found in neighborhood.</li>
 *     <li>A @ImplementedService cannot be injected because a service has already been injected.</li>
 *     <li>While trying to find a service implementation or usage, duplicate @ImplementedService or @UsedService has been found in another component.</li>
 *     <li>While trying to find an event source or sink, duplicate @EventsSource or @EventsSink has been found in another component.</li>
 * </ul>
 */
public class DependencyResolutionException extends Exception {
    private InstanceId instanceId;

    public DependencyResolutionException(InstanceId anInstanceId, String message) {
        super(message);

        this.instanceId = anInstanceId;
    }

    /**
     * Instance id of the concerned instance.
     * @return an InstanceId.
     */
    public InstanceId getInstanceId() {
        return instanceId;
    }
}
