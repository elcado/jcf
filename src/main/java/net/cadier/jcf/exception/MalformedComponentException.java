package net.cadier.jcf.exception;

import net.cadier.jcf.lang.component.Component;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MalformedComponentException extends Exception {

    private final Component partialComponent;
    private final Class<?> componentClass;
    private final List<String> malformedCauses = new ArrayList<>();

    public MalformedComponentException(Class<?> aComponentClass, Component aPartialComponent, List<String> theMalformedCauses) {
        super("Malformed component '" + aComponentClass + "'");

        this.componentClass = aComponentClass;
        this.partialComponent = aPartialComponent;
        this.malformedCauses.addAll(theMalformedCauses);
    }

    public void printDiagnostic(final PrintStream printStream) {
        synchronized (printStream) {
            String allCauses = this.malformedCauses.stream()
                    .map(cause -> "\t-> " + cause)
                    .collect(Collectors.joining("\n"));
            printStream.println("Checker diagnostic failed for '" + this.componentClass + "':\n" + allCauses);
        }
    }
}
