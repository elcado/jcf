/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.components.messagessink.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.EventSink;
import net.cadier.jcf.annotations.Id;
import net.cadier.jcf.annotations.Parameter;
import net.cadier.jcf.components.messagessink.MessageSinkParameters;
import net.cadier.jcf.components.messagessink.MessagesSink;
import net.cadier.jcf.components.messagessource.Message;
import net.cadier.jcf.lang.component.Startable;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.parameters.Parameters;
import net.cadier.jcf.lang.instance.events.Sink;
import net.cadier.jcf.utils.searchableconsole.SearchableConsole;

@ComponentImplementation
public class MessagesSinkImpl implements MessagesSink, Startable {
    private String name = "Sink #";

    public MessagesSinkImpl() { }

    @Id
    private final transient InstanceId thisId = null;

    @Parameter
    private final transient Parameters<MessageSinkParameters> messageSinkParameters = null;

    @EventSink
    private final transient Sink<Message> messagesSink = event -> {
        try {
            Thread.sleep((long) (Math.random() * 250));
        } catch (InterruptedException e) {
            SearchableConsole.addMessage("[" + this.name + "] interrupted");
        }

        SearchableConsole.addMessage("[" + this.name + "] receiving '" + event + "' from [" + event.getSenderName() + "]");
    };

    @Override
    public boolean onStarted() {
        name += this.messageSinkParameters.get(this.thisId, MessageSinkParameters.NUMBER);
        return true;
    }

    @Override
    public boolean onPaused() {
        return true;
    }

    @Override
    public boolean onResumed() {
        return true;
    }

    @Override
    public boolean onStopped() {
        return true;
    }

    @Override
    public boolean onFailed() {
        return true;
    }
}
