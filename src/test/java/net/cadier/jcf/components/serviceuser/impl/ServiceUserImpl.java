/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.components.serviceuser.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.components.HelloWorldService;
import net.cadier.jcf.components.serviceuser.ServiceUser;
import net.cadier.jcf.lang.component.Initializable;
import net.cadier.jcf.lang.component.Startable;
import net.cadier.jcf.utils.searchableconsole.SearchableConsole;

import java.util.Objects;
import java.util.concurrent.*;

/**
 * @author fred
 */
@ComponentImplementation
public class ServiceUserImpl implements ServiceUser, Initializable, Startable {

    @UsedService
    protected final transient HelloWorldService helloWorldService = null;

    private ScheduledExecutorService executor;
    private Runnable runnable;
    private ScheduledFuture<?> future;

    @Override
    public boolean onInitialized() {
        this.executor = Executors.newSingleThreadScheduledExecutor();
        this.runnable = () -> {
            if (this.helloWorldService == null) {
                SearchableConsole.addMessage("No provider");
            } else {
                SearchableConsole.addMessage(this.helloWorldService.getHelloWorld() + " from " + this.getClass().getSimpleName());
            }
        };

        return true;
    }

    @Override
    public boolean onStarted() {
        this.future = this.executor.scheduleAtFixedRate(this.runnable, 100, 500, TimeUnit.MILLISECONDS);
        return true;
    }

    @Override
    public boolean onPaused() {
        this.future.cancel(true);
        return true;
    }

    @Override
    public boolean onResumed() {
        this.future = this.executor.scheduleAtFixedRate(this.runnable, 100, 500, TimeUnit.MILLISECONDS);
        return true;
    }

    @Override
    public boolean onStopped() {
        this.executor.shutdown();
        this.future.cancel(true);
        return true;
    }

    @Override
    public boolean onFailed() {
        if (Objects.nonNull(this.executor)) {
            this.executor.shutdownNow();
        }
        return true;
    }

    @Override
    public boolean onDisposed() {
        this.future = null;
        this.runnable = null;
        this.executor = null;

        return true;
    }

}
