/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.components.debugserviceuser.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.components.debugserviceuser.DebugServiceUser;
import net.cadier.jcf.core.container.ContainerDebugServices;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.lang.component.Initializable;
import net.cadier.jcf.lang.component.Startable;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.utils.searchableconsole.SearchableConsole;
import net.cadier.utils.tree.Tree;

/**
 * @author fred
 */
@ComponentImplementation
public class DebugServiceUserImpl implements DebugServiceUser, Initializable, Startable {

    @UsedService
    protected final transient LoggingServices loggingServices = null;

    @UsedService
    protected final transient ContainerDebugServices containerDebugServices = null;

    @Override
    public boolean onInitialized() {
        return true;
    }

    @Override
    public boolean onStarted() {
        Tree<InstanceId, Instance> instancesTree = this.containerDebugServices.getLocalInstancesTree();
        instancesTree.getRoots().forEach(rootId -> SearchableConsole.addMessage(rootId.toString()));

        InstanceId localContainerId = this.containerDebugServices.getLocalContainerId();
        boolean foundLocalContainerInTree = instancesTree.getRoots().stream().anyMatch(localContainerId::equals);

        return foundLocalContainerInTree;
    }

    @Override
    public boolean onPaused() {
        return true;
    }

    @Override
    public boolean onResumed() {
        return true;
    }

    @Override
    public boolean onStopped() {
        return true;
    }

    @Override
    public boolean onFailed() {
        return true;
    }

    @Override
    public boolean onDisposed()  {
        return true;
    }

}
