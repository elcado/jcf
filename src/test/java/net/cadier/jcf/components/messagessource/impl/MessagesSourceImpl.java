/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.components.messagessource.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.EventSource;
import net.cadier.jcf.annotations.Id;
import net.cadier.jcf.annotations.Parameter;
import net.cadier.jcf.components.messagessource.Message;
import net.cadier.jcf.components.messagessource.MessageSourceParameters;
import net.cadier.jcf.components.messagessource.MessagesSource;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.parameters.Parameters;
import net.cadier.jcf.lang.component.Startable;
import net.cadier.jcf.lang.instance.events.Source;
import net.cadier.jcf.utils.searchableconsole.SearchableConsole;

@ComponentImplementation
public class MessagesSourceImpl implements MessagesSource, Startable {
    private String name = "Source #";

    public MessagesSourceImpl() { }

    @Id
    private final transient InstanceId thisId = null;

    @Parameter
    private final transient Parameters<MessageSourceParameters> messageSourceParameters = null;

    @EventSource
    public final transient Source<Message> messagesSource = null;

    // TODO replace with executor
    Thread thread = new Thread(() -> {
        try {
            int counter = 0;
            while (true) {
                Thread.sleep((long) (Math.random() * 500));
                Thread.yield();

                Message message = new Message(this.name, "event #" + counter++);
                SearchableConsole.addMessage("[" + this.name + "] posting '" + message + "'");
                this.messagesSource.postEvent(message);
            }
        } catch (InterruptedException e) {
            SearchableConsole.addMessage("[" + this.name + "] interrupted");
        }
    });

    @Override
    public boolean onStarted() {
        name += this.messageSourceParameters.get(this.thisId, MessageSourceParameters.NUMBER);
        this.thread.start();
        return true;
    }

    @Override
    public boolean onPaused() {
        return true;
    }

    @Override
    public boolean onResumed() {
        return true;
    }

    @Override
    public boolean onStopped() {
        this.thread.interrupt();
        return true;
    }

    @Override
    public boolean onFailed() {
        this.thread.interrupt();
        return true;
    }

}
