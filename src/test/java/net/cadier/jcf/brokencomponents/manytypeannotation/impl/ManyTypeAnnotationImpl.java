package net.cadier.jcf.brokencomponents.manytypeannotation.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.brokencomponents.manytypeannotation.ATypeAnnotation;
import net.cadier.jcf.brokencomponents.manytypeannotation.ManyTypeAnnotation;

@ComponentImplementation
public class ManyTypeAnnotationImpl implements ManyTypeAnnotation, ATypeAnnotation {
}
