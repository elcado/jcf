package net.cadier.jcf.brokencomponents.brokenfield.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.Id;
import net.cadier.jcf.brokencomponents.brokenfield.BrokenField;
import net.cadier.jcf.lang.instance.InstanceId;

@ComponentImplementation
public class IdBrokenFieldImpl implements BrokenField {
    @Id
    private final transient String thisId = null;

    @Id
    final transient InstanceId packageId = null;

    @Id
    public final transient InstanceId publicId = null;

    @Id
    private transient InstanceId notFinalId = null;

    @Id
    private final InstanceId notTransientId = null;
}
