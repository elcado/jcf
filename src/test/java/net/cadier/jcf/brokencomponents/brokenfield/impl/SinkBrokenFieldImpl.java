package net.cadier.jcf.brokencomponents.brokenfield.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.EventSink;
import net.cadier.jcf.annotations.EventSource;
import net.cadier.jcf.brokencomponents.brokenfield.BrokenField;
import net.cadier.jcf.components.messagessource.Message;
import net.cadier.jcf.lang.instance.events.Sink;
import net.cadier.jcf.lang.instance.events.Source;

@ComponentImplementation
public class SinkBrokenFieldImpl implements BrokenField {
    @EventSink
    private final transient String sinkTypeError = "paf";

    @EventSink
    private final transient Sink<BrokenField> sinkDataTypeClassOrEnumError = null;

    @EventSink
    private final transient Sink<String> sinkDataTypeAnnotationError = null;

    @EventSink
    final transient Sink<Message> packageMessageSink = null;

    @EventSink
    private transient Sink<Message> notFinalMessageSink = null;

    @EventSink
    private final Sink<Message> notTransientMessageSink = null;

}
