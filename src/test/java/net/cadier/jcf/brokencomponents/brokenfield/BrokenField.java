package net.cadier.jcf.brokencomponents.brokenfield;

import net.cadier.jcf.annotations.ComponentType;
import net.cadier.jcf.components.HelloWorldService;
import net.cadier.jcf.components.messagessource.Message;

@ComponentType(provides = {HelloWorldService.class}, produces = {Message.class})
public interface BrokenField {
}
