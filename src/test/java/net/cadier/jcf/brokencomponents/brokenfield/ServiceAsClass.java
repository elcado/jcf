package net.cadier.jcf.brokencomponents.brokenfield;

import net.cadier.jcf.annotations.ComponentService;

@ComponentService
public class ServiceAsClass {
    void dummy() {

    }
}
