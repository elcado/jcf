package net.cadier.jcf.brokencomponents.brokenfield.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.brokencomponents.brokenfield.BrokenField;
import net.cadier.jcf.brokencomponents.brokenfield.ServiceAsClass;
import net.cadier.jcf.brokencomponents.brokenfield.ServiceMissingAnnotation;
import net.cadier.jcf.components.HelloWorldService;

import java.util.HashMap;
import java.util.Map;

@ComponentImplementation
public class UsedServiceBrokenFieldImpl implements BrokenField {
    @UsedService
    private final transient Map<String, HelloWorldService> mapKeyAsString = null;

    @UsedService
    private final transient ServiceAsClass serviceAsClass = null;

    @UsedService
    private final transient ServiceMissingAnnotation serviceMissingAnnotation = null;

    @UsedService
    final transient HelloWorldService packageHelloWorldService = null;

    @UsedService
    public final transient HelloWorldService publicHelloWorldService = null;

    @UsedService
    private transient HelloWorldService notFinalHelloWorldService = null;

    @UsedService
    private final HelloWorldService notTransientHelloWorldService = null;
}
