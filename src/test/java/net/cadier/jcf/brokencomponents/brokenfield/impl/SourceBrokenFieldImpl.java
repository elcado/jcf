package net.cadier.jcf.brokencomponents.brokenfield.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.EventSource;
import net.cadier.jcf.brokencomponents.brokenfield.BrokenField;
import net.cadier.jcf.components.messagessource.Message;
import net.cadier.jcf.lang.instance.events.Source;

@ComponentImplementation
public class SourceBrokenFieldImpl implements BrokenField {
    @EventSource
    private final transient String sourceTypeError = "paf";

    @EventSource
    private final transient Source<BrokenField> sourceDataTypeClassOrEnumError = null;

    @EventSource
    private final transient Source<String> sourceDataTypeAnnotationError = null;

    @EventSource
    final transient Source<Message> packageMessageSource = null;

    @EventSource
    private transient Source<Message> notFinalMessageSource = null;

    @EventSource
    private final Source<Message> notTransientMessageSource = null;

}
