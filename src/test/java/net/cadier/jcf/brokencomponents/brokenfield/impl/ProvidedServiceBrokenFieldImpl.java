package net.cadier.jcf.brokencomponents.brokenfield.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.EventSource;
import net.cadier.jcf.annotations.ProvidedService;
import net.cadier.jcf.brokencomponents.brokenfield.BrokenField;
import net.cadier.jcf.brokencomponents.brokenfield.ServiceAsClass;
import net.cadier.jcf.brokencomponents.brokenfield.ServiceMissingAnnotation;
import net.cadier.jcf.components.HelloWorldService;
import net.cadier.jcf.components.messagessource.Message;
import net.cadier.jcf.lang.instance.events.Source;

@ComponentImplementation
public class ProvidedServiceBrokenFieldImpl implements BrokenField {
    @ProvidedService
    private final transient ServiceAsClass serviceAsClass = new ServiceAsClass();

    @ProvidedService
    private final transient ServiceMissingAnnotation serviceMissingAnnotation = () -> { };

    @ProvidedService
    final transient HelloWorldService packageHelloWorldService = () -> "Hello, world !";

    @ProvidedService
    private transient HelloWorldService notFinalHelloWorldService = () -> "Hello, world !";

    @ProvidedService
    private final HelloWorldService notTransientHelloWorldService = () -> "Hello, world !";
}
