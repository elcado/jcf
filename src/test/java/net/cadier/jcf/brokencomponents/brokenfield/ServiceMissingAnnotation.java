package net.cadier.jcf.brokencomponents.brokenfield;

public interface ServiceMissingAnnotation {
    void dummy();
}
