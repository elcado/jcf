package net.cadier.jcf.brokencomponents.brokenfield.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.Parameter;
import net.cadier.jcf.brokencomponents.brokenfield.BrokenField;
import net.cadier.jcf.components.serviceproviderwithparameter.ServiceProviderParameter;
import net.cadier.jcf.lang.instance.parameters.Parameters;

import javax.lang.model.element.ElementKind;

@ComponentImplementation
public class ParametersBrokenFieldImpl implements BrokenField {
    @Parameter
    private final transient String parametersTypeError = "paf";

    @Parameter
    private final transient Parameters<ElementKind> parametersDataTypeAnnotationError = null;

    @Parameter
    final transient Parameters<ServiceProviderParameter> packageMessageParameters = null;

    @Parameter
    public final transient Parameters<ServiceProviderParameter> publicMessageParameters = null;

    @Parameter
    private transient Parameters<ServiceProviderParameter> notFinalMessageParameters = null;

    @Parameter
    private final Parameters<ServiceProviderParameter> notTransientMessageParameters = null;

}
