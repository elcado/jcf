package net.cadier.jcf.brokencomponents.missingtypeannotation.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.brokencomponents.missingtypeannotation.MissingTypeAnnotation;

@ComponentImplementation
public class MissingTypeAnnotationImpl implements MissingTypeAnnotation {
}
