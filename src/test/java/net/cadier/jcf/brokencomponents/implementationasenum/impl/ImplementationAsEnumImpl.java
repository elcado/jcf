package net.cadier.jcf.brokencomponents.implementationasenum.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.brokencomponents.implementationasenum.ImplementationAsEnum;

@ComponentImplementation
public enum ImplementationAsEnumImpl implements ImplementationAsEnum {
}
