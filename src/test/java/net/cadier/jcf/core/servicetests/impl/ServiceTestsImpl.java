/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.core.servicetests.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.core.servicetests.ServiceTests;
import net.cadier.jcf.exception.MalformedComponentException;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.utils.abstracttest.AbstractJCFTest;
import net.cadier.jcf.components.inheritedserviceprovider.impl.InheritedServiceProviderImpl;
import net.cadier.jcf.components.inheritedserviceuser.impl.InheritedServiceUserImpl;
import net.cadier.jcf.components.serviceprovider.impl.ServiceProviderImpl;
import net.cadier.jcf.components.serviceuser.impl.ServiceUserImpl;
import net.cadier.jcf.utils.searchableconsole.SearchableConsole;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.logging.Level;

@ComponentImplementation
public class ServiceTestsImpl extends AbstractJCFTest implements ServiceTests {
    public static final int PAUSE_TIME_IN_MILLIS = 1750;

    @Override
    protected Level getTestLoggingLevel() {
        return Level.OFF;
    }

    /**
     * Deploys	1/ service provider
     * 2/ service user
     */
    @Test
    @Order(1)
    public void deployUserAfterProviderTest() {
        // register components
        try {
            this.containerServices.registerComponent(ServiceProviderImpl.class);
            this.containerServices.registerComponent(ServiceUserImpl.class);
        } catch (MalformedComponentException mce) {
            mce.printDiagnostic(System.err);
            Assertions.fail(mce);
        }

        // deploy provider
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);

        // deploy user
        InstanceId userId = this.deployInstance(this.thisId, ServiceUserImpl.class, false);

        // start user
        this.startInstance(userId, ServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertExactMessageNbOfOccurrences("Hello, world! from ServiceUserImpl", 4, true);

        // stop user
        this.stopInstance(userId, ServiceUserImpl.class);

        // unregister components
        this.containerServices.unregisterComponent(ServiceUserImpl.class);
        this.containerServices.unregisterComponent(ServiceProviderImpl.class);
    }

    /**
     * Deploys	1/ service provider
     * 2/ service user
     * Then 3/ pauses user
     * 4/ resumes user
     */
    @Test
    @Order(2)
    public void deployUserAfterProviderThenPauseAndResumeTest() {
        // register components
        try {
            this.containerServices.registerComponent(ServiceProviderImpl.class);
            this.containerServices.registerComponent(ServiceUserImpl.class);
        } catch (MalformedComponentException mce) {
            mce.printDiagnostic(System.err);
            Assertions.fail(mce);
        }

        // deploy provider
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);

        // deploy user
        InstanceId userId = this.deployInstance(this.thisId, ServiceUserImpl.class, false);

        // start user
        this.startInstance(userId, ServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertExactMessageNbOfOccurrences("Hello, world! from ServiceUserImpl", 4, true);

        // pause user
        this.pauseInstance(userId, ServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertNoMessage(true);

        // resume user
        this.resumeInstance(userId, ServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertExactMessageNbOfOccurrences("Hello, world! from ServiceUserImpl", 4, true);

        // stop user
        this.stopInstance(userId, ServiceUserImpl.class);

        // unregister components
        this.containerServices.unregisterComponent(ServiceUserImpl.class);
        this.containerServices.unregisterComponent(ServiceProviderImpl.class);
    }

    /**
     * Deploys	1/ service user
     * 2/ service provider
     */
    @Test
    @Order(3)
    public void deployUserBeforeProviderTest() {
        // register components
        try {
            this.containerServices.registerComponent(ServiceProviderImpl.class);
            this.containerServices.registerComponent(ServiceUserImpl.class);
        } catch (MalformedComponentException mce) {
            mce.printDiagnostic(System.err);
            Assertions.fail(mce);
        }

        // deploy user
        InstanceId userId = this.deployInstance(this.thisId, ServiceUserImpl.class, false);

        // start user
        this.startInstance(userId, ServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertExactMessageNbOfOccurrences("No provider", 4, true);

        // pause user
        this.pauseInstance(userId, ServiceUserImpl.class);

        // deploy and start provider
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);

        // resume user
        this.resumeInstance(userId, ServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertExactMessageNbOfOccurrences("Hello, world! from ServiceUserImpl", 4, true);

        // stop user
        this.stopInstance(userId, ServiceUserImpl.class);

        // unregister components
        this.containerServices.unregisterComponent(ServiceUserImpl.class);
        this.containerServices.unregisterComponent(ServiceProviderImpl.class);
    }

    /**
     * Deploy provider and user, undeploy user, deploy user, undeploy provider, deploy provider
     */
    @Test
    @Order(4)
    public void deployUndeployTest() {
        // register components
        try {
            this.containerServices.registerComponent(ServiceProviderImpl.class);
            this.containerServices.registerComponent(ServiceUserImpl.class);
        } catch (MalformedComponentException mce) {
            mce.printDiagnostic(System.err);
            Assertions.fail(mce);
        }

        // deploy provider
        InstanceId providerId = this.deployInstance(this.thisId, ServiceProviderImpl.class, true);

        // deploy user
        InstanceId userId = this.deployInstance(this.thisId, ServiceUserImpl.class, false);

        // start user
        this.startInstance(userId, ServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertExactMessageNbOfOccurrences("Hello, world! from ServiceUserImpl", 4, true);

        // undeploy and re-deploy user
        this.stopInstance(userId, ServiceUserImpl.class);
        this.undeployInstance(userId, ServiceUserImpl.class);
        userId = this.deployInstance(this.thisId, ServiceUserImpl.class, false);

        // start user
        this.startInstance(userId, ServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertExactMessageNbOfOccurrences("Hello, world! from ServiceUserImpl", 4, true);

        // undeploy provider
        this.stopInstance(providerId, ServiceProviderImpl.class);
        this.undeployInstance(providerId, ServiceProviderImpl.class);

        // let user work
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertMessage("No provider", true);

        // re-deploy provider
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);

        // let user work
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertMessage("Hello, world! from ServiceUserImpl", true);

        // unregister components
        this.containerServices.unregisterComponent(ServiceUserImpl.class);
        this.containerServices.unregisterComponent(ServiceProviderImpl.class);
    }

    /**
     * Deploy service with class hierarchy provider
     */
    @Test
    @Order(5)
    public void deployInheritedProviderTest() {
        // register components
        try {
            this.containerServices.registerComponent(InheritedServiceProviderImpl.class);
            this.containerServices.registerComponent(ServiceUserImpl.class);
        } catch (MalformedComponentException mce) {
            mce.printDiagnostic(System.err);
            Assertions.fail(mce);
        }

        // deploy provider
        this.deployInstance(this.thisId, InheritedServiceProviderImpl.class, true);

        // deploy user
        InstanceId userId = this.deployInstance(this.thisId, ServiceUserImpl.class, false);

        // start user
        this.startInstance(userId, ServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertExactMessageNbOfOccurrences("Hello, world! from ServiceUserImpl", 4, true);

        // stop user
        this.stopInstance(userId, ServiceUserImpl.class);

        // unregister components
        this.containerServices.unregisterComponent(ServiceUserImpl.class);
        this.containerServices.unregisterComponent(InheritedServiceProviderImpl.class);
    }

    /**
     * Deploy service with class hierarchy user
     */
    @Test
    @Order(6)
    public void deployInheritedUserTest() {
        // register components
        try {
            this.containerServices.registerComponent(ServiceProviderImpl.class);
            this.containerServices.registerComponent(InheritedServiceUserImpl.class);
        } catch (MalformedComponentException mce) {
            mce.printDiagnostic(System.err);
            Assertions.fail(mce);
        }

        // deploy provider
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);

        // deploy and start user
        InstanceId userId = this.deployInstance(this.thisId, InheritedServiceUserImpl.class, false);

        // start user
        this.startInstance(userId, InheritedServiceUserImpl.class);
        this.pause(PAUSE_TIME_IN_MILLIS);
        SearchableConsole.assertExactMessageNbOfOccurrences("Hello, world! from InheritedServiceUserImpl", 4, true);

        // stop user
        this.stopInstance(userId, InheritedServiceUserImpl.class);

        // unregister components
        this.containerServices.unregisterComponent(InheritedServiceUserImpl.class);
        this.containerServices.unregisterComponent(ServiceProviderImpl.class);
    }
}
