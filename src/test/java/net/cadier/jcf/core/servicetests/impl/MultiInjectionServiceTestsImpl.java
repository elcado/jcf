/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.core.servicetests.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.core.servicetests.ServiceTests;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.utils.abstracttest.AbstractJCFTest;
import net.cadier.jcf.components.servicemultiuser.impl.ServiceMultiUserImpl;
import net.cadier.jcf.components.serviceprovider.impl.AlternativeServiceProviderImpl;
import net.cadier.jcf.components.serviceprovider.impl.ServiceProviderImpl;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

@ComponentImplementation
public class MultiInjectionServiceTestsImpl extends AbstractJCFTest implements ServiceTests {
    public static final int PAUSE_TIME_IN_MILLIS = 5000;

    @Override
    protected Level getTestLoggingLevel() {
        return Level.INFO;
    }

    @Override
    protected List<Class<?>> getTestComponentsToRegister() {
        return Arrays.asList(ServiceProviderImpl.class, AlternativeServiceProviderImpl.class,
                ServiceMultiUserImpl.class);
    }

    /**
     * Deploys	1/ service providers
     * 2/ service user
     */
    @Test
    @Order(1)
    public void deployMultiUserTest() {
        // deploy providers
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);
        this.deployInstance(this.thisId, AlternativeServiceProviderImpl.class, true);

        // deploy user
        InstanceId userId = this.deployInstance(this.thisId, ServiceMultiUserImpl.class, true);

        // start user
        this.startInstance(userId, ServiceMultiUserImpl.class);

        this.pause(PAUSE_TIME_IN_MILLIS);

        // stop user
        this.stopInstance(userId, ServiceMultiUserImpl.class);
    }

    /**
     * Deploys	1/ service one provider
     * 2/ service user
     * 3/ service another provider
     */
    @Test
    @Order(2)
    public void deployMultiUserTest2() {
        // deploy one provider
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);

        // deploy user
        InstanceId userId = this.deployInstance(this.thisId, ServiceMultiUserImpl.class, true);

        // start user
        this.startInstance(userId, ServiceMultiUserImpl.class);

        this.pause(3000);

        // deploy another provider
        this.deployInstance(this.thisId, AlternativeServiceProviderImpl.class, true);

        this.pause(3000);

        // stop user
        this.stopInstance(userId, ServiceMultiUserImpl.class);
    }

    /**
     * Deploys	1/ service providers
     * 2/ service user
     * Undeploys one provider
     */
    @Test
    @Order(3)
    public void deployMultiUserTest3() {
        // deploy providers
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);
        InstanceId alternativeProviderId = this.deployInstance(this.thisId,
                AlternativeServiceProviderImpl.class, true);

        // deploy user
        InstanceId userId = this.deployInstance(this.thisId, ServiceMultiUserImpl.class, true);

        // start user
        this.startInstance(userId, ServiceMultiUserImpl.class);

        this.pause(2000);

        // undeploys alternative provider
        this.undeployInstance(alternativeProviderId, AlternativeServiceProviderImpl.class);

        this.pause(2000);

        // stop user
        this.stopInstance(userId, ServiceMultiUserImpl.class);
    }

    /**
     * Deploys	1/ service user
     * 2/ service providers
     */
    @Test
    @Order(4)
    public void deployUserBeforeProviders() {
        // deploy user
        InstanceId userId = this.deployInstance(this.thisId, ServiceMultiUserImpl.class, true);

        // deploy providers
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);
        InstanceId alternativeProviderId = this.deployInstance(this.thisId,
                AlternativeServiceProviderImpl.class, true);

        // start user
        this.startInstance(userId, ServiceMultiUserImpl.class);

        this.pause(2000);

        // undeploys alternative provider
        this.undeployInstance(alternativeProviderId, AlternativeServiceProviderImpl.class);

        this.pause(2000);

        // stop user
        this.stopInstance(userId, ServiceMultiUserImpl.class);
    }
}
