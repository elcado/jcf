/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.core.eventstests.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.Parameter;
import net.cadier.jcf.core.eventstests.EventsTests;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.parameters.Parameters;
import net.cadier.jcf.utils.abstracttest.AbstractJCFTest;
import net.cadier.jcf.components.messagessink.MessageSinkParameters;
import net.cadier.jcf.components.messagessink.impl.MessagesSinkImpl;
import net.cadier.jcf.components.messagessource.MessageSourceParameters;
import net.cadier.jcf.components.messagessource.impl.MessagesSourceImpl;
import net.cadier.jcf.utils.searchableconsole.SearchableConsole;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

@ComponentImplementation
public class EventsTestsImpl extends AbstractJCFTest implements EventsTests {
    public static final int PAUSE_TIME_IN_MILLIS = 2000;

    @Parameter
    private final transient Parameters<MessageSourceParameters> messageSourceParameters = null;

    @Parameter
    private final transient Parameters<MessageSinkParameters> messageSinkParameters = null;

    @Override
    protected Level getTestLoggingLevel() {
        return Level.INFO;
    }

    @Override
    protected List<Class<?>> getTestComponentsToRegister() {
        return Arrays.asList(MessagesSourceImpl.class, MessagesSinkImpl.class);
    }

    /**
     * Deploys  1/ one messages' source and starts it
     *          2/ one messages' sink and starts it
     */
    @Test
    @Order(1)
    public void deploySinkAfterSource() {
        // deploys and starts source
        InstanceId messagesSourceId_0 = this.deployInstance(this.thisId, MessagesSourceImpl.class, false);
        this.messageSourceParameters.set(messagesSourceId_0, MessageSourceParameters.NUMBER, 0);
        this.startInstance(messagesSourceId_0, MessagesSourceImpl.class);

        // deploys and starts sink
        InstanceId messagesSinkId_0 = this.deployInstance(this.thisId, MessagesSinkImpl.class, false);
        this.messageSinkParameters.set(messagesSinkId_0, MessageSinkParameters.NUMBER, 0);
        this.startInstance(messagesSinkId_0, MessagesSinkImpl.class);

        // lets work
        this.pause(PAUSE_TIME_IN_MILLIS);

        // stops source and sink
        this.stopInstance(messagesSourceId_0, MessagesSourceImpl.class);
        this.stopInstance(messagesSinkId_0, MessagesSinkImpl.class);

        SearchableConsole.assertMessage("[Source #0] posting 'event #0'", false);
        SearchableConsole.assertMessage("[Source #0] posting 'event #1'", false);

        SearchableConsole.assertMessage("[Sink #0] receiving 'event #0' from [Source #0]", false);
        SearchableConsole.assertMessage("[Sink #0] receiving 'event #1' from [Source #0]", true);
    }

    /**
     * Deploys  1/ one messages' sink and starts it
     *          2/ one messages' source
     * 3/ Starts messages' sources
     */
    @Test
    @Order(1)
    public void deploySinkBeforeSource() {
        // deploy sink
        InstanceId messagesSinkId_0 = this.deployInstance(this.thisId, MessagesSinkImpl.class, false);
        this.messageSinkParameters.set(messagesSinkId_0, MessageSinkParameters.NUMBER, 0);
        this.startInstance(messagesSinkId_0, MessagesSinkImpl.class);

        // deploy source
        InstanceId messagesSourceId_0 = this.deployInstance(this.thisId, MessagesSourceImpl.class, false);
        this.messageSourceParameters.set(messagesSourceId_0, MessageSourceParameters.NUMBER, 0);
        this.startInstance(messagesSourceId_0, MessagesSourceImpl.class);

        // lets work
        this.pause(PAUSE_TIME_IN_MILLIS);

        // stops source and sink
        this.stopInstance(messagesSourceId_0, MessagesSourceImpl.class);
        this.stopInstance(messagesSinkId_0, MessagesSinkImpl.class);

        SearchableConsole.assertMessage("[Source #0] posting 'event #0'", false);
        SearchableConsole.assertMessage("[Source #0] posting 'event #1'", false);

        SearchableConsole.assertMessage("[Sink #0] receiving 'event #0' from [Source #0]", false);
        SearchableConsole.assertMessage("[Sink #0] receiving 'event #1' from [Source #0]", true);
    }

    /**
     * Deploys	1/ many messages' sources
     *          2/ many messages' sinks and starts them
     * 3/ Starts messages' sources
     */
    @Test
    @Order(3)
    public void testManyEventsInjection() {
        // deploy source
        InstanceId messagesSourceId_0 = this.deployInstance(this.thisId, MessagesSourceImpl.class, false);
        InstanceId messagesSourceId_1 = this.deployInstance(this.thisId, MessagesSourceImpl.class, false);
        InstanceId messagesSourceId_3 = this.deployInstance(this.thisId, MessagesSourceImpl.class, false);
        this.messageSourceParameters.set(messagesSourceId_0, MessageSourceParameters.NUMBER, 0);
        this.messageSourceParameters.set(messagesSourceId_1, MessageSourceParameters.NUMBER, 1);
        this.messageSourceParameters.set(messagesSourceId_3, MessageSourceParameters.NUMBER, 2);

        // deploy sink
        InstanceId messagesSinkId_0 = this.deployInstance(this.thisId, MessagesSinkImpl.class, false);
        this.messageSinkParameters.set(messagesSinkId_0, MessageSinkParameters.NUMBER, 0);
        this.startInstance(messagesSinkId_0, MessagesSinkImpl.class);
        InstanceId messagesSinkId_1 = this.deployInstance(this.thisId, MessagesSinkImpl.class, false);
        this.messageSinkParameters.set(messagesSinkId_1, MessageSinkParameters.NUMBER, 1);
        this.startInstance(messagesSinkId_1, MessagesSinkImpl.class);

        // starts sources
        this.startInstance(messagesSourceId_0, MessagesSourceImpl.class);
        this.startInstance(messagesSourceId_1, MessagesSourceImpl.class);
        this.startInstance(messagesSourceId_3, MessagesSourceImpl.class);

        // lets work
        this.pause(PAUSE_TIME_IN_MILLIS);

        // stops sources and sinks
        this.stopInstance(messagesSourceId_0, MessagesSourceImpl.class);
        this.stopInstance(messagesSourceId_1, MessagesSourceImpl.class);
        this.stopInstance(messagesSourceId_3, MessagesSourceImpl.class);
        this.stopInstance(messagesSinkId_0, MessagesSourceImpl.class);
        this.stopInstance(messagesSinkId_1, MessagesSourceImpl.class);

        SearchableConsole.assertMessage("[Source #0] posting 'event #0'", false);
        SearchableConsole.assertMessage("[Source #0] posting 'event #1'", false);
        SearchableConsole.assertMessage("[Source #1] posting 'event #0'", false);
        SearchableConsole.assertMessage("[Source #1] posting 'event #1'", false);
        SearchableConsole.assertMessage("[Source #2] posting 'event #0'", false);
        SearchableConsole.assertMessage("[Source #2] posting 'event #1'", false);

        SearchableConsole.assertMessage("[Sink #0] receiving 'event #0' from [Source #0]", false);
        SearchableConsole.assertMessage("[Sink #0] receiving 'event #1' from [Source #1]", false);
        SearchableConsole.assertMessage("[Sink #1] receiving 'event #0' from [Source #0]", false);
        SearchableConsole.assertMessage("[Sink #1] receiving 'event #1' from [Source #1]", true);
    }

    /**
     * Deploys	1/ many messages' sources
     *          2/ many messages' sinks
     * 3/ Starts messages' sources
     * 4/ Undeploys some sources and sinks
     */
    @Test
    @Order(4)
    public void testManyEventsInjectionAndUndeploySome() {
        // deploy source
        InstanceId messagesSourceId_0 = this.deployInstance(this.thisId, MessagesSourceImpl.class, false);
        InstanceId messagesSourceId_1 = this.deployInstance(this.thisId, MessagesSourceImpl.class, false);
        InstanceId messagesSourceId_2 = this.deployInstance(this.thisId, MessagesSourceImpl.class, false);

        // deploy sink
        this.deployInstance(this.thisId, MessagesSinkImpl.class, true);
        InstanceId messageSinkId_1 = this.deployInstance(this.thisId, MessagesSinkImpl.class, true);

        // start sources
        this.startInstance(messagesSourceId_0, MessagesSourceImpl.class);
        this.startInstance(messagesSourceId_1, MessagesSourceImpl.class);
        this.startInstance(messagesSourceId_2, MessagesSourceImpl.class);

        this.pause(PAUSE_TIME_IN_MILLIS);

        // undeploy some sources and sinks
        this.undeployInstance(messageSinkId_1, MessagesSinkImpl.class);

        this.stopInstance(messagesSourceId_2, MessagesSourceImpl.class);
        this.undeployInstance(messagesSourceId_2, MessagesSourceImpl.class);

        this.pause(PAUSE_TIME_IN_MILLIS);

        // stop source
        this.stopInstance(messagesSourceId_0, MessagesSourceImpl.class);
        this.stopInstance(messagesSourceId_1, MessagesSourceImpl.class);
    }
}
