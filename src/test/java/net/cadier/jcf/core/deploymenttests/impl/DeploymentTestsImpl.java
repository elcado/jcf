/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.core.deploymenttests.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.brokencomponents.brokenfield.impl.*;
import net.cadier.jcf.brokencomponents.implementationasenum.impl.ImplementationAsEnumImpl;
import net.cadier.jcf.brokencomponents.manytypeannotation.impl.ManyTypeAnnotationImpl;
import net.cadier.jcf.brokencomponents.missingimplannotation.impl.MissingImplAnnotationImpl;
import net.cadier.jcf.brokencomponents.missingprovidedserviceandproducedevent.impl.MissingProvidedServiceAndProducedEventImpl;
import net.cadier.jcf.brokencomponents.missingtypeannotation.impl.MissingTypeAnnotationImpl;
import net.cadier.jcf.brokencomponents.namingproblem.NamingggProblem;
import net.cadier.jcf.brokencomponents.namingproblem.imple.NamingggProblemImplementation;
import net.cadier.jcf.components.HelloWorldService;
import net.cadier.jcf.components.messagessource.Message;
import net.cadier.jcf.components.serviceprovider.impl.ServiceProviderImpl;
import net.cadier.jcf.components.serviceuser.impl.ServiceUserImpl;
import net.cadier.jcf.core.deploymenttests.DeploymentTests;
import net.cadier.jcf.core.logging.impl.JDKLoggingImpl;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.exception.DeploymentRuleException;
import net.cadier.jcf.exception.MalformedComponentException;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.utils.abstracttest.AbstractJCFTest;
import net.cadier.jcf.utils.searchableconsole.SearchableConsole;
import net.cadier.utils.reflection.ReflectionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.logging.Level;

@ComponentImplementation
public class DeploymentTestsImpl extends AbstractJCFTest implements DeploymentTests {
    @Override
    protected Level getTestLoggingLevel() {
        return Level.INFO;
    }

    @Test
    @Order(1)
    public void abstractTestDeployment() {
        assert this.thisId != null;
        assert this.loggingServices != null;
        assert this.loggingParameters != null;
    }

    @Test
    @Order(2)
    public void registerAlreadyRegisteredImplementation() {
        // register provider
        registerComponent(ServiceProviderImpl.class);

        // re register provider
        registerComponent(ServiceProviderImpl.class);
    }

    @Test
    @Order(3)
    public void deployAlreadyDeployedImplementation() {
        // register provider
        registerComponent(ServiceProviderImpl.class);

        // deploy provider
        assert this.deployInstance(this.thisId, ServiceProviderImpl.class, true) != null;

        // re deploy provider
        assert this.deployInstance(this.thisId, ServiceProviderImpl.class, true) != null;
    }

    @Test
    @Order(4)
    public void registerBrokenComponents() {
        // Global
        this.failToRegisterComponent(MissingTypeAnnotationImpl.class);
        SearchableConsole.assertContainsMessage("Cannot find an implemented interface annotated with @ComponentType for '" + MissingTypeAnnotationImpl.class.getCanonicalName() + "'.", true);

        this.failToRegisterComponent(MissingProvidedServiceAndProducedEventImpl.class);
        SearchableConsole.assertContainsMessage("Following provided services declared in component's type have not been found in implementation: '" + HelloWorldService.class.getCanonicalName() + "'.");
        SearchableConsole.assertContainsMessage("Following produced events declared in component's type have not been found in implementation: '" + Message.class.getCanonicalName() + "'.", true);

        // ComponentType
        this.failToRegisterComponent(MissingImplAnnotationImpl.class);
        SearchableConsole.assertContainsMessage("Implementation class '" + MissingImplAnnotationImpl.class.getCanonicalName() + "' is missing @ComponentImplementation annotation.", true);

        this.failToRegisterComponent(ManyTypeAnnotationImpl.class);
        SearchableConsole.assertContainsMessage("Found more thant one implemented interfaces annotated with @ComponentType for '" + ManyTypeAnnotationImpl.class.getCanonicalName() + "'.", true);

        // ComponentImplementation
        this.failToRegisterComponent(ImplementationAsEnumImpl.class);
        SearchableConsole.assertContainsMessage("@ComponentImplementation element '" + ImplementationAsEnumImpl.class.getCanonicalName() + "' must be a class.", true);

        // Component naming convention
        this.failToRegisterComponent(NamingggProblemImplementation.class);
        SearchableConsole.assertContainsMessage("The package's name of type '" + NamingggProblem.class.getCanonicalName() + "' must end with 'namingggproblem'.");
        SearchableConsole.assertContainsMessage("The name of the component implementation '" + NamingggProblemImplementation.class.getCanonicalName() + "' must end with 'NamingggProblemImpl'.");
        SearchableConsole.assertContainsMessage("The package's name of implementation '" + NamingggProblemImplementation.class.getCanonicalName() + "' must end with 'namingggproblem.impl'.", true);

        // Fields tests
        this.failToRegisterComponent(IdBrokenFieldImpl.class);
        SearchableConsole.assertContainsMessage("@Id field 'thisId' must be typed as 'net.cadier.jcf.lang.instance.InstanceId'.");
        SearchableConsole.assertContainsMessage("@Id field 'packageId' field must be private or protected.");
        SearchableConsole.assertContainsMessage("@Id field 'publicId' field must be private or protected.");
        SearchableConsole.assertContainsMessage("@Id field 'notFinalId' field must be final.");
        SearchableConsole.assertContainsMessage("@Id field 'notTransientId' field must be transient.", true);

        this.failToRegisterComponent(ProvidedServiceBrokenFieldImpl.class);
        SearchableConsole.assertContainsMessage("@ProvidedService 'serviceAsClass' type must be an interface.");
        SearchableConsole.assertContainsMessage("@ProvidedService 'serviceAsClass' isn't declared in component's type @ComponentType annotation.");
        SearchableConsole.assertContainsMessage("@ProvidedService 'serviceMissingAnnotation' type must be annotated with @ComponentService.");
        SearchableConsole.assertContainsMessage("@ProvidedService 'packageHelloWorldService' field  must be public or protected or private.");
        SearchableConsole.assertContainsMessage("@ProvidedService 'notFinalHelloWorldService' field  must be final.");
        SearchableConsole.assertContainsMessage("@ProvidedService 'notTransientHelloWorldService' field  must be transient.", true);

        this.failToRegisterComponent(UsedServiceBrokenFieldImpl.class);
        SearchableConsole.assertContainsMessage("@UsedService 'mapKeyAsString' *-cardinality map's key must be typed as 'net.cadier.jcf.lang.instance.InstanceId'.");
        SearchableConsole.assertContainsMessage("@UsedService 'serviceAsClass' field's type must be an interface.");
        SearchableConsole.assertContainsMessage("@UsedService 'serviceMissingAnnotation' type must be annotated with @ComponentService.");
        SearchableConsole.assertContainsMessage("@UsedService 'packageHelloWorldService' field  must be protected or private.");
        SearchableConsole.assertContainsMessage("@UsedService 'publicHelloWorldService' field  must be protected or private.");
        SearchableConsole.assertContainsMessage("@UsedService 'notFinalHelloWorldService' field  must be final.");
        SearchableConsole.assertContainsMessage("@UsedService 'notTransientHelloWorldService' field  must be transient.", true);

        this.failToRegisterComponent(SourceBrokenFieldImpl.class);
        SearchableConsole.assertContainsMessage("@EventsSource 'sourceTypeError' must be typed as 'net.cadier.jcf.lang.instance.events.Source'.");
        SearchableConsole.assertContainsMessage("@EventsSource 'sourceDataTypeClassOrEnumError' data type 'net.cadier.jcf.brokencomponents.brokenfield.BrokenField' must be a class or an enum.");
        SearchableConsole.assertContainsMessage("@EventsSource 'sourceDataTypeAnnotationError' data type 'java.lang.String' must be annotated with @EventData.");
        SearchableConsole.assertContainsMessage("@EventsSource 'sourceDataTypeAnnotationError' data type 'java.lang.String' isn't declared in component's type @ComponentType annotation.");
        SearchableConsole.assertContainsMessage("@EventsSource 'packageMessageSource' field must be public or protected or private.");
        SearchableConsole.assertContainsMessage("@EventsSource 'notFinalMessageSource' field must be final.");
        SearchableConsole.assertContainsMessage("@EventsSource 'notTransientMessageSource' field must be transient.", true);

        this.failToRegisterComponent(SinkBrokenFieldImpl.class);
        SearchableConsole.assertContainsMessage("@EventsSink 'sinkTypeError' must be typed as 'net.cadier.jcf.lang.instance.events.Sink'.");
        SearchableConsole.assertContainsMessage("@EventsSink 'sinkDataTypeClassOrEnumError' data type 'net.cadier.jcf.brokencomponents.brokenfield.BrokenField' must be a class or an enum.");
        SearchableConsole.assertContainsMessage("@EventsSink 'sinkDataTypeAnnotationError' data type 'java.lang.String' must be annotated with @EventData.");
        SearchableConsole.assertContainsMessage("@EventsSink 'packageMessageSink' field must be private.");
        SearchableConsole.assertContainsMessage("@EventsSink 'notFinalMessageSink' field must be final.");
        SearchableConsole.assertContainsMessage("@EventsSink 'notTransientMessageSink' field must be transient.", true);

        this.failToRegisterComponent(ParametersBrokenFieldImpl.class);
        SearchableConsole.assertContainsMessage("@Parameter 'parametersTypeError' must be typed as 'net.cadier.jcf.lang.instance.parameters.Parameters'.");
        SearchableConsole.assertContainsMessage("@Parameter 'parametersDataTypeAnnotationError' data type 'javax.lang.model.element.ElementKind' must be annotated with @ParameterData.");
        SearchableConsole.assertContainsMessage("@Parameter 'packageMessageParameters' field must be private or protected.");
        SearchableConsole.assertContainsMessage("@Parameter 'publicMessageParameters' field must be private or protected.");
        SearchableConsole.assertContainsMessage("@Parameter 'notFinalMessageParameters' field must be final.");
        SearchableConsole.assertContainsMessage("@Parameter 'notTransientMessageParameters' field must be transient.", true);
    }

    @Test
    @Order(5)
    public void unregisteredComponentException() {
        // try to deploy an instance of an unregistered component
        this.failToDeployInstance(this.thisId, ServiceUserImpl.class, true);
        SearchableConsole.assertContainsMessage("Cannot deploy instance of an unregistered component: '" + ServiceUserImpl.class.getCanonicalName() + "'.", true);
    }

    @Test
    @Order(6)
    public void deploymentRuleException() {
        // try to deploy, as root, a second instance of a root
        this.failToDeployInstance(null, JDKLoggingImpl.class, true);
        SearchableConsole.assertContainsMessage("Instance of '" + JDKLoggingImpl.class.getCanonicalName() + "' cannot be deployed as a root instance because it is already deployed.", true);

        // try to deploy an instance of an already deployed, as root, component
        this.failToDeployInstance(this.thisId, JDKLoggingImpl.class, true);
        SearchableConsole.assertContainsMessage("Instance of '" + JDKLoggingImpl.class.getCanonicalName() + "' cannot be deployed under '" + this.thisId + "' because it is already deployed as a root instance.", true);
    }

    @Test
    @Order(7)
    public void dependencyResolutionException() {
        this.registerComponent(ServiceUserImpl.class);
        this.registerComponent(ServiceProviderImpl.class);

        // deploy 2 provider and 1 user that should fail on resolution
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);
        this.deployInstance(this.thisId, ServiceProviderImpl.class, true);
        this.failToDeployInstance(this.thisId, ServiceUserImpl.class, true);
        SearchableConsole.assertContainsMessage("Found too much implementations for @UsedService 'net.cadier.jcf.components.HelloWorldService'", true);
    }

    private void failToRegisterComponent(Class<?> implToRegister) {
        try {
            this.containerServices.registerComponent(implToRegister);
            Assertions.fail("MalformedComponentException should raise for '" + implToRegister.getCanonicalName() + "'.");
        } catch (MalformedComponentException mce) {
            mce.printDiagnostic(SearchableConsole.getSearchablePrintStream());
        }
    }

    private void failToDeployInstance(InstanceId parentId, Class<?> implToDeploy, boolean autoStart) {
        try {
            this.containerServices.deployInstance(parentId, implToDeploy, autoStart);
            Assertions.fail("InstanceDeploymentException should raise for '" + implToDeploy.getCanonicalName() + "'.");
        } catch (DeploymentRuleException | DependencyResolutionException | ReflectionException ide) {
            ide.printStackTrace(SearchableConsole.getSearchablePrintStream());
        }
    }
}
