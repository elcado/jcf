/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */
package net.cadier.jcf.tree;

import net.cadier.utils.tree.Tree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Frédéric Cadier
 */
public class TreeTest {

    private final Tree<Float, String> tree = new Tree<>();

    @Test
    public void assertAddRoots() {
        this.tree.addRoot(1f, "elt 1");
        Assertions.assertTrue(this.tree.getRoots().contains(1f));

        this.tree.addRoot(2f, "elt 2");
        Assertions.assertTrue(this.tree.getRoots().contains(2f));

        this.tree.addRoot(3f, "elt 3");
        Assertions.assertTrue(this.tree.getRoots().contains(3f));
    }

    @Test
    public void assertAddElements() {
        this.assertAddRoots();

        this.tree.addElement(1f, 1.1f, "elt 1.1");
        this.tree.addElement(1f, 1.2f, "elt 1.2");
        Assertions.assertTrue(this.tree.getChildren(1f).contains(1.1f));
        Assertions.assertTrue(this.tree.getChildren(1f).contains(1.2f));

        this.tree.addElement(2f, 2.1f, "elt 2.1");
        this.tree.addElement(2f, 2.2f, "elt 2.2");
        this.tree.addElement(2f, 2.3f, "elt 2.3");
        Assertions.assertTrue(this.tree.getChildren(2f).contains(2.1f));
        Assertions.assertTrue(this.tree.getChildren(2f).contains(2.2f));
        Assertions.assertTrue(this.tree.getChildren(2f).contains(2.3f));

        this.tree.addElement(2.2f, 2.21f, "elt 2.2.1");
        this.tree.addElement(2.2f, 2.22f, "elt 2.2.2");
        this.tree.addElement(2.2f, 2.23f, "elt 2.2.3");
        Assertions.assertTrue(this.tree.getChildren(2.2f).contains(2.21f));
        Assertions.assertTrue(this.tree.getChildren(2.2f).contains(2.22f));
        Assertions.assertTrue(this.tree.getChildren(2.2f).contains(2.23f));

        this.tree.addElement(3f, 3.1f, "elt 3.1");
        Assertions.assertTrue(this.tree.getChildren(3f).contains(3.1f));

        // should fail because parent 4 does not exists in tree
        try {
            this.tree.addElement(4f, 4.1f, "elt 4.1");
            Assertions.fail("Should have raised IllegalArgumentException");
        } catch (IllegalArgumentException iae) {
        }
    }

    @Test
    public void assertGetter() {
        this.assertAddRoots();
        this.assertAddElements();

        Assertions.assertNotNull(this.tree.getRef(3.1f));
        try {
            Assertions.assertNotNull(this.tree.getRef(4f));
            Assertions.fail("Should have raised IllegalArgumentException");
        } catch (IllegalArgumentException iae) {
        }

        Assertions.assertTrue(!this.tree.getChildren(2f).isEmpty());
        Assertions.assertTrue(this.tree.getChildren(2.3f).isEmpty());

        Assertions.assertNotNull(this.tree.getParent(2.2f));
        Assertions.assertNull(this.tree.getParent(2f));

        Assertions.assertNotNull(this.tree.getRoots());

        Assertions.assertTrue(!this.tree.getNeighbors(1.2f).isEmpty());
        Assertions.assertTrue(this.tree.getNeighbors(3.1f).isEmpty());
    }

    @Test
    public void assertRemove() {
        this.assertAddRoots();
        this.assertAddElements();

        System.out.println(this.treeToString());

        // should pass
        this.tree.removeElement(2.2f);

        // should fail
        try {
            this.tree.removeElement(4f);
            Assertions.fail("Should have raised IllegalArgumentException");
        } catch (IllegalArgumentException iae) {
        }

        System.out.println(this.treeToString());

    }

    private String treeToString() {
        StringBuilder bob = new StringBuilder();

        bob.append("=====================================================\n");

        for (Float instanceId : this.tree.getRoots()) {
            bob.append(this.instanceToString(instanceId, ""));
        }

        bob.append("=====================================================\n");

        return bob.toString();
    }

    private Object instanceToString(Float instanceId, String indent) {
        StringBuilder bob = new StringBuilder();

        String instance = this.tree.getRef(instanceId);

        String prefix;
        if (indent.isEmpty()) prefix = "- ";
        else prefix = "" + indent + "- ";

        bob.append(prefix);
        bob.append(instance);
        bob.append("\n");

        for (Float childId : this.tree.getChildren(instanceId)) {
            bob.append(this.instanceToString(childId, indent + "|  "));
        }

        return bob.toString();
    }
}
