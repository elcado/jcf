/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcf.utils.abstracttest;

import net.cadier.jcf.annotations.Id;
import net.cadier.jcf.annotations.Parameter;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.core.container.ContainerServices;
import net.cadier.jcf.core.container.impl.ContainerImpl;
import net.cadier.jcf.core.logging.LoggingParameters;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.exception.DeploymentRuleException;
import net.cadier.jcf.exception.MalformedComponentException;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.parameters.Parameters;
import net.cadier.jcf.utils.searchableconsole.SearchableConsole;
import net.cadier.utils.reflection.ReflectionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

@TestMethodOrder(OrderAnnotation.class)
public abstract class AbstractJCFTest {
    private Object defaultLoggingLevel;

    protected ContainerServices containerServices;

    @Id
    protected final transient InstanceId thisId = null;

    @UsedService
    protected final transient LoggingServices loggingServices = null;

    @Parameter
    protected final transient Parameters<LoggingParameters> loggingParameters = null;

    @RegisterExtension
    BeforeEachCallback beforeEachCallback = context -> {
        // accepting message by default
        SearchableConsole.reset();

        // initialize container with the current object as root instance
        this.containerServices = ContainerImpl.initialize(AbstractJCFTest.this);

        // log level
        this.defaultLoggingLevel = this.loggingParameters.set(LoggingParameters.LOGGING_LEVEL,
                getTestLoggingLevel());

        List<Class<?>> testComponentsToRegister = this.getTestComponentsToRegister();
        if (!testComponentsToRegister.isEmpty()) {
            this.loggingServices.info("#################################\n" + "### DEPLOYING TEST " +
                    "COMPONENTS ###\n" + "#################################");
        }

        testComponentsToRegister.forEach(componentImplementation -> {
            try {
                this.containerServices.registerComponent(componentImplementation);
            } catch (MalformedComponentException mce) {
                mce.printDiagnostic(System.err);
                System.err.println("\n### ABORTING TEST ###");
                Assertions.fail(mce);
            }
        });

        this.loggingServices.info("######################\n" + "### LAUNCHING TEST ###\n" +
                "######################");
    };

    protected Level getTestLoggingLevel() {
        return Level.SEVERE;
    }

    protected List<Class<?>> getTestComponentsToRegister() {
        return Arrays.asList();
    }

    @RegisterExtension
    AfterEachCallback afterEachCallback = context -> {
        this.loggingServices.info("###################\n" + "### ENDING TEST ###\n" +
                "###################");

        boolean testFailed = context.getExecutionException().isPresent();
        this.containerServices.terminate(testFailed);

        this.loggingParameters.set(LoggingParameters.LOGGING_LEVEL, this.defaultLoggingLevel);
    };

    protected void registerComponent(Class<?> componentImplClass) {
        this.loggingServices.info("### Registering '" + componentImplClass.getCanonicalName() + "' component");
        try {
            this.containerServices.registerComponent(componentImplClass);
        } catch (MalformedComponentException mce) {
            mce.printDiagnostic(System.err);
            Assertions.fail(mce);
        }
    }

    protected void unregisterComponent(Class<?> componentImplClass) {
        this.loggingServices.info("### Unregistering '" + componentImplClass.getCanonicalName() + "' component");
        this.containerServices.unregisterComponent(componentImplClass);
    }

    protected InstanceId deployInstance(InstanceId parentId, Class<?> componentType, boolean autoStart) {
        this.loggingServices.info("### Deploying '" + componentType.getSimpleName() + "' instance");
        InstanceId instanceId = null;
        try {
            instanceId = this.containerServices.deployInstance(parentId, componentType, autoStart);
        } catch (DeploymentRuleException | DependencyResolutionException | ReflectionException e) {
            Assertions.fail(e);
        }
        return instanceId;
    }

    protected void startInstance(InstanceId instanceId, Class<?> componentType) {
        this.loggingServices.info("### Starting '" + instanceId + "' instance [" + componentType.getSimpleName() + "]");
        this.containerServices.startInstance(instanceId);
    }

    protected void pauseInstance(InstanceId instanceId, Class<?> componentType) {
        this.loggingServices.info("### Pausing '" + instanceId + "' instance [" + componentType.getSimpleName() + "]");
        this.containerServices.pauseInstance(instanceId);
    }

    protected void resumeInstance(InstanceId instanceId, Class<?> componentType) {
        this.loggingServices.info("### Resuming '" + instanceId + "' instance [" + componentType.getSimpleName() + "]");
        this.containerServices.resumeInstance(instanceId);
    }

    protected void stopInstance(InstanceId instanceId, Class<?> componentType) {
        this.loggingServices.info("### Stopping '" + instanceId + "' instance [" + componentType.getSimpleName() + "]");
        this.containerServices.stopInstance(instanceId);
    }

    protected boolean undeployInstance(InstanceId instanceId, Class<?> componentType) {
        this.loggingServices.info("### Undeploying '" + instanceId + "' instance [" + componentType.getSimpleName() + "]");
        return this.containerServices.undeployInstance(instanceId);
    }

    protected void pause(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            this.loggingServices.error(e.getMessage());
            // TODO throw exception
        }
    }

}
