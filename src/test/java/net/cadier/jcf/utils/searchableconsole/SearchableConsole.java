package net.cadier.jcf.utils.searchableconsole;

import java.io.PrintStream;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

public class SearchableConsole {
    /**
     * Flag to block new message while checking.
     */
    private static AtomicBoolean acceptingMessage = new AtomicBoolean(true);

    private static ConcurrentLinkedQueue<String> messageQueue = new ConcurrentLinkedQueue<String>();

    private static PrintStream printStream = new PrintStream(System.out) {
        @Override
        public void println(String message) {
            if (acceptingMessage.get()) {
                super.println(message);
                messageQueue.add(message);
            }
        }

        @Override
        public void println(Object message) {
            if (acceptingMessage.get()) {
                super.println(message);
                messageQueue.add(message.toString());
            }
        }
    };

    /**
     * Adds a message to the searchable console.
     *
     * @param message message to add
     */
    public static void addMessage(String message) {
        printStream.println(message);
    }

    /**
     * Returns a searchable {@link PrintStream}.
     *
     * @return a wrapper on System.out
     */
    public static PrintStream getSearchablePrintStream() {
        return printStream;
    }

    /**
     * Reset searchable console.
     */
    public static void reset() {
        acceptingMessage.set(false);
        messageQueue.clear();
        acceptingMessage.set(true);
    }

    /**
     * Test if no message has been received.
     *
     * @param resetMessageQueue flag to reset searchable console
     */
    public static void assertNoMessage(boolean resetMessageQueue) {
        acceptingMessage.set(false);

        assert messageQueue.isEmpty() : messageQueue.size() + " message(s) has(ve) been received.";

        if (resetMessageQueue) messageQueue.clear();

        acceptingMessage.set(true);
    }

    /**
     * Test if supplied exact message can be found the exact supplied number of occurrences, without resetting the searchable console.
     *
     * @param message         searched message
     * @param nbOfOccurrences searched number of occurrences
     */
    public static void assertExactMessageNbOfOccurrences(String message, int nbOfOccurrences) {
        assertExactMessageNbOfOccurrences(message, nbOfOccurrences, false);
    }

    /**
     * Test if supplied exact message can be found the exact supplied number of occurrences.
     *
     * @param message           searched message
     * @param nbOfOccurrences   searched number of occurrences
     * @param resetMessageQueue flag to reset the searchable console
     */
    public static void assertExactMessageNbOfOccurrences(String message, int nbOfOccurrences, boolean resetMessageQueue) {
        acceptingMessage.set(false);

        long count = messageQueue.stream().filter(message::equals).count();
        assert (nbOfOccurrences == count) : "Couldn't find exactly " + nbOfOccurrences + " occurrences of '" + message + "' (found " + count + ".";

        if (resetMessageQueue) messageQueue.clear();

        acceptingMessage.set(true);
    }

    /**
     * Test if supplied message can be exactly found at least once, without resetting the searchable console.
     *
     * @param message searched message
     */
    public static void assertMessage(String message) {
        assertMessage(message, false);
    }

    /**
     * Test if supplied message can be exactly found at least once.
     *
     * @param message           searched message
     * @param resetMessageQueue flag to reset the searchable console
     */
    public static void assertMessage(String message, boolean resetMessageQueue) {
        acceptingMessage.set(false);

        boolean found = messageQueue.stream().anyMatch(message::equals);
        assert found : "Couldn't find any occurrence of '" + message + "'.";

        if (resetMessageQueue) messageQueue.clear();

        acceptingMessage.set(true);
    }

    /**
     * Test if supplied message is contained in one added messages, without resetting the searchable console.
     *
     * @param message searched message
     */
    public static void assertContainsMessage(String message) {
        assertContainsMessage(message, false);
    }

    /**
     * Test if supplied message is contained in one added messages.
     *
     * @param message           searched message
     * @param resetMessageQueue flag to reset the searchable console
     */
    public static void assertContainsMessage(String message, boolean resetMessageQueue) {
        acceptingMessage.set(false);

        boolean found = messageQueue.stream().anyMatch(s -> s.contains(message));
        assert found : "Couldn't find any occurrence of '" + message + "'.";

        if (resetMessageQueue) messageQueue.clear();

        acceptingMessage.set(true);
    }

    /**
     * Test if supplied regex can be found the exact supplied number of occurrences, without resetting the searchable console.
     *
     * @param regex           searched regex
     * @param nbOfOccurrences searched number of occurrences
     */
    public static void assertExactRegexNbOfOccurrences(String regex, int nbOfOccurrences) {
        assertExactRegexNbOfOccurrences(regex, nbOfOccurrences, false);
    }

    /**
     * Test if supplied regex can be found the exact supplied number of occurrences.
     *
     * @param regex             searched regex
     * @param nbOfOccurrences   searched number of occurrences
     * @param resetMessageQueue flag to reset the searchable console
     */
    public static void assertExactRegexNbOfOccurrences(String regex, int nbOfOccurrences, boolean resetMessageQueue) {
        acceptingMessage.set(false);

        long count = messageQueue.stream().filter(message -> Pattern.compile(regex).matcher(message).find()).count();
        assert (nbOfOccurrences == count) : "Couldn't find exactly " + nbOfOccurrences + " occurrences of '" + regex + "' (found " + count + ".";

        if (resetMessageQueue) messageQueue.clear();

        acceptingMessage.set(true);
    }

}
