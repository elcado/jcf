Release notes - jCF
===================

###### v1.2.6 02/01/2022
- Advanced internal exception to capture deployment errors
- Added support for JDK 11

###### v1.2.5 10/11/2021
- Added internal exception to capture deployment errors

###### v1.2.4 17/01/2021
- Added: a component can be notified on parameter change through a Sink on parameter data 
- Added : jcf.properties

###### v1.2.3 09/01/2021
- Added: component can be deployed after components that uses it (service or event)
- Refacto: Container initialization and services

###### v1.2.2 13/12/2020
- Fixed stopping Sinkqueue

###### v1.2.1 06/12/2020
- Fixed composition

###### v1.2 11/11/2020
- Added a new Debug services in Container.
- Added a FATAL level in Logging Service.

###### v1.1.1 06/11/2020
- Relaxing constraint on @UsedService definition to allow PROTECTED when extending implementation.

###### v1.1 06/11/2020
- Allows provided services to be defined in superclass of implementation.

###### v1.0 27/07/2020
- Initial version with component definition and deployment.